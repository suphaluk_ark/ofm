*** Setting ***
Resource    ${CURDIR}/../../../keywords/web/web_imports.robot
Resource 	${CURDIR}/../../../keywords/api/api_imports.robot
Suite Setup     Run Keywords 	web_imports.Global init
... 	AND 	api_imports.API global init
Test Teardown    Run Keywords 	CommonWebKeywords.Test Teardown 	AND 	SeleniumLibrary.Close browser
Suite Teardown    SeleniumLibrary.Close All Browsers

*** Variables ***
${personal_member} 	personal
${company_member} 	company
${guest_member} 	guest
${facebook_member} 	facebook
${credit_card_order_id} 	${None}
${online_banking_order_id} 	${None}

*** Keyword ***
Setup - Open browser
	CommonWebKeywords.Open Chrome Browser to page    ${URL_OFM} 	${seleniumSpeed} 	headless=${headless_flag}

Test setup - open browser
	${status}= 	Run Keyword And Ignore Error 	SeleniumLibrary.Get Location
	Run Keyword If 	'officemate' not in '${status[1]}' 	Setup - Open browser

Test teardown - update shipping and billing address as default value
	api_customer_details.Update customer as default shipping address 	${personal_username} 	${personal_password}
	api_customer_details.Update customer as default billing address 	${personal_username} 	${personal_password}

Test teardown - update billing address as default value
	${status} 	Run Keyword And Return Status 	header_menu.Select manage account in modal
	Run Keyword If 	not ${status} 	Run Keywords 	header_menu.Select OFM logo
	... 	AND 	header_menu.Select manage account in modal
	... 	ELSE 	Select manage account in modal
	my_account_feature.Update billing address as default value

Test setup - go to home page
	${status} 	Run Keyword And Return Status 	header_menu.Select manage account in modal
	Run Keyword If 	not ${status} 	Run Keywords 	header_menu.Select OFM logo

Test setup - go to home page and log out
	${status} 	Run Keyword And Return Status 	header_menu.Select Log in button
	Run Keyword If 	not ${status} 	Run Keywords 	Run Keyword And Ignore Error 	header_menu.Select OFM logo
	... 	AND 	header_menu.Select log out in modal

Remove 2 product items from shopping cart modal and cart list
	[Arguments] 	@{product_list}
	${sku_number}= 	Evaluate 	$product_list[random.randint(0,(len($product_list)-1))] 	random
	Remove Values From List 	${product_list} 	${sku_number}
	header_menu.Remove and display product amount in shipping cart modal 	${sku_number} 	@{product_list}
	${sku_number}= 	Evaluate 	$product_list[random.randint(0,(len($product_list)-1))] 	random
	Remove Values From List 	${product_list} 	${sku_number}
	cart.Remove and display product amount in cart page 	${sku_number} 	@{product_list}
	Display product amount in shopping cart modal 	@{product_list}
	Set Test Variable 	@{product_list} 	@{product_list}

Remove remaining item in shopping cart
	[Arguments] 	${username} 	${password}
	${status}= 	Run Keyword And Return Status 	Display empty item in shopping cart
	Run Keyword If 	not ${status} 	Run Keywords 	Mouse over on OFM logo
	... 	AND 	Remove all product items in shopping cart

Remove remaining item in shopping cart for personal member
	Remove remaining item in shopping cart 	${personal_username} 	${personal_password}

Remove remaining item in shopping cart for facebook member
	Remove remaining item in shopping cart 	${facebook_username} 	${facebook_username}

Remove remaining item in shopping cart for company member
	Remove remaining item in shopping cart 	${corporate_username} 	${corporate_password}

Test setup - access system by member type
	[Arguments] 	${user_type}
	Run Keyword If 	'${user_type}'=='personal' 	Run Keywords 	Test setup - go to home page and log out
	... 	AND 	login_feature.Login with Personal account
	... 	AND 	Remove remaining item in shopping cart for personal member
	...	ELSE IF 	'${user_type}'=='company' 	Run Keywords 	Test setup - go to home page and log out
	... 	AND 	login_feature.Login with Corporate account
	... 	AND 	Remove remaining item in shopping cart for company member
	...	ELSE IF 	'${user_type}'=='facebook' 	Run Keywords 	Test setup - go to home page and log out
	... 	AND 	login_feature.Login with Facebook account
	... 	AND 	Remove remaining item in shopping cart for facebook member
	...	ELSE 	Test setup - go to home page and log out

Add product item to cart from PDP and go to cart details page
	${product_quantity}= 	Set Variable 	${1}
	${product_sku}= 	Set Variable 	@{product_sku_list}[3]
	checkoutProduct.Add product item from PDP to shopping cart 	${product_sku}
	header_menu.Display product amount in shopping cart modal 	${product_quantity} 	${product_sku}
	header_menu.Go to Shopping cart
	[Return]	${product_quantity} 	${product_sku}


Add product item to cart from PLP and go to cart details page
	[Arguments] 	${product_quantity}=${2}
	${product_quantity}= 	Set Variable 	${product_quantity}
	${product_sku}= 	Set Variable 	@{product_sku_list}[0]
	checkoutProduct.Add product item and adjust quantity to shopping cart 	${product_sku} 	${product_quantity}
	header_menu.Display product amount in shopping cart modal 	${product_quantity} 	${product_sku}
	header_menu.Go to Shopping cart
	[Return] 	${product_quantity} 	${product_sku}

Add product item to shopping cart
	${product_quantity}= 	Set Variable 	${1}
	${product_sku}= 	Set Variable 	@{product_sku_list}[2]
	header_menu.Search Product name or SKU number 	${product_sku}
	productCategory.Select Add to cart button by SKU number 	${product_sku}
	[Return] 	${product_quantity} 	${product_sku}

Display cart details and go to checkout details page
	[Arguments] 	${product_quantity} 	${product_sku}
	header_menu.Wait until does not display full page load
	cart.Display shopping cart section 	${product_sku}
	order_summary.Get order summary details by sku and quantity dictionary 	${product_sku}=${product_quantity}
	order_summary.Display order summary
	order_summary.Select on Proceed to checkout button

Display checkout details for member and go to payment details page
	[Arguments] 	${product_sku}
	header_menu.Wait until does not display full page load
	checkout.Display checkout page and default address when access via member 	${product_sku}
	order_summary.Display order summary
	checkoutProduct.Select on Proceed to checkout button and go to payment details page

C&C and input billing address for guest and go to payment details page
	[Arguments] 	${product_sku} 	${display_special_delivery}=${False}
	checkout.Retry display specify address title
	checkout.Select pick up store 	search_wording=40000 	branch=${web_common.store_location.central_khonkaen}
	checkout.Input pick up details
	header_menu.Wait until does not display full page load
	checkout.Input guest billing address
	checkout.Display product item in checkout page 	${product_sku}
	order_summary.Display order summary 	${display_special_delivery}
	checkoutProduct.Select on Proceed to checkout button and go to payment details page

Display payment details and pay by credit card
	[Arguments] 	${display_special_delivery}=${True}
	header_menu.Wait until does not display full page load
	order_summary.Display order summary 	${display_special_delivery}
	${order_number}= 	checkoutProduct.Successfully place order by credit card
	Set Suite Variable 	${credit_card_order_id} 	${order_number}

Display payment details and pay by online banking
	header_menu.Wait until does not display full page load
	order_summary.Display order summary
	${order_number}= 	checkoutProduct.Place order by online banking
	Set Suite Variable 	${online_banking_order_id} 	${order_number}

Display thank you page
	[Arguments] 	${order_id} 	${payment_type} 	${order_status} 	${display_special_delivery}=${True}
	web_thankyou.Displace thank you label in thank you page 	${order_id}
	web_thankyou.Display payment type in thank you page 	${payment_type}
	web_thankyou.Display order status in thank you page 	${order_status}
	order_summary.Display order summary 	${display_special_delivery}

Input guest home delivery, billing address and go to payment details page
	header_menu.Wait until does not display full page load
	checkout.Retry display specify address title
	checkout.Input Shipping details
	checkout.Input guest billing address
	checkoutProduct.Select on Proceed to checkout button and go to payment details page

Display default shipping and billing in checkout details and go to payment details page
	checkout.Retry display shipping address title
	checkout.Display default shipping address in checkout page
	checkout.Display default billing address in checkout page
	checkoutProduct.Select on Proceed to checkout button and go to payment details page


Test setup - prepare credit card order_id
	Return From Keyword If 	'${credit_card_order_id}'!='${None}'
	Setup - Open browser
	Test setup - access system by member type 	${personal_member}
	Add product item to cart from PLP and go to cart details page 	${1}
	header_menu.Wait until does not display full page load
	order_summary.Select on Proceed to checkout button
	checkoutProduct.Select on Proceed to checkout button and go to payment details page
	header_menu.Wait until does not display full page load
	payment.Pay order with credit card
	${order_number}= 	2c2p.Get Order number in 2c2p page
	2c2p.Submit credit card
	2c2p.Proceed OTP
	Wait Until Keyword Succeeds 	3x 	10 sec 	web_thankyou.Display thank you title
	Set Suite Variable 	${credit_card_order_id} 	${order_number}
	SeleniumLibrary.Close All Browsers

Test setup - prepare online banking order_id
	Return From Keyword If 	'${online_banking_order_id}'!='${None}'
	Setup - Open browser
	Test setup - access system by member type 	${personal_member}
	Add product item to cart from PLP and go to cart details page 	${1}
	header_menu.Wait until does not display full page load
	order_summary.Select on Proceed to checkout button
	checkoutProduct.Select on Proceed to checkout button and go to payment details page
	header_menu.Wait until does not display full page load
	payment.Pay order with online banking 	back_code=BBL 	tel=0255555555
	2c2p.Display phone number in 123 page 	phone_number=0255555555
	2c2p.Select next button in 123 page
	2c2p.Select on back button in 123 page
	Wait Until Keyword Succeeds 	3x 	10 sec 	web_thankyou.Display thank you title
	${url}= 	SeleniumLibrary.Get Location
	${order_number}= 	Evaluate 	$url.split('/')[-1]
	Set Suite Variable 	${online_banking_order_id} 	${order_number}
	SeleniumLibrary.Close All Browsers

Add product to cart by search sku, verify item on shopping cart and checkout detail page
	${product_quantity} 	${product_sku} 	Add product item to shopping cart
	header_menu.Go to Shopping cart
	cart.Display product item in shopping cart page 	@{product_sku_list}[2]
	Display cart details and go to checkout details page 	${product_quantity} 	${product_sku}

Add product then go to Shopping cart
	${product_quantity} 	${product_sku} 	Add product item to shopping cart
	header_menu.Go to Shopping cart
	cart.Display product item in shopping cart page 	@{product_sku_list}[2]

Add product item to cart from quick add and verify cart details page
	[Arguments] 	${product_quantity}=${1}
	${product_quantity}= 	Set Variable 	${product_quantity}
	${product_sku}= 	Set Variable 	@{product_sku_list}[3]
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[searchTXT] 	label=${quick_add_placeholder}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	CommonWebKeywords.Input Text And Verify Input For Web Element 	${elem} 	${product_sku}
	${elem}= 	CommonKeywords.Format Text 	&{cart}[btn_quick_add] 	label=&{web_common}[quick_add]
	CommonWebKeywords.Click Element 	${elem}
	header_menu.Wait until does not display full page load
	[Return] 	${product_quantity} 	${product_sku}

Add product via shop by product ID, remove product, verify surcharge fee and go to payment details page
	[Arguments] 	${display_special_delivery}=${True}
	Add product then go to Shopping cart
	${product_quantity} 	${product_sku} 	Add product item to cart from quick add and verify cart details page
	cart.Select on delete button in cart page 	@{product_sku_list}[2]
	cart.Display delete in modal cart
	Select on remove in modal cart
	cart.Display shopping cart section 	${product_sku}
	order_summary.Get order summary details by sku and quantity dictionary 	${product_sku}=${product_quantity}
	order_summary.Display order summary 	${display_special_delivery}
	checkoutProduct.Member select on Proceed to checkout button and go to checkout detials page


*** Test Cases ***
C134889 - Successfully add product via PDP, order products with Credit card method and pickup at store by guest
	[Tags] 	smoke	guest
	[Setup] 	Run Keywords 	Setup - Open browser
	... 	AND 	Test setup - access system by member type 	${guest_member}
	${product_quantity} 	${product_sku} 	Add product item to cart from PDP and go to cart details page
	Display cart details and go to checkout details page 	${product_quantity} 	${product_sku}
	C&C and input billing address for guest and go to payment details page 	${product_sku}
	Display payment details and pay by credit card	display_special_delivery=${False}
	Display thank you page 	order_id=${credit_card_order_id} 	payment_type=&{web_thank_you}[credit_card] 	order_status=&{web_thank_you}[processing]	display_special_delivery=${False}
	[Teardown] 	Run Keywords 	CommonWebKeywords.Test Teardown
	... 	AND 	SeleniumLibrary.Close browser

C134894 - Successfully add product via PLP, order products with Online banking method and pickup at store by member
	[Tags] 	smoke	individual
	[Setup] 	Run Keywords 	Setup - Open browser
	... 	AND 	Test setup - access system by member type 	${personal_member}
	${product_quantity} 	${product_sku} 	Add product item to cart from PLP and go to cart details page
	Display cart details and go to checkout details page 	${product_quantity} 	${product_sku}
	Display checkout details for member and go to payment details page 	${product_sku}
	Display payment details and pay by online banking
	Display thank you page 	order_id=${online_banking_order_id} 	payment_type=&{web_thank_you}[online_banking] 	order_status=&{web_thank_you}[processing] 	display_special_delivery=${False}
	[Teardown] 	Run Keywords 	CommonWebKeywords.Test Teardown
	... 	AND 	SeleniumLibrary.Close browser

C134892 - Successfully order products with Central The 1 Credit Card method and Home delivery
	[Tags] 	smoke	individual
	[Setup] 	Run Keywords 	Setup - Open browser
	... 	AND 	Test setup - access system by member type 	${facebook_member}
	${product_quantity} 	${product_sku} 	Add product item to cart from PLP and go to cart details page 	${1}
	Display cart details and go to checkout details page 	${product_quantity} 	${product_sku}
	Display default shipping and billing in checkout details and go to payment details page
	${order_number}= 	checkoutProduct.Successfully place order by T1 credit card
	Display thank you page 	order_id=${order_number}	payment_type=&{web_thank_you}[t1c_credit_card] 	order_status=&{web_thank_you}[processing] 	display_special_delivery=${False}
	[Teardown] 	Run Keywords 	CommonWebKeywords.Test Teardown
	... 	AND 	SeleniumLibrary.Close browser

C134893 - Successfully order products with Counter service method and Home delivery
	[Tags] 	smoke	guest
	[Setup] 	Run Keywords 	Setup - Open browser
	... 	AND 	Test setup - access system by member type 	${guest_member}
	${product_quantity} 	${product_sku} 	Add product item to cart from PLP and go to cart details page 	${1}
	Display cart details and go to checkout details page 	${product_quantity} 	${product_sku}
	Input guest home delivery, billing address and go to payment details page
	${order_number}= 	checkoutProduct.Place order by counter service
	Display thank you page 	order_id=${order_number} 	payment_type=&{web_thank_you}[online_banking] 	order_status=&{web_thank_you}[processing] 	display_special_delivery=${False}
	[Teardown] 	Run Keywords 	CommonWebKeywords.Test Teardown
	... 	AND 	SeleniumLibrary.Close browser

C134898 - Verify order status on MCOM after order created (Payment authorise)
	[Tags] 	smoke	guest
	[Setup] 	Test setup - prepare credit card order_id
	verify_mdc.Verify mcom order status with retry 	order_id=${credit_card_order_id} 	expected_status=&{mcom_order_status}[logistics]

C134898 - Verify order status on MCOM after order created (On hold)
	[Tags] 	smoke	individual
	[Setup] 	Test setup - prepare online banking order_id
	verify_mdc.Verify mcom order status with retry 	order_id=${online_banking_order_id} 	expected_status=&{mcom_order_status}[onhold]

C134891 - Verify order status in order history page
	[Tags] 	smoke	individual
	[Setup] 	Run Keywords 	Test setup - prepare online banking order_id
	... 	AND 	Setup - Open browser
	... 	AND 	Test setup - go to home page and log out
	... 	AND 	login_feature.Login with Personal account
	header_menu.Go to order history
	order_history.Display order list by increment id 	mdc_increment_id=${online_banking_order_id} 	productImagesAmount=1


C134890 - Successfully add product via search, order products has delivery fee with COD method and Standard Home delivery by company admin.
	[Tags] 	smoke	company
	[Setup] 	Run Keywords 	Setup - Open browser
	... 	AND 	Test setup - access system by member type 	${company_member}
	Add product to cart by search sku, verify item on shopping cart and checkout detail page
	Display default shipping and billing in checkout details and go to payment details page
	${order_number}= 	checkoutProduct.Place order by cod
	Display thank you page 	order_id=${order_number} 	payment_type=&{web_thank_you}[cod] 	order_status=&{web_thank_you}[processing] 	display_special_delivery=${False}


C134895 - Successfully add product via Shop by product ID, order products has surcharge fee with CCOD method and Standard Home delivery by purchaser.
	[Tags] 	smoke	company
	[Setup] 	Run Keywords 	Setup - Open browser
	... 	AND 	Test setup - access system by member type 	${company_member}
	Add product via shop by product ID, remove product, verify surcharge fee and go to payment details page
	Display default shipping and billing in checkout details and go to payment details page
	${order_number}= 	checkoutProduct.Place order by ccod
	Display thank you page 	order_id=${order_number} 	payment_type=&{web_thank_you}[ccod] 	order_status=&{web_thank_you}[processing] 	display_special_delivery=${True}


