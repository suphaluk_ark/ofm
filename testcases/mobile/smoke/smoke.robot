*** Setting ***
Resource 	${CURDIR}/../../../keywords/mobile/mobile_imports.robot
Resource 	${CURDIR}/../../../keywords/api/api_imports.robot

Suite Setup     Run Keywords 	mobile_imports.Mobile global init
... 	AND 	api_imports.API global init
Test Teardown    CommonMobileKeywords.Test Teardown

*** Variables ***
${personal_member} 	personal
${company_member} 	company
${guest_member} 	guest
${facebook_member} 	facebook
${user} 	personal_mobile1

*** Keyword ***
Setup Open application
    [Arguments] 	${no_reset}=${True}
    CommonMobileKeywords.Open Application On 	&{${mobileDevice}}[platform]
    ... 	&{${mobileDevice}}[deviceName]
    ... 	&{${mobileDevice}}[appLocation]
    ... 	no_reset=${False}

Test setup - logout from application
    Run Keyword And Ignore Error 	header_menu.Select main menu
    ${status}= 	Run Keyword And Return Status 	left_menu.Display log in menu
    Run Keyword If 	not ${status} 	left_menu.Select on sign out button

Test setup - access application by member type
    [Arguments] 	${member_type}
    Run Keyword If 	'${member_type}'=='${personal_member}' 	Run Keywords 	Setup Open application 	no_reset=${False}
    ... 	AND 	login_feature.Go to login and sign in with personal account 	${user}
    ... 	AND 	Delete products in cart page
    ...	ELSE IF 	'${member_type}'=='${facebook_member}' 	Run Keywords 	Setup Open application 	no_reset=${False}
    ... 	AND 	login_feature.Go to login and sign in with facebook account
    ... 	AND 	Delete products in cart page
    ...	ELSE 	Setup Open application 	no_reset=${False}
    Set Test Variable 	${member_type} 	${member_type}

Get native application context
    ${native_app}= 	AppiumLibrary.Get Current Context
    Set Test Variable 	${application_context} 	${native_app}
    Set Test Variable 	${native_app} 	${native_app}

Display thank you page and order summary
    [Arguments] 	${order_id} 	${payment_method} 	${order_status}     ${isNoSurchargeFee}=${False}    ${isNoDeliveryFee}=${False}
    Run Keyword If  '${application_context}'!='NATIVE_APP' 	AppiumLibrary.Switch To Context 	${native_app}
    mobile_thankyou.Display order_id, payment_type, and status in thank you page 	order_id=${order_id} 	payment_method=${payment_method} 	order_status=${order_status}
    order_summary.Display order summary in thank you page 	${isNoSurchargeFee}    ${isNoDeliveryFee}

Display cart details and go to checkout details page
    [Arguments] 	${product_sku} 	${product_qty}
    cart.Display product details in cart details page 	${product_sku} 	${product_qty}
    order_summary.Calculate order summary 	${product_sku} 	${product_qty}
    order_summary.Display order summary in cart and checkout page
    place_order_feature.Select continue payment and go to checkout details

Input Pick up at store, billing and go to payment details page
    [Arguments] 	${product_sku} 	${product_qty}
    checkout.Guest input pick up at store and billing address
    order_summary.Calculate order summary  ${product_sku} 	${product_qty}  isNoSurchargeFee=${isNoSurchargeFee}
    order_summary.Display order summary in cart and checkout page 	isNoSurchargeFee=${isNoSurchargeFee}
    place_order_feature.Select continue payment and go to payment details

Member input Pick up at store, display billing and go to payment details page
    [Arguments]     ${product_sku}  ${product_qty}
    checkout.Member input pick up at store and display billing address
    order_summary.Calculate order summary  ${product_sku} 	${product_qty}  ${isNoSurchargeFee}    ${isNoDeliveryFee}
    order_summary.Display order summary in cart and checkout page   ${isNoSurchargeFee}    ${isNoDeliveryFee}
    place_order_feature.Select continue payment and go to payment details

Display checkout details with default shipping, billing and go to payment details page
    [Arguments]     ${product_sku}  ${product_qty}
    checkout.Member display default shipping and billing address in checkout details page
    order_summary.Calculate order summary  ${product_sku} 	${product_qty}
    order_summary.Display order summary in cart and checkout page
    place_order_feature.Select continue payment and go to payment details

Display payment details and place order by credit card
    order_summary.Display order summary in payment page 	isNoSurchargeFee=${True}
    ${order_number}= 	place_order_feature.Successful order by credit card
    [Return] 	${order_number}

Display payment details and place order by online banking
    order_summary.Display order summary in payment page     ${isNoSurchargeFee}    ${isNoDeliveryFee}
    ${order_number}=    place_order_feature.Successful order by online banking
    [Return]    ${order_number}

Display payment details and place order by T1C credit card
    order_summary.Display order summary in payment page
    ${order_number}=    place_order_feature.Successful order by T1C credit card
    [Return]    ${order_number}

*** Test Cases ***
C144430 - Successfully add product via PDP, order products with Credit card method and pickup at store by guest
    [Tags]	smoke	guest
    [Setup] 	Run Keywords  Setup Open application
    ... 	AND 	Get native application context
    ${product_sku} 	${product_qty} 	place_order_feature.Add item to shopping cart and go to cart details page 	@{product_sku_list}[3]
    Display cart details and go to checkout details page 	${product_sku} 	${product_qty}
    Input Pick up at store, billing and go to payment details page 	${product_sku} 	${product_qty}
    ${order_number}= 	Display payment details and place order by credit card
    Display thank you page and order summary 	order_id=${order_number} 	payment_method=&{mobile_thankyou}[credit_card] 	order_status=&{mobile_thankyou}[processing]     isNoSurchargeFee=${True}

C144426 - Successfully add product via PLP, order products with Online banking method and pickup at store by member
    [Tags] 	smoke 	individual
    [Setup] 	Run Keywords  Setup Open application
    ... 	AND 	Get native application context
    ... 	AND 	Test setup - access application by member type 	${personal_member}
    ${product_sku} 	${product_qty} 	place_order_feature.Input qty and add product to shopping cart by SKU number 	sku_number=@{product_sku_list}[0] 	input_qty=${2}  cart_qty=${2}
    Display cart details and go to checkout details page 	${product_sku} 	${product_qty}
    Member input Pick up at store, display billing and go to payment details page   ${product_sku}  ${product_qty}
    Display payment details and place order by online banking
    Display thank you page and order summary 	order_id=${order_number} 	payment_method=&{mobile_thankyou}[online_banking] 	order_status=&{mobile_thankyou}[processing]     isNoSurchargeFee=${True}   isNoDeliveryFee=${True}

C144427 - Successfully order products with Central The 1 Credit Card method and Home delivery
    [Tags]  smoke   individual  superTest
    [Setup] 	Run Keywords  Setup Open application
    ... 	AND 	Get native application context
    ... 	AND 	Test setup - access application by member type 	${facebook_member}
    ${product_sku} 	${product_qty} 	place_order_feature.Add item to shopping cart and go to cart details page 	@{product_sku_list}[0]
    Display cart details and go to checkout details page 	${product_sku} 	${product_qty}
    Display checkout details with default shipping, billing and go to payment details page  ${product_sku} 	${product_qty}
    ${order_number}=    Display payment details and place order by T1C credit card
    Display thank you page and order summary 	order_id=${order_number} 	payment_method=&{mobile_thankyou}[credit_card] 	order_status=&{mobile_thankyou}[processing]