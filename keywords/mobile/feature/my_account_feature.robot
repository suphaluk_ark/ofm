*** Keywords ***
Update shipping address as edit value
	shipping_address.Select on edit button by address    &{shipping_address_${language}}[address]
	shipping_address.Input shipping address section     firstname=&{edit_shipping_address_${language}}[firstname]
    ...    lastname=&{edit_shipping_address_${language}}[lastname]
    ...    phone=&{edit_shipping_address_${language}}[phone]
    ...    address=&{edit_shipping_address_${language}}[address]
    ...    region=&{edit_shipping_address_${language}}[region]
    ...    district=&{edit_shipping_address_${language}}[district]
    ...    sub_district=&{edit_shipping_address_${language}}[sub_district]
    shipping_address.Click on save address button

Update shipping address as default value
	shipping_address.Select on edit button by address    &{edit_shipping_address_${language}}[address]
	shipping_address.Input shipping address section     firstname=&{shipping_address_${language}}[firstname]
    ...    lastname=&{shipping_address_${language}}[lastname]
    ...    phone=&{shipping_address_${language}}[phone]
    ...    address=&{shipping_address_${language}}[address]
    ...    region=&{shipping_address_${language}}[region]
    ...    district=&{shipping_address_${language}}[district]
    ...    sub_district=&{shipping_address_${language}}[sub_district]
    shipping_address.Click on save address button

Update billing address as edit value
    billing_address.Select on edit button by address    &{billing_address_${language}}[address]
    billing_address.Input billing address section     firstname=&{edit_billing_address${language}}[firstname]
    ...    lastname=&{edit_billing_address${language}}[lastname]
    ...    phone=&{edit_billing_address${language}}[phone]
    ...    address=&{edit_billing_address${language}}[address]
    ...    region=&{edit_billing_address${language}}[region]
    ...    district=&{edit_billing_address${language}}[district]
    ...    sub_district=&{edit_billing_address${language}}[sub_district]
    ...    tax_id=&{edit_billing_address${language}}[guest_tax_id]
    billing_address.Click on save address button

Update billing address as default value
    billing_address.Select on edit button by address    &{edit_billing_address_${language}}[address]
    billing_address.Input billing address section     firstname=&{billing_address${language}}[firstname]
    ...    lastname=&{billing_address${language}}[lastname]
    ...    phone=&{billing_address${language}}[phone]
    ...    address=&{billing_address${language}}[address]
    ...    region=&{billing_address${language}}[region]
    ...    district=&{billing_address${language}}[district]
    ...    sub_district=&{billing_address${language}}[sub_district]
    ...    tax_id=&{billing_address${language}}[guest_tax_id]
    billing_address.Click on save address button