*** Setting ***

*** Keywords ***
Login with Personal account
	[Arguments] 	${user}=personal_android1
	header_menu.Select menu 	&{mobile_common}[lbl_login]
	login.Input Email 	${${user}.personal_mobile_username}
	login.Input Password 	${${user}.personal_mobile_password}
	login.Select Log in button

Logout from application
	header_menu.Select main menu
	left_menu.Select on sign out button

Login with facebook account
	header_menu.Select menu 	&{mobile_common}[lbl_login]
	login.Select on facebook button
	login.Display facebook login button
	place_order_feature.Switch to application web view context
	login.Login facebook 	${facebook_username} 	${facebook_password}
	Run Keyword If  '${application_context}'!='NATIVE_APP' 	AppiumLibrary.Switch To Context 	${native_app}

Go to login and sign in with personal account
	[Arguments] 	${user}=personal_android1
	header_menu.Select main menu
	Login with Personal account 	${user}
	header_menu.Select main menu
	header_menu.Display Log in firstname 	${${user}.personal_mobile_information.firstname}
	left_menu.Swipe left to close left menu
	${order_number}= 	api_customer_details.Get order number from API cart mine response  ${${user}.personal_mobile_username} 	${${user}.personal_mobile_password}
	Set test variable 	${order_number} 	${order_number}

Go to login and sign in with facebook account
	header_menu.Select main menu
	Login with facebook account
	header_menu.Select main menu
	header_menu.Display Log in firstname 	&{facebook_information}[firstname]
	left_menu.Swipe left to close left menu