*** Keywords ***
Add new shipping address and show correct in checkout page
    checkout.Select on select shipping address button in checkout page
    shipping_address.Input shipping address section     firstname=&{shipping_address_${language}}[firstname]
    ...    lastname=&{shipping_address_${language}}[lastname]
    ...    phone=&{shipping_address_${language}}[phone]
    ...    address=&{shipping_address_${language}}[address]
    ...    region=&{shipping_address_${language}}[region]
    ...    district=&{shipping_address_${language}}[district]
    ...    sub_district=&{shipping_address_${language}}[sub_district]
    shipping_address.Click on save address button
    checkout.Verify shipping address on checkout page     firstname=&{shipping_address_${language}}[firstname]
    ...    lastname=&{shipping_address_${language}}[lastname]
    ...    phone=&{shipping_address_${language}}[phone]
    ...    address_name=&{shipping_address_${language}}[address]
    ...    region=&{shipping_address_${language}}[region]
    ...    district=&{shipping_address_${language}}[district]
    ...    sub_district=&{shipping_address_${language}}[sub_district]
    ...    postcode=&{shipping_address_${language}}[zip_code]

Add new billing address and show correct in checkout page
    [Arguments]     ${guest}=${True}
    checkout.Select billing address button in checkout page
    Run Keyword If  '${guest}'=='${False}'  billing_address.Select add new address
    billing_address.Input billing address section     firstname=&{billing_address_${language}}[firstname]
    ...    lastname=&{billing_address_${language}}[lastname]
    ...    phone=&{billing_address_${language}}[phone]
    ...    address=&{billing_address_${language}}[address]
    ...    region=&{billing_address_${language}}[region]
    ...    district=&{billing_address${language}}[district]
    ...    sub_district=&{billing_address_${language}}[sub_district]
    ...    tax_id=&{billing_address_${language}}[guest_tax_id]
    billing_address.Click on save address button
    Run Keyword If  '${guest}'=='${False}'  header_menu.Select on back button in header menu
    checkout.Verify billing address on checkout page     firstname=&{billing_address_${language}}[firstname]
    ...    lastname=&{billing_address_${language}}[lastname]
    ...    phone=&{billing_address_${language}}[phone]
    ...    address_name=&{billing_address_${language}}[address]
    ...    region=&{billing_address_${language}}[region]
    ...    district=&{billing_address_${language}}[district]
    ...    sub_district=&{billing_address_${language}}[sub_district]
    ...    tax_id=&{billing_address_${language}}[guest_tax_id]
    ...    postcode=&{billing_address_${language}}[zip_code]
