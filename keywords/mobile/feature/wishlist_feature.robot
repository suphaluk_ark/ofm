*** Keywords ***
Guest need add product to wishlist
    wishlist.Add product to wishlist by SKU number   ${sku_number_add_wishlist}
    wishlist.Click wishlist icon
    Run Keyword If  '${member_type}'=='${guest_member}'     wishlist.Accept popup login

Add product to wishlist
    wishlist.Add product to wishlist by SKU number   ${sku_number_add_wishlist}
    wishlist.Click wishlist icon

Create wishlist group
	header_menu.Select main menu
	header_menu.Select menu 	&{wishlist}[menu]