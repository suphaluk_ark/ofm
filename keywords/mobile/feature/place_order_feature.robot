*** Keywords ***
Add product to shopping cart by SKU number
	[Arguments] 	${sku_number} 	${cart_qty}=${1}
	header_menu.Select on search button
	Wait Until Keyword Succeeds  ${global_retry_time} x 	${global_retry_in_sec} sec 	search_page.Search and select product  ${sku_number}
	product_page.Select on add to cart button in product page
	header_menu.Display product quantity at icon	${cart_qty}

Input qty and add product to shopping cart by SKU number
	[Arguments] 	${sku_number} 	${input_qty} 	${cart_qty}=${1}
	header_menu.Select on search button
	Wait Until Keyword Succeeds  ${global_retry_time} x 	${global_retry_in_sec} sec 	search_page.Search and select product  ${sku_number}
	product_page.Input qty in product page 	${input_qty}
	product_page.Select on add to cart button in product page
	header_menu.Display product quantity at icon	${cart_qty}
	place_order_feature.Select shopping cart and go to cart details
	[Return] 	${sku_number} 	${input_qty}

Add multiple product to shopping cart by SKU number
	[Arguments] 	${sku_numbers}
	${sku_len} 	Get Length 	${sku_numbers}
	${add_product_quantities}= 	Create List
	: FOR 	${index} 	${sku} 	IN ENUMERATE 	@{sku_numbers}
	\  ${qty}= 	Evaluate 	random.randint(2,5) 	random
	\  Search and select product item by SKU number 	${sku}
	\  product_page.Input product quantity in product page 	${${index+${qty}}}
	\  product_page.Select on add to cart button in product page
	\  cart.Select cart icon
	\  cart.Display product details by SKU number in cart page 	${sku} 	${${index+${qty}}}
	\  Append to List 	${add_product_quantities} 	${${index+${qty}}}
	\  Exit For Loop If 	${index}==${${sku_len-1}}
	\  header_menu.Select on back button in header menu
	Set Test Variable 	${add_product_quantities} 	${add_product_quantities}

Adjust product quantity in product page
	[Arguments] 	${sku_number}
	search_feature.Search and select product item by SKU number 	${sku_number}
	product_page.Select on add product quantity in product page
	product_page.Select on remove product quantity in product page

Pay by credit card
	[Arguments] 	${amount}=${grand_total_with_format}
	payment.Select on online credit card payment button
	order_total.Select on continue payment button
	2c2p.Display product details as officemate in 2c2p
	place_order_feature.Switch to application web view context
	2c2p.Display payment amount in 2c2p 	${amount}

Pay by COD
	payment.Select on COD payment method
	order_total.Select on continue payment button

Successful order by credit card
	Pay by credit card
	${order_number}= 	2c2p.Submit credit card
	2c2p.Procedd OTP in 2c2p page
	[Return] 	${order_number}

Pay by the 1 card redemption
	[Arguments] 	${username} 	${password}
	Sign in the 1 card member 	${username} 	${password}

Successful order by online banking
	[Arguments] 	${amount}=${grand_total_with_format}
	Payment.Payment by online banking
	order_total.Select on continue payment button
	2c2p.Display phone number in 2c2p_123 page
	2c2p.Select next button in 2c2p_123 page
	2c2p.Display payment amount in 2c2p_123 page 	${amount}
	2c2p.Select back button in 2c2p_123 page

Successful order by T1C credit card
	[Arguments] 	${amount}=${grand_total_with_format}
	Payment.Select T1C credit card button
	order_total.Select on continue payment button
	${order_number}= 	2c2p.Submit T1C credit card
	2c2p.Procedd OTP in 2c2p page
	[Return] 	${order_number}

Select shopping cart and go to cart details
	header_menu.Select on shopping cart button
	cart.Retry display shopping cart title in cart page

Select continue payment and go to checkout details
	order_total.Select on continue payment button
	checkout.Retry display shipping address title in checkout details

Select continue payment and go to payment details
	order_total.Select on continue payment button	
	payment.Retry display choose payment method title

Add item to shopping cart and go to cart details page
	[Arguments] 	${product_sku} 	${product_qty}=1
	place_order_feature.Add product to shopping cart by SKU number 	${product_sku}
	place_order_feature.Select shopping cart and go to cart details
	[Return] 	${product_sku} 	${product_qty}

Delete products in cart page
	${status}= 	Run Keyword And Return Status 	header_menu.Display product quantity at icon 	${0}
	Run Keyword If 	not ${status} 	Run Keywords 	header_menu.Select on shopping cart button
	...	AND 	cart.Delete all product in cart page
	...	AND 	header_menu.Select on back button in header menu

Switch to application web view context
	${webview_2}= 	AppiumLibrary.Get Contexts
	${webview}= 	Evaluate 	[w for w in $webview_2 if 'WEBVIEW' in w]
	Set Test Variable  ${application_context}  @{webview}[-1]
	AppiumLibrary.Switch To Context 	@{webview}[-1]
