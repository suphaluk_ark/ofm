*** Keywords ***
Search and select product item by SKU number
	[Arguments] 	${product}
	header_menu.Select on search button
	search_page.Search product in search page 	${product}
	search_page.Select product on the first search result
	product_page.Display product page 	&{${product}}[product_name] 	&{${product}}[price] 	&{${product}}[product_sku]