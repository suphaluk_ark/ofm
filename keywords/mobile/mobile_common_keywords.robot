*** Settings ***
Resource 	${CURDIR}/mobile_imports.robot
Resource 	${CURDIR}/../../resources/imports.robot
Variables 	${CURDIR}/../../resources/common_configs.yaml

*** Variables ***
${testdata_filepath} 	${CURDIR}/../../resources/testdata/

*** Keywords ***
Mobile global init
	Log to console 	currentDirectoy ${CURDIR}
	Import Variables 	${testdata_filepath}${ENV.lower()}${/}test_data.yaml
	Set Global Variable 	${language} 	${language.lower()}
	Import Variables 	${testdata_filepath}common${/}mobile_translation_${language}.yaml

Open Android application
	AppiumLibrary.Open Application




