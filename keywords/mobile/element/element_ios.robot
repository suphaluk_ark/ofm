*** Variables ***
&{headerMenu}   label=chain=**/*[`name CONTAINS '{label}'`]
...     hamburger=chain=**/*[`name == 'ic menu'`]
...     img_logo=
...     btn_search=chain=**/XCUIElementTypeButton[`name == "ic search"`]
...     btn_cart=chain=**/XCUIElementTypeOther[`name == "OFMNavBar"`]/XCUIElementTypeButton[`name == "ic cart"`]
...     btn_menu=
...     btn_login=
...     btn_logout=
...     lbl_product_qty=chain=**/XCUIElementTypeOther[`name == "OFMNavBar"`]/XCUIElementTypeStaticText[`name == "{qty}"`]
...     btn_back=chain=**/*[`name == 'ic back'`]

&{loginPage}    label=
...     rdo_register=
...     rdo_login=
...     txt_email=chain=**/*[`name CONTAINS[cd] 'firstname'`][2]
...     txt_pwd=chain=**/*[`name CONTAINS[cd] 'firstname'`][1]
...     btn_login=chain=**/*[`name == '{label}'`]
...     btn_facebook=chain=**/*[`name CONTAINS[cd] '${mobile_login}[facebook_login]'`]
...     txt_facebook_username=id=m_login_email
...     txt_facebook_password=id=m_login_password
...     btn_facebook_login=chain=**/XCUIElementTypeButton[`name == '${mobile_login}[btn_login]'`]
...     btn_facebook_login_web=xpath=//button[@value='${mobile_login}[btn_login]']

&{search_page} 	btn_search=
...     txt_search=chain=**/XCUIElementTypeSearchField
...     modal_search_result=
...     img_progress_bar=
...     lal_product=chain=**/XCUIElementTypeStaticText[`name CONTAINS '{product_name}'`]

&{left_menu} 	btn_back=
...     screen_window=chain=**/XCUIElementTypeWindow
...     screen_menu=chain=**/XCUIElementTypeOther[`name == 'MenuTable'`]

&{product_page} 	lbl_product_name=
... 	lbl_product_view_price=
... 	btn_add_to_cart=chain=**/XCUIElementTypeButton[`name == "&{mobile_common}[add_to_cart]"`]
... 	btn_remove_quantity=
... 	btn_add_quantity=
... 	txt_quantity=chain=**/*[`name == 'AddToCartBar'`]/**/XCUIElementTypeStaticText[`name == '{qty}'`]
... 	lbl_sku=
...     btn_popup_ok=chain=**/*[`name == '${mobile_common}[ok]'`]
...     txt_popup_qty=chain=**/XCUIElementTypeOther[$name == '${mobile_common}[quantity_amount]'$]/**/XCUIElementTypeTextField

&{order_total}  btn_checkout=chain=**/XCUIElementTypeButton[`name == '${mobile_common.continue_payment.upper()}'`]

&{checkout}     btn_pickup_at_store=chain=**/XCUIElementTypeCell[`name BEGINSWITH "ShippingAddressSelector"`]/XCUIElementTypeButton[`name == "&{mobile_checkout}[pickup_at_store]"`]
...     btn_change_in_shipping=chain=**/XCUIElementTypeCell[`name BEGINSWITH "ShippingAddressSelector"`]/XCUIElementTypeButton[`name == "&{mobile_checkout}[change]"`]
...     txt_firstname_pickup=chain=**/XCUIElementTypeOther[`name =="Phone"`][4]
...     txt_lastname_pickup=chain=**/XCUIElementTypeOther[`name =="Phone"`][1]
...     txt_phone_pickup=chain=**/XCUIElementTypeOther[`name =="Phone"`][2]
...     txt_email_pickup=chain=**/XCUIElementTypeOther[`name =="Phone"`][3]
...     btn_branch=chain=**/XCUIElementTypeButton[`name == '&{mobile_checkout}[select_pickup]'`]
...     txt_search_pickup=chain=**/XCUIElementTypeSearchField
...     btn_allow_location=chain=**/XCUIElementTypeButton[`name == '&{mobile_checkout}[allow]'`]
...     lbl_pickup_branch=chain=**/XCUIElementTypeCell[`name BEGINSWITH '{lbl_branch}'`]
...     btn_save=chain=**/XCUIElementTypeButton[`name == '&{mobile_checkout}[save]'`]
...     btn_change_in_billing=chain=**/XCUIElementTypeCell[`name == "TaxAddressSelector:3"`]/XCUIElementTypeButton[`name == "&{mobile_checkout}[change]"`][2]
...     txt_company_name_in_biiling=chain=**/XCUIElementTypeOther[`name =="Phone"`][4]
...     txt_tax_id_in_biiling=chain=**/XCUIElementTypeOther[`name =="Phone"`][1]
...     txt_company_address_in_billing=chain=**/XCUIElementTypeOther[`name =="Phone"`][2]
...     sel_province_in_billing=chain=**/XCUIElementTypeOther[`name =="Password"`][1]
...     sel_district_in_billing=chain=**/XCUIElementTypeOther[`name =="Email"`]
...     sel_sub_district_in_billing=chain=**/XCUIElementTypeOther[`name =="Password"`][2]
...     txt_postcode_in_billing=chain=**/XCUIElementTypeOther[`name =="Phone"`][3]
...     txt_checkout_email=chain=**/XCUIElementTypeCell[`name BEGINSWITH "GuestEmailInput"`]/XCUIElementTypeTextField
...     lbl_shipping_title=chain=**/XCUIElementTypeStaticText[`name == '${mobile_checkout}[shipping_address_title]'`]
...     lbl_billing_address=chain=**/*[`name BEGINSWITH 'TaxAddressSelector'`]/XCUIElementTypeStaticText[`name == '{billing_address}'`]
...     lbl_shipping_address=chain=**/*[`name BEGINSWITH 'ShippingAddressSelector'`]/XCUIElementTypeStaticText[`name == '{shipping_address}'`]

&{payment}  btn_credit_cart=chain=**/XCUIElementTypeStaticText[`name == '${mobile_common}[credit_card]'`]
...     lbl_choose_payment=chain=**/XCUIElementTypeStaticText[`name == '${mobile_payment_type}[choose_payment]'`]
...     btn_online_banking=chain=**/*[`name BEGINSWITH 'method p2c2p_123'`]/XCUIElementTypeStaticText[`name == '${mobile_payment_type}[online_banking]'`]
...     txt_telephone=chain=**/*[`name BEGINSWITH 'phoneInput'`]/XCUIElementTypeTextField
...     btn_bank=chain=**/*[`name CONTAINS[cd] '{bank}'`]
...     btn_hide_keyboard=chain=**/XCUIElementTypeButton[`name == 'Done'`]
...     btn_T1C_credit_card=chain=**/*[`name BEGINSWITH 'method theonepayment'`]/[`name == 'The 1 Credit Card'`]

&{2c2p}     btn_creditcard=css=.card > a
...     lbl_order_number=css=#payment-order > b
...     txt_credit_number=id=credit_card_number
...     sel_month=id=credit_card_expiry_month
...     sel_year=id=credit_card_expiry_year
...     txt_cvv=id=credit_card_cvv
...     txt_cardholder=id=credit_card_holder_name
...     lbl_expiry_month=css=#credit_card_expiry_month option[value="{month}"]
...     lbl_expiry_year=css=#credit_card_expiry_year option[value="{year}"]
...     lbl_payment_details=chain=**/XCUIElementTypeStaticText[`name == "Officemate"`]
...     btn_continue_payment=id=btnCCSubmit
...     lbl_otp=id=lblTestMode
...     txt_otp=id=txnOTP
...     btn_proceed=id=btnProceed
...     lbl_order_amount=xpath=//div[@class='detail']//b[contains(text(),'{amount}')]
...     lbl_phone_number_123=chain=**/XCUIElementTypeTextField[`value == '{phone_number}'`]
...     btn_next_123=chain=**/XCUIElementTypeButton[`name == '${mobile_common}[next]'`]
...     lbl_amount_123=chain=**/XCUIElementTypeStaticText[`name == '{amount}'`]
...     btn_back_123=chain=**/XCUIElementTypeButton[`name == '${mobile_common}[back_from_123]'`]

&{mobile_thankyou_page}  lbl_thankyou_common=chain=**/XCUIElementTypeStaticText[`name == "{label}"`]

&{cart_page}    lbl_product_name=chain=**/XCUIElementTypeCell[`name BEGINSWITH "ProductCart"`]/*[`name == "text_product_name" AND label == '{label}'`]
...     lbl_product_sku=chain=**/XCUIElementTypeCell[`name BEGINSWITH "ProductCart"`]/*[`name == "text_product_sku" AND label BEGINSWITH '{label}'`]
...     lbl_product_price_excl=chain=**/XCUIElementTypeCell[`name BEGINSWITH "ProductCart"`]/*[`name == "text_product_price" AND label == '{label}'`]
...     lbl_product_price_incl=chain=**/XCUIElementTypeCell[`name BEGINSWITH "ProductCart"`]/*[`name == "text_product_price_total" AND label == '{label}'`]
...     lbl_inlu_vat=chain=**/XCUIElementTypeCell[`name BEGINSWITH "ProductCart"`]/*[`name == "text_product_include_vat" AND label == '{label}'`]
...     lbl_cart_title=chain=**/XCUIElementTypeStaticText[`name == '${mobile_common}[shopping_cart]'`]
... 	btn_remove=chain=**/*[`name == 'button_delete_product'`]

&{order_summary}    lbl_total_price_excl_vat=chain=**/XCUIElementTypeCell[`name BEGINSWITH "AllPrice"`]/*[`name == '{label}'`]
...     lbl_total_vat=chain=**/XCUIElementTypeCell[`name BEGINSWITH "AllPrice"`]/*[`name == '{label}'`]
...     lbl_delivery_fee=chain=**/XCUIElementTypeCell[`name BEGINSWITH "AllPrice"`]/*[`name == '{label}'`]
...     lbl_surcharge_fee=chain=**/XCUIElementTypeCell[`name BEGINSWITH "AllPrice"`]/*[`name == '{label}'`]
...     lbl_grand_total=chain=**/XCUIElementTypeCell[`name BEGINSWITH "TotalPrice"`]/*[`name == '{lable}'`]
...     lbl_grand_total_thank_you=chain=**/XCUIElementTypeCell[`name BEGINSWITH "GrandTotalCell"`]/*[`name == '{lable}'`]
...     screen_cart_table=chain=**/XCUIElementTypeOther[`name == 'CartTable'`]
...     screen_payment_table=chain=**/XCUIElementTypeOther[`name == 'CheckoutPaymentTable'`]
...     screen_thankyou_table=chain=**/XCUIElementTypeOther[`name == 'CheckoutCompletedTable'`]