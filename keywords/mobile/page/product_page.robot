*** Keywords ***
Display product name in product page
	[Arguments] 	${expected_text}
	CommonMobileKeywords.Verify Elements Are Visible 	&{product_page}[lbl_product_name]
	AppiumLibrary.Element Text Should Be 	&{product_page}[lbl_product_name] 	${expected_text}

Display product view price in product page
	[Arguments] 	${expected_text}
	CommonMobileKeywords.Verify Elements Are Visible 	&{product_page}[lbl_product_view_price]
	${expected_text}= 	Convert price to total amount format 	${expected_text}
	AppiumLibrary.Element Text Should Be 	&{product_page}[lbl_product_view_price] 	฿${expected_text}

Display product quantity in product page
	[Arguments] 	${expected_text}=1
	CommonMobileKeywords.Verify Elements Are Visible 	&{product_page}[txt_quantity]
	AppiumLibrary.Element Text Should Be 	&{product_page}[txt_quantity] 	${expected_text}

Display add to cart button in product page
	CommonMobileKeywords.Verify Elements Are Visible 	&{product_page}[btn_add_to_cart]
	AppiumLibrary.Element Text Should Be 	&{product_page}[btn_add_to_cart] 	${mobile_common.add_to_cart.upper()}

Display product sku in product page
	[Arguments] 	${sku_number}
	CommonMobileKeywords.Verify Elements Are Visible 	&{product_page}[lbl_sku]
	AppiumLibrary.Element Text Should Be 	&{product_page}[lbl_sku] 	${mobile_common.sku.title()}: ${sku_number}

Select on add to cart button in product page
	CommonMobileKeywords.Click Element 	&{product_page}[btn_add_to_cart]

Select on remove product quantity in product page
	${before_remove}= 	Get product quantity in product page
	CommonMobileKeywords.Click Element 	&{product_page}[btn_remove_quantity]
	${after_remove}= 	Get product quantity in product page
	Should be True 	${${before_remove}-1}==${after_remove}

Select on add product quantity in product page
	${before_add}= 	Get product quantity in product page
	CommonMobileKeywords.Click Element 	&{product_page}[btn_add_quantity]
	${after_add}= 	Get product quantity in product page
	Should be True 	${${before_add}+1}==${after_add}

Get product quantity in product page
	CommonMobileKeywords.Verify Elements Are Visible	&{product_page}[txt_quantity]
	${qty}= 	AppiumLibrary.Get Text 	&{product_page}[txt_quantity]
	[Return] 	${qty}

Input product quantity in product page
	[Arguments] 	${product_quantity}=2
	CommonMobileKeywords.Verify Elements Are Visible 	&{product_page}[txt_quantity]
	AppiumLibrary.Clear Text 	&{product_page}[txt_quantity]
	CommonMobileKeywords.Input Text And Verify Input For Mobile Element 	&{product_page}[txt_quantity] 	${product_quantity}
	Sleep 	10 sec

Display product page
	[Arguments] 	${product_name} 	${product_price} 	${sku_number}
	Display product name in product page 	${product_name}
	Display product view price in product page 	${product_price}
	Display add to cart button in product page
	Display product quantity in product page
	Display product sku in product page 	${sku_number}

Input qty in qty popup
	[Arguments] 	${qty}
	CommonMobileKeywords.Verify Elements Are Visible 	${product_page}[txt_popup_qty]
	CommonMobileKeywords.Click Element 	${product_page}[txt_popup_qty]
	AppiumLibrary.Clear Text 	${product_page}[txt_popup_qty]
	AppiumLibrary.Input Text 	${product_page}[txt_popup_qty] 	${qty}

Select qty textbox in product page
	[Arguments] 	${qty}=1
	${locator}= 	CommonKeywords.Format Text  ${product_page}[txt_quantity] 	qty=${qty}
	CommonMobileKeywords.Click Element 	${locator}

Select OK on confirm button
	CommonMobileKeywords.Click Element  ${product_page}[btn_popup_ok]

Display qty value in product page
	[Arguments] 	${qty}=1
	${locator}= 	CommonKeywords.Format Text  ${product_page}[txt_quantity] 	qty=${qty}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Input qty in product page
	[Arguments] 	${qty}
	Select qty textbox in product page
	Input qty in qty popup 	${qty}
	Select OK on confirm button
