*** Keywords ***
Display thank you page
	[Arguments] 	${increment_id} 	${sku_numbers} 	${product_quantities}
	: FOR 	${text} 	IN 	${mobile_thankyou.message1}
	... 	${mobile_thankyou.message2}
	... 	${mobile_thankyou.order_summary.title}
	... 	${mobile_thankyou.order_summary.order_code}
	... 	${mobile_thankyou.order_summary.order_date}
	... 	${mobile_thankyou.order_summary.payment_type}
	... 	${mobile_thankyou.order_summary.order_status}
	... 	${mobile_thankyou.order_summary.the1card}
	... 	${mobile_thankyou.shipping.title}
	... 	${mobile_thankyou.billing.title}
	... 	${mobile_thankyou.payment_detail.total_price}
	... 	${mobile_thankyou.payment_detail.shipping_handling}
	... 	${mobile_thankyou.payment_detail.discount}
	... 	${mobile_thankyou.payment_detail.the1card}
	... 	${mobile_thankyou.payment_detail.grand_total}
	\  ${elem}= 	CommonKeywords.Format Text 	&{mobile_thankyou_elem}[lal_label] 	label=${text}
	\  CommonMobileKeywords.Verify Elements Are Visible 	${elem}
	Display order increment id in thank you page 	${increment_id}
	Display order date in thank you page 	${increment_id}
	### Bug :: payment type display with invalid lang (EN) should be (TH)
	# Display payment type in thank you page 	${increment_id}
	Display order status in thank you page 	${increment_id}
	Display payment details in thank you page 	${sku_numbers} 	${product_quantities}

Get increment id in thank you page
	${elem}= 	AppiumLibrary.Get Text  	&{mobile_thankyou_elem}[txt_incrementId]
	[Return] 	${elem}

Display order date in thank you page
	[Arguments] 	${increment_id}
	${date}= 	api_order_details.Get create_at with +7 utc format 	${increment_id}
	${date}= 	Convert response to +7 utc datetime for mobile application 	${date}
	${elem}= 	CommonKeywords.Format Text 	&{mobile_thankyou_elem}[lal_label] 	label=${date}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Display order increment id in thank you page
	[Arguments] 	${increment_id}
	${elem}= 	CommonKeywords.Format Text 	&{mobile_thankyou_elem}[lal_label] 	label=${increment_id}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Display product details by SKU number in thankyou page
	[Arguments] 	${sku_number} 	${product_qty}=1
	${elem_product_sku}= 	AppiumLibrary.Get Webelements 	&{mobile_thankyou_elem}[lbl_product_sku]
	${mobile_len}= 	Get Length 	${elem_product_sku}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_product_sku]
	\  ${get_sku_number}= 	AppiumLibrary.Get Text 	@{elem_product_sku}[${index}]
	\  Run Keyword If 	$sku_number == $get_sku_number 	Verify product details in thankyou page
	... 	&{${sku_number}}[product_name]
	... 	&{${sku_number}}[product_sku]
	... 	&{${sku_number}}[price]
	... 	${product_qty}
	... 	${index}

Verify product details in thankyou page
	[Arguments] 	${product_name} 	${product_sku} 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product name in thankyou page 	${product_name}
	Verify product sku in thankyou page 	${product_sku} 	${product_list_index}
	Verify product view price in thankyou page 	${product_price} 	${product_list_index}
	Verify product sub total in thankyou page 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product total price in thankyou page 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product quantity in thankyou page 	${product_quantity} 	${product_list_index}

Verify product name in thankyou page
	[Arguments] 	${product_name}
	${elem}= 	CommonKeywords.Format Text 	&{mobile_thankyou_elem}[lbl_product_name] 	text=${product_name}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Verify product sku in thankyou page
	[Arguments] 	${product_sku} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_product_sku]
	${elems_product_sku}= 	AppiumLibrary.Get Webelements 	&{mobile_thankyou_elem}[lbl_product_sku]
	AppiumLibrary.Element Text Should Be 	@{elems_product_sku}[${product_list_index}] 	${product_sku}

Verify product view price in thankyou page
	[Arguments] 	${product_price} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_product_view_price]
	${elems_product_view_price}= 	AppiumLibrary.Get Webelements 	&{mobile_thankyou_elem}[lbl_product_view_price]
	${product_view_price}= 	calculation.Convert price to total amount format 	${product_price}
	AppiumLibrary.Element Text Should Be 	@{elems_product_view_price}[${product_list_index}] 	฿${product_view_price}

Verify product sub total in thankyou page
	[Arguments] 	${product_price} 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_sub_total]
	${elems_product_sub_total}= 	AppiumLibrary.Get Webelements 	&{mobile_thankyou_elem}[lbl_sub_total]
	calculation.Summarize price with quantity 	${product_quantity} 	${product_price}
	AppiumLibrary.Element Text Should Be 	@{elems_product_sub_total}[${product_list_index}] 	฿${price_with_quantity_format}

Verify product total price in thankyou page
	[Arguments] 	${product_price} 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_price_incVat]
	${elems_product_total_price}= 	AppiumLibrary.Get Webelements 	&{mobile_thankyou_elem}[lbl_price_incVat]
	calculation.Summarize price with VAT 	${product_price}
	calculation.Summarize price with quantity 	${product_quantity}
	AppiumLibrary.Element Text Should Be 	@{elems_product_total_price}[${product_list_index}] 	( ฿${price_with_quantity_format} incl. &{mobile_common}[vat] )

Verify product quantity in thankyou page
	[Arguments] 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[txt_product_quantity]
	${elems_product_quantity}= 	AppiumLibrary.Get Webelements 	&{mobile_thankyou_elem}[txt_product_quantity]
	${product_quantity}= 	Evaluate 	str($product_quantity)
	AppiumLibrary.Element Text Should Be 	@{elems_product_quantity}[${product_list_index}] 	${mobile_thankyou.order_list.quantity} ${product_quantity}

Display shipping address in thank you page
	[Arguments] 	${firstname}
	... 	${lastname}
	... 	${address_name}
	... 	${region}
	... 	${district}
	... 	${sub_district}
	... 	${postcode}
	... 	${phone}
	${elem_txt_name}=    CommonKeywords.Format Text    &{mobile_thankyou_elem}[txt_shipping_name]     text=${firstname}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem_txt_name}
	${get_cust_name}=     AppiumLibrary.Get Text    ${elem_txt_name}
	${edited_shipping_name}=	BuiltIn.Set Variable    ${firstname} ${lastname}
	Should Match Regexp     ${edited_shipping_name}    ${get_cust_name}
	${elem_txt_address}=    CommonKeywords.Format Text    &{mobile_thankyou_elem}[txt_shipping_address]     text=${address_name}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem_txt_address}
	${get_cust_address}=     AppiumLibrary.Get Text    ${elem_txt_address}
	${edited_shipping_address}= 	BuiltIn.Set Variable    ${address_name}, ${sub_district}, ${district}, ${region}, ${postcode}
	Should Match Regexp     ${edited_shipping_address}    ${get_cust_address}
	${elem_txt_phone}=    CommonKeywords.Format Text    &{mobile_thankyou_elem}[txt_shipping_phone]     text=${phone}
	CommonMobileKeywords.Verify Elements Are Visible	${elem_txt_phone}
	${get_cust_phone}=     AppiumLibrary.Get Text    ${elem_txt_phone}
	${edited_shipping_phone}= 	BuiltIn.Set Variable    ${mobile_common.phone_number} ${phone}
	Should Match Regexp     ${edited_shipping_phone}    ${get_cust_phone}

Display billing address in thank you page
    [Arguments]     ${firstname}
    ...     ${lastname}
    ...     ${address_name}
    ...     ${region}
    ...     ${district}
    ...     ${sub_district}
    ...     ${postcode}
    ...     ${phone}
    ...     ${tax_id}
	${elem_txt_name}=    CommonKeywords.Format Text    &{mobile_thankyou_elem}[txt_shipping_name]     text=${firstname}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_txt_address}
	${get_cust_name}=     AppiumLibrary.Get Text    ${elem_txt_address}
	${edited_shipping_name}=	BuiltIn.Set Variable    ${firstname} ${lastname}
	Should Match Regexp     ${edited_shipping_name}    ${get_cust_name}
	${elem_txt_address}=    CommonKeywords.Format Text    &{mobile_thankyou_elem}[txt_shipping_address]     text=${address_name}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_txt_address}
	${get_cust_address}=     AppiumLibrary.Get Text    ${elem_txt_address}
	${edited_shipping_address}= 	BuiltIn.Set Variable    ${address_name}, ${sub_district}, ${district}, ${region}, ${postcode}
	Should Match Regexp     ${edited_shipping_address}    ${get_cust_address}
	${elem_txt_phone}=    CommonKeywords.Format Text    &{mobile_thankyou_elem}[txt_shipping_phone]     text=${phone}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_txt_phone}
	${get_cust_phone}=     AppiumLibrary.Get Text    ${elem_txt_phone}
	${edited_shipping_phone}= 	BuiltIn.Set Variable    ${mobile_common.phone_number} ${phone}
	Should Match Regexp     ${edited_shipping_phone}    ${get_cust_phone}
	${elem_txt_taxId}=    CommonKeywords.Format Text    &{mobile_thankyou_elem}[txt_shipping_name]     text=${tax_id}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_txt_taxId}
    ${get_cust_vatId}=  AppiumLibrary.Get Text      ${elem_txt_taxId}
    ${edited_tax_id}=     BuiltIn.Set Variable    ${mobile_common.tax_id} ${tax_id}
    Should Match Regexp     ${get_cust_vatId}   ${edited_tax_id}

Display payment details in thank you page
	[Arguments] 	${sku_numbers} 	${product_quantities}
	Get total price from product details 	${sku_numbers} 	${product_quantities}
	Verify total price in thank you page
	Verify shipping & handling in thank you page
	Verify discount in thank you page
	Verify the 1 card redemption in thank you page
	Verify grand total in thank you page

Verify total price in thank you page
	${total_price}= 	Convert price to total amount format 	${total_price}
	${elem}= 	CommonKeywords.Format Text 	&{mobile_thankyou_elem}[lbl_total_price] 	label=฿${total_price}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Verify shipping & handling in thank you page
	${shipping}= 	Set Variable If 	${total_price}<=${499} 	${50}
	...	ELSE 	${0}
	${shipping}= 	Convert price to total amount format 	${shipping}
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_shipping_handling]
	AppiumLibrary.Element Text Should Be 	&{mobile_thankyou_elem}[lbl_shipping_handling] 	฿${shipping}

Verify discount in thank you page
	[Documentation] 	TBC
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_discount]
	AppiumLibrary.Element Text Should Be 	&{mobile_thankyou_elem}[lbl_discount] 	฿${price_with_vat_format}

Verify the 1 card redemption in thank you page
	[Documentation] 	TBC
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_the_1_card]
	AppiumLibrary.Element Text Should Be 	&{mobile_thankyou_elem}[lbl_the_1_card] 	฿${price_with_vat_format}

Verify grand total in thank you page
	[Documentation] 	TBC
	CommonMobileKeywords.Verify Elements Are Visible 	&{mobile_thankyou_elem}[lbl_grand_total]
	AppiumLibrary.Element Text Should Be 	&{mobile_thankyou_elem}[lbl_grand_total] 	฿${price_with_vat_format}

Select on countinue shopping button in thankyou page
	CommonMobileKeywords.Click Element 	&{mobile_thankyou_elem}[btn_continue_shop]

Display thank you message in thank you page
	FOR 	${label} 	IN 	${mobile_thankyou}[message1] 	
	... 	${mobile_thankyou}[message2]
		${locator}= 	CommonKeywords.Format Text  ${mobile_thankyou_page}[lbl_thankyou_common] 	label=${label}
		CommonMobileKeywords.Verify Elements Are Visible 	${locator}
	END

Retry Display thank you message in thank you page
	Wait Until Keyword Succeeds  ${global_retry_time} x 	${global_retry_in_sec} sec 	Display thank you message in thank you page

Display order id in thank you page
	[Arguments] 	${order_number}
	${locator}= 	CommonKeywords.Format Text 	${mobile_thankyou_page}[lbl_thankyou_common] 	label=${order_number}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Display order status in thank you page
	[Arguments] 	${order_status}
	${locator}= 	CommonKeywords.Format Text 	${mobile_thankyou_page}[lbl_thankyou_common] 	label=${order_status}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Display payment type in thank you page
	[Arguments] 	${payment_type}
	${locator}= 	CommonKeywords.Format Text 	${mobile_thankyou_page}[lbl_thankyou_common] 	label=${payment_type}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Display order_id, payment_type, and status in thank you page
	[Arguments] 	${order_id} 	${payment_method} 	${order_status} 	${switch_to_native}=${True}
	Run Keyword If  '${switch_to_native}'=='${True}' 	AppiumLibrary.Switch To Context 	${native_app}
	mobile_thankyou.Retry Display thank you message in thank you page 	
	mobile_thankyou.Display order id in thank you page 	${order_id}
	mobile_thankyou.Display payment type in thank you page 	${payment_method}
	mobile_thankyou.Display order status in thank you page 	${order_status}















