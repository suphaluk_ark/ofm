*** Keywords ***
Select on back button in left menu
	CommonMobileKeywords.Click Element 	&{left_menu}[btn_back]

Select on sign out button
	CommonMobileKeywords.Click Element 	&{left_menu}[btn_sign_out]

Display log in menu
	${elem}= 	CommonKeywords.Format Text 	&{left_menu}[label] 	label=&{mobile_common}[lbl_login]
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Swipe left to close left menu
	[Arguments] 	${duration}=500
	CommonMobileKeywords.Verify Elements Are Visible 	&{left_menu}[screen_menu]
	${location_menu}= 	AppiumLibrary.Get Element Size 	&{left_menu}[screen_menu]
	${location_window}= 	AppiumLibrary.Get Element Size 	&{left_menu}[screen_window]
	${location_y}= 	Evaluate 	${location_window}[width]/2
	${location_x}= 	Evaluate 	${location_menu}[width]/2
	AppiumLibrary.Swipe 	${location_window}[width] 	${location_y} 	${location_x} 	${location_y} 	${duration}