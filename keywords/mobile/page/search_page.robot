*** Keywords ***
Select on search button in search page
	CommonMobileKeywords.Click Element 	 &{search_page}[btn_search]

Input search value in search page
	[Arguments] 	${value}
	CommonMobileKeywords.Verify Elements Are Visible  &{search_page}[txt_search]
	AppiumLibrary.Clear Text 	&{search_page}[txt_search]
	CommonMobileKeywords.Input Text And Verify Input For Mobile Element	&{search_page}[txt_search] 	${value}

Search product in search page
	[Arguments] 	${value}
	Select on search button in search page
	Input search value in search page 	${value}
	CommonMobileKeywords.Verify Elements Are Visible 	&{search_page}[modal_search_result]

Select product on the first search result
	[Documentation] 	TODO
	${search_box_location}= 	AppiumLibrary.Get Element Location 	&{search_page}[txt_search]
	${search_box_size}= 	AppiumLibrary.Get Element Size 	&{search_page}[txt_search]
	${elem}= 	Evaluate 	&{search_box_location}[y] + (&{search_box_size}[height] + &{search_box_size}[height]/2)
	AppiumLibrary.Click Element At Coordinates 	coordinate_X=&{search_box_location}[x] 	coordinate_Y=${elem}

Select product by product name
	[Arguments]	${product_sku}
	${locator}=	CommonKeywords.Format Text  &{search_page}[lal_product]  product_name=&{${product_sku}}[product_name]
	CommonMobileKeywords.Click Element	${locator}

Search and select product
	[Arguments] 	${sku_number}
	search_page.Input search value in search page 	${sku_number}
	search_page.Select product by product name 	${sku_number}