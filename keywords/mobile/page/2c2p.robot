*** Keywords ***
Select on credit/debit card number
	CommonMobileKeywords.Click Element 	&{2c2p}[btn_creditcard]

Input credit card number in 2c2p page
	[Arguments] 	${card_number}
	CommonMobileKeywords.Verify Elements Are Visible 	&{2c2p}[txt_credit_number]
	AppiumLibrary.Input Text 	&{2c2p}[txt_credit_number]  ${card_number}

Select expiry month in 2c2p page
	[Arguments] 	${expiry_month}
	CommonMobileKeywords.Click Element  &{2c2p}[sel_month]
	${locator}= 	CommonKeywords.Format Text  &{2c2p}[lbl_expiry_month] 	month=${expiry_month}
	CommonMobileKeywords.Click Element 	${locator}

Select expiey year in 2c2p page
	[Arguments] 	${expiry_year}
	CommonMobileKeywords.Click Element 	&{2c2p}[sel_year]
	${locator}= 	CommonKeywords.Format Text 	&{2c2p}[lbl_expiry_year] 	year=${expiry_year}
	CommonMobileKeywords.Click Element 	${locator}

Input cvv in 2c2p page
	[Arguments] 	${card_cvv}
	CommonMobileKeywords.Verify Elements Are Visible  &{2c2p}[txt_cvv]
	AppiumLibrary.Input Text 	&{2c2p}[txt_cvv] 	${card_cvv}

Input card holder name in 2c2p page
	[Arguments] 	${card_holder}
	CommonMobileKeywords.Verify Elements Are Visible 	&{2c2p}[txt_cardholder]
	AppiumLibrary.Input Text 	&{2c2p}[txt_cardholder] 	${card_holder}

Select on continue payment button in 2c2p page
	CommonMobileKeywords.Click Element 	&{2c2p}[btn_continue_payment]

Submit credit card
	[Arguments] 	${card_number}=&{credit_card}[number]
	... 	${card_holder}=&{credit_card}[holder]
	... 	${card_cvv}=&{credit_card}[cvv]
	... 	${expiry_month}=&{credit_card}[month]
	... 	${expiry_year}=&{credit_card}[year]
	Select on credit/debit card number
	${order_number}= 	Wait Until Keyword Succeeds  ${global_retry_time} x  ${global_retry_in_sec} sec 	Get order number in 2c2p page
	Input credit card number in 2c2p page 	${card_number}
	Select expiry month in 2c2p page 	${expiry_month}
	Select expiey year in 2c2p page 	${expiry_year}
	Input cvv in 2c2p page 	${card_cvv}
	Input card holder name in 2c2p page 	${card_holder}
	Select on continue payment button in 2c2p page
	[Return] 	${order_number}

Submit T1C credit card
	[Arguments] 	${card_number}=&{credit_card}[t1c_number]
	... 	${card_holder}=&{credit_card}[holder]
	... 	${card_cvv}=&{credit_card}[cvv]
	... 	${expiry_month}=&{credit_card}[month]
	... 	${expiry_year}=&{credit_card}[year]
	${order_number}= 	Wait Until Keyword Succeeds  ${global_retry_time} x  ${global_retry_in_sec} sec 	Get order number in 2c2p page
	Input credit card number in 2c2p page 	${card_number}
	Select expiry month in 2c2p page 	${expiry_month}
	Select expiey year in 2c2p page 	${expiry_year}
	Input cvv in 2c2p page 	${card_cvv}
	Input card holder name in 2c2p page 	${card_holder}
	Select on continue payment button in 2c2p page
	[Return] 	${order_number}

Get OTP in 2c2p page
	CommonMobileKeywords.Verify Elements Are Visible 	&{2c2p}[lbl_otp]
	${otp}= 	AppiumLibrary.Get Text 	&{2c2p}[lbl_otp]
	${otp}= 	Get Regexp Matches 	${otp} 	=\\s([0-9]+) 	1
	[Return] 	@{otp}[0]

Get order number in 2c2p page
	CommonMobileKeywords.Verify Elements Are Visible 	&{2c2p}[lbl_order_number]
	${order_number}= 	AppiumLibrary.Get Text 	&{2c2p}[lbl_order_number]
	[Return] 	${order_number}

Input OTP in 2c2p page
	[Arguments] 	${otp}
	CommonMobileKeywords.Verify Elements Are Visible 	&{2c2p}[txt_otp]
	AppiumLibrary.Input Password 	&{2c2p}[txt_otp] 	${otp}

Select on proceed button in 2c2p page
	CommonMobileKeywords.Click Element 	&{2c2p}[btn_proceed]

Procedd OTP in 2c2p page
	${otp}= 	Get OTP in 2c2p page
	Input OTP in 2c2p page 	${otp}
	Select on proceed button in 2c2p page

Display product details as officemate in 2c2p
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec 	CommonMobileKeywords.Verify Elements Are Visible 	${2c2p}[lbl_payment_details]


Display payment amount in 2c2p
	[Arguments] 	${amount}
	${locator}= 	CommonKeywords.Format Text  ${2c2p}[lbl_order_amount] 	amount=${amount}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Display phone number in 2c2p_123 page
	[Arguments] 	${phone_number}=0255555555
	${locator}= 	CommonKeywords.Format Text  ${2c2p}[lbl_phone_number_123] 	phone_number=${phone_number}
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec 	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Select next button in 2c2p_123 page
	CommonMobileKeywords.Click Element 	${2c2p}[btn_next_123]

Display payment amount in 2c2p_123 page
	[Arguments] 	${amount}
	${locator}= 	CommonKeywords.Format Text  ${2c2p}[lbl_amount_123] 	amount=${amount}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Select back button in 2c2p_123 page
	CommonMobileKeywords.Click Element  ${2c2p}[btn_back_123]