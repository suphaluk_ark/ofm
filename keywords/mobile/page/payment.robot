*** Keywords ***
Select on online credit card payment button
	CommonMobileKeywords.Click Element 	&{payment}[btn_credit_cart]

Select on COD payment method
	${elem}= 	CommonKeywords.Format Text 	&{payment}[lal_scroll_to_label] 	label=&{mobile_common}[cod]
	CommonMobileKeywords.Click Element 	${elem}

Expand the 1 card section in payment page
	CommonMobileKeywords.Click Element 	&{payment}[btn_expand_the1card]

Input the 1 card username in payment page
	[Arguments] 	${username}
	CommonMobileKeywords.Verify Elements Are Visible 	&{payment}[txt_email_the1card]
	CommonMobileKeywords.Input Text And Verify Input For Mobile Element 	&{payment}[txt_email_the1card] 	${username}

Input the 1 card password in payment page
	[Arguments] 	${password}
	CommonMobileKeywords.Verify Elements Are Visible 	&{payment}[txt_password_the1card]
	CommonMobileKeywords.Input Text And Verify Input For Mobile Element 	&{payment}[txt_password_the1card] 	${password}

Select log in the 1 card in payment page
	CommonMobileKeywords.Click Element 	&{payment}[btn_login_the1card]

Sign in the 1 card member
	[Arguments] 	${username} 	${password}
	Expand the 1 card section in payment page
	Input the 1 card username in payment page 	${username}
	Input the 1 card password in payment page 	${password}
	Select log in the 1 card in payment page

Select Online Banking button
	CommonMobileKeywords.Click Element 	${payment}[btn_online_banking]

Input phone number in payment details page
	[Arguments] 	${phone_number}
	CommonMobileKeywords.Verify Elements Are Visible 	${payment}[txt_telephone]
	AppiumLibrary.Input Text 	${payment}[txt_telephone] 	${phone_number}
	Select done button for hide keyboard

Select Banking in payment details page
	[Arguments] 	${bank_name}
	${locator}= 	CommonKeywords.Format Text  ${payment}[btn_bank] 	bank=${bank_name}
	CommonMobileKeywords.Click Element 	${locator}

Payment by online banking
	[Arguments] 	${phone_number}=0255555555 	${bank_name}=${banking_name}[bangkok]
	Select Online Banking button
	Input phone number in payment details page 	${phone_number}
	Select Banking in payment details page 	${bank_name}

Select T1C credit card button
	CommonMobileKeywords.Click Element  ${payment}[btn_T1C_credit_card]
	
Display banking name
	: FOR 	${text} 	IN 	&{banking_name}[krungsri]
	... 	&{banking_name}[ktb]
	... 	&{banking_name}[tmb]
	... 	&{banking_name}[thanachart]
	... 	&{banking_name}[cimb]
	... 	&{banking_name}[bangkok]
	... 	&{banking_name}[scb]
	... 	&{banking_name}[uob]
	... 	&{banking_name}[kbank]
	\  ${elem}= 	CommonKeywords.Format Text 	&{payment}[lal_scroll_to_banking_name] 	text=${text}
	\  CommonMobileKeywords.Verify Elements Are Visible 	${elem}
	${ele}= 	CommonKeywords.Format Text 	&{payment}[lal_title_detail] 	text=&{banking_name}[desc]
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}
	${ele}= 	CommonKeywords.Format Text 	&{payment}[lal_title] 	text=&{banking_name}[title]
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Retry display choose payment method title
	Wait Until Keyword Succeeds  ${global_retry_time} x 	${global_retry_in_sec} sec 	CommonMobileKeywords.Verify Elements Are Visible 	&{payment}[lbl_choose_payment]

Select done button for hide keyboard
	CommonMobileKeywords.Click Element  ${payment}[btn_hide_keyboard]
