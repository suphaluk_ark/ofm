*** Keywords ***
Select on select shipping address button in checkout page
	CommonMobileKeywords.Click Element 	&{checkout}[btn_select_address]

Select billing address button in checkout page
    CommonMobileKeywords.Click Element  &{checkout}[lbl_billing_address]
    CommonMobileKeywords.Click Element  &{checkout}[btn_billing_address]

Verify shipping address on checkout page
	[Arguments] 	${firstname}
	... 	${lastname}
	... 	${address_name}
	... 	${region}
	... 	${district}
	... 	${sub_district}
	... 	${postcode}
	... 	${phone}
	${elem_txt_address}=    CommonKeywords.Format Text  &{delivery_details}[txt_address]     text=${firstname}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem_txt_address}
	${get_cust_address}=     AppiumLibrary.Get Text 	${elem_txt_address}
	${edited_shipping_address}=	BuiltIn.Set Variable    ${firstname} ${lastname}${\n}${address_name}, ${sub_district}, ${district}, ${region}, ${postcode}${\n}${phone}
	Should Match Regexp     ${edited_shipping_address}    ${get_cust_address}

Verify billing address on checkout page
    [Arguments]     ${firstname}
    ...     ${lastname}
    ...     ${address_name}
    ...     ${region}
    ...     ${district}
    ...     ${sub_district}
    ...     ${postcode}
    ...     ${phone}
    ...     ${tax_id}
    ${elem_txt_address}=    CommonKeywords.Format Text  &{delivery_details}[txt_address]     text=${district}
    CommonMobileKeywords.Verify Elements Are Visible    ${elem_txt_address}
    ${get_billing_address}=    AppiumLibrary.Get Text      ${elem_txt_address}
    ${elem_vat_id}=     CommonKeywords.Format Text  &{delivery_details}[txt_taxId]     text=${tax_id}
    ${get_tax_id}=  AppiumLibrary.Get Text      ${elem_vat_id}
    ${edited_billing_address}=    BuiltIn.Set Variable    ${firstname} ${lastname}${\n}${address_name}, ${sub_district}, ${district}, ${region}, ${postcode}${\n}${phone}
    ${edited_vat_id}=   BuiltIn.Set Variable    ${tax_id}
    Should Match Regexp     ${edited_billing_address}    ${get_billing_address}
    Should Match Regexp     ${edited_vat_id}   ${get_tax_id}

Select pickup at store in checkout page
    CommonMobileKeywords.Click Element  &{checkout}[btn_pickup_at_store]
    Set Test Variable   ${isNoSurchargeFee}    ${True}
    Set Test Variable   ${isNoDeliveryFee}    ${True}

Select change button in shipping section
    CommonMobileKeywords.Click Element  &{checkout}[btn_change_in_shipping]

Input firstname in pickup shipping section
    [Arguments]     ${firstname}
    CommonMobileKeywords.Verify Elements Are Visible    &{checkout}[txt_firstname_pickup]
    AppiumLibrary.Input Text   &{checkout}[txt_firstname_pickup]    ${firstname}

Input lastname in pickup shipping section
    [Arguments]     ${lastname}
    CommonMobileKeywords.Verify Elements Are Visible    &{checkout}[txt_lastname_pickup]
    AppiumLibrary.Input Text     &{checkout}[txt_lastname_pickup]  ${lastname}

Input phone in pickup shipping section
    [Arguments]     ${phone}
    CommonMobileKeywords.Verify Elements Are Visible    &{checkout}[txt_phone_pickup]
    AppiumLibrary.Input Text     &{checkout}[txt_phone_pickup]   ${phone}

Input email in pickup shipping section
    [Arguments]     ${email}
    CommonMobileKeywords.Verify Elements Are Visible    &{checkout}[txt_email_pickup]
    AppiumLibrary.Input Text     &{checkout}[txt_email_pickup]   ${email}

Input pickup shipping address section
    [Arguments]     ${firstname}=&{shipping_address_${language}}[firstname]
    ...     ${lastname}=&{shipping_address_${language}}[lastname]
    ...     ${phone}=&{shipping_address_${language}}[phone]
    ...     ${email}=&{shipping_address_${language}}[email]
    Input firstname in pickup shipping section  ${firstname}
    Input lastname in pickup shipping section   ${lastname}
    Input phone in pickup shipping section  ${phone}
    Input email in pickup shipping section  ${email}

Select pickup branch button
    CommonMobileKeywords.Click Element  &{checkout}[btn_branch]

Select pickup at store
    [Arguments]     ${search_value}=12130   ${branch_name}=ฟิวเจอร์พาร์ค
    Select pickup branch button
    Run Keyword And Ignore Error    Select on allow application location
    Search pickup at store  ${search_value}
    Select pickup branch    ${branch_name}

Select on allow application location
    CommonMobileKeywords.Click Element  &{checkout}[btn_allow_location]

Search pickup at store 
    [Arguments]     ${search_value}
    CommonMobileKeywords.Verify Elements Are Visible  &{checkout}[txt_search_pickup]
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element     &{checkout}[txt_search_pickup]  ${search_value}

Select pickup branch
    [Arguments]     ${branch_name}
    ${locator}=     CommonKeywords.Format Text  &{checkout}[lbl_pickup_branch]  lbl_branch=${branch_name}
    CommonMobileKeywords.Click Element  ${locator}

Select save button
    CommonMobileKeywords.Click Element     &{checkout}[btn_save]

Pick up at store and input shipping details
    [Arguments]     ${search_value}=10150   ${branch_name}=${mobile_common.store_location.central_rama2}
    Select pickup at store in checkout page
    Select change button in shipping section
    Input pickup shipping address section
    Select pickup at store     ${search_value}     ${branch_name}
    Select save button

Select change button in billing section
    CommonMobileKeywords.Click Element  &{checkout}[btn_change_in_billing]

Input company name billing section
    [Arguments]     ${company_name}
    CommonMobileKeywords.Verify Elements Are Visible  &{checkout}[txt_company_name_in_biiling]
    AppiumLibrary.Input Text     &{checkout}[txt_company_name_in_biiling]   ${company_name}

Input tax id billing section
    [Arguments]     ${tax_id}
    CommonMobileKeywords.Verify Elements Are Visible  &{checkout}[txt_tax_id_in_biiling]
    AppiumLibrary.Input Text     &{checkout}[txt_tax_id_in_biiling]   ${tax_id}

Input company address billing section
    [Arguments]     ${company_address}
    CommonMobileKeywords.Verify Elements Are Visible  &{checkout}[txt_company_address_in_billing]
    AppiumLibrary.Input Text     &{checkout}[txt_company_address_in_billing]   ${company_address}

Select province billing section
    [Arguments]     ${province}
    CommonMobileKeywords.Click Element  &{checkout}[sel_province_in_billing]
    Select first value from dropdown list    &{checkout}[sel_province_in_billing]

Select first value from dropdown list
    [Arguments]     ${locator}
    CommonMobileKeywords.Verify Elements Are Visible    ${locator}
    ${location}=    AppiumLibrary.Get Element Location  ${locator}
    ${size}=    AppiumLibrary.Get Element Size  ${locator}
    ${location_y}=  Evaluate    ${location}[y] + ${size}[height] + (${size}[height] * 0.05)
    ${location_x}=  Evaluate  ${location}[x] + (${size}[width] / 2)
    AppiumLibrary.Click Element At Coordinates  ${location_x}  ${location_y}

Select district billing section
    [Arguments]     ${district}
    CommonMobileKeywords.Click Element  &{checkout}[sel_district_in_billing]
    Select first value from dropdown list   &{checkout}[sel_district_in_billing]

Select sub district billing section
    [Arguments]     ${sub_district}
    CommonMobileKeywords.Click Element  &{checkout}[sel_sub_district_in_billing]
    Select first value from dropdown list   &{checkout}[sel_sub_district_in_billing]

Input billing address details
    [Arguments]     ${company_name}=&{billing_address_${language}}[company_name]
    ...     ${tax_id}=&{billing_address_${language}}[guest_tax_id]
    ...     ${company_address}=&{billing_address_${language}}[address]
    ...     ${province}=&{billing_address_${language}}[region]
    ...     ${district}=&{billing_address_${language}}[district]
    ...     ${sub_district}=&{billing_address_${language}}[sub_district]
    ...     ${postcode}=81120
    Input company name billing section  ${company_name}
    Input tax id billing section    ${tax_id}
    Input company address billing section   ${company_address}
    AppiumLibrary.Hide Keyboard
    Select province billing section     ${province}
    Sleep   ${${GLOBALTIMEOUT}/6} sec
    Select district billing section     ${district}
    Sleep   ${${GLOBALTIMEOUT}/6} sec
    Select sub district billing section     ${sub_district}
    Select save button

Input checkout email address
    [Arguments]     ${email}=${personal_mobile1}[personal_mobile_username]
    CommonMobileKeywords.Verify Elements Are Visible    ${checkout}[txt_checkout_email]
    AppiumLibrary.Input Text    ${checkout}[txt_checkout_email]     ${email}

Retry display shipping address title in checkout details
    Wait Until Keyword Succeeds  ${global_retry_time} x     ${global_retry_in_sec} sec  CommonMobileKeywords.Verify Elements Are Visible    ${checkout}[lbl_shipping_title]

Guest input pick up at store and billing address
	checkout.Input checkout email address
	checkout.Pick up at store and input shipping details
	checkout.Select change button in billing section
	checkout.Input billing address details

Display billing address in checkout details page
    [Arguments]     ${company_name}=&{billing_address_${language}}[company_name]
    ...     ${address}=&{billing_address_${language}}[address]
    ...     ${district}=&{billing_address_${language}}[district]
    ...     ${sub_district}=&{billing_address_${language}}[sub_district]
    ...     ${region}=&{billing_address_${language}}[region]
    ...     ${postcode}=&{billing_address__${language}}[zip_code]
    ...     ${tax_id}=&{billing_address_${language}}[guest_tax_id]
    ${locator}=     CommonKeywords.Format Text  ${checkout}[lbl_billing_address]     billing_address=${company_name} ${address} ${district} ${sub_district} ${region}, ${postcode}
    CommonMobileKeywords.Verify Elements Are Visible    ${locator}
    ${locator}=     CommonKeywords.Format Text  ${checkout}[lbl_billing_address]    billing_address=${tax_id}
    CommonMobileKeywords.Verify Elements Are Visible    ${locator}

Display shipping address in checkout details page
    [Arguments]     ${firstname}=&{shipping_address_${language}}[firstname]
    ${lastname}=&{shipping_address_${language}}[lastname]
    ${address}=&{shipping_address_${language}}[address]
    ${district}=&{shipping_address_${language}}[district]
    ${sub_district}=&{shipping_address_${language}}[sub_district]
    ${region}=&{shipping_address_${language}}[region]
    ${postcode}=&{shipping_address__${language}}[zip_code]
    ${phone_number}=&{shipping_address_${language}}[phone]
    ${locator}=     CommonKeywords.Format Text     ${checkout}[lbl_shipping_address]   shipping_address=${firstname} ${lastname} ${address} ${district} ${sub_district} ${region}, ${postcode} ${mobile_common}[phone_number]: ${phone_number}
    CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Member input pick up at store and display billing address
    checkout.Pick up at store and input shipping details
    checkout.Display billing address in checkout details page

Member display default shipping and billing address in checkout details page
    Display shipping address in checkout details page
    checkout.Display billing address in checkout details page