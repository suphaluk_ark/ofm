*** Setting ***

*** Variables ***


*** Keywords ***
Select add new address
    CommonMobileKeywords.Click Element      &{billing_address}[btn_add_new_address]

Select on edit button by address
    [Arguments]     ${address}
    ${elems}=   Get customer address elements in shipping page
    ${mobile_len}=  Get Length  ${elems}
    ${elems_btn_edit}=  AppiumLibrary.Get Webelements   &{billing_address}[btn_edit]
    ${btn_edit_len}=    Get Length  ${elems_btn_edit}
    : FOR   ${index}    IN RANGE    ${mobile_len}
    \  ${get_cust_address}=     AppiumLibrary.Get Text  @{elems}[${index}]
    \  Run Keyword If   $address in $get_cust_address   Run Keywords    CommonMobileKeywords.Click Element    @{elems_btn_edit}[${index}]    AND    Return From Keyword

Input billing firstname
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible   &{billing_address}[txt_firstName]
    Input Text And Verify Input For Mobile Element    &{billing_address}[txt_firstName]    ${value}

Input billing lastname
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible   &{billing_address}[txt_lastName]
    Input Text And Verify Input For Mobile Element     &{billing_address}[txt_lastName]    ${value}

Input billing phone
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible   &{billing_address}[txt_phone]
    Input Text And Verify Input For Mobile Element    &{billing_address}[txt_phone]    ${value}

Input billing address
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible   &{billing_address}[txt_address]
    Input Text And Verify Input For Mobile Element    &{billing_address}[txt_address]    ${value}

Input billing zip code
    [Arguments]     ${value}
    CommonMobileKeywords.Verify Elements Are Visible   &{billing_address}[txt_zipCode]
    Input Text And Verify Input For Mobile Element    &{billing_address}[txt_zipCode]    ${value}

Select value in dropdown list by label
    [Arguments]     ${value}
    ${elem}=    CommonKeywords.Format Text  &{shipping_address}[txt_in_ddl]     text=${value}
    CommonMobileKeywords.Click Element      ${elem}

Select billing region by label
    [Arguments]     ${value}
    CommonMobileKeywords.Click Element     &{shipping_address}[ddl_province]
    billing_address.Select value in dropdown list by label     ${value}

Select billing district by label
    [Arguments]     ${value}
    CommonMobileKeywords.Click Element    &{shipping_address}[ddl_district]
    billing_address.Select value in dropdown list by label     ${value}

Select billing sub district by label
    [Arguments]     ${value}
    CommonMobileKeywords.Click Element     &{shipping_address}[ddl_subdistrict]
    billing_address.Select value in dropdown list by label     ${value}

Input billing tax identification number
    [Arguments]    ${value}
    CommonMobileKeywords.Verify Elements Are Visible   &{billing_address}[txt_vatId]
    Input Text And Verify Input For Mobile Element     &{billing_address}[txt_vatId]    ${value}

Click on save address button
    CommonMobileKeywords.Click Element     &{billing_address}[btn_save]

Input billing address section
    [Arguments]    ${firstname}=&{billing_address${language}}[firstname]
    ...    ${lastname}=&{billing_address${language}}[lastname]
    ...    ${phone}=&{billing_address${language}}[phone]
    ...    ${address}=&{billing_address${language}}[address]
    ...    ${region}=&{billing_address${language}}[region]
    ...    ${district}=&{billing_address${language}}[district]
    ...    ${sub_district}=&{billing_address${language}}[sub_district]
    ...    ${tax_id}=&{billing_address${language}}[tax_id]
    billing_address.Input billing firstname    ${firstname}
    billing_address.Input billing lastname    ${lastname}
    billing_address.Input billing phone     ${phone}
    billing_address.Input billing address    ${address}
    billing_address.Select billing region by label    ${region}
    billing_address.Select billing district by label    ${district}
    billing_address.Select billing sub district by label    ${sub_district}
    billing_address.Input billing tax identification number    ${tax_id}

Display billing address list
    [Arguments]     ${firstname}
    ...     ${lastname}
    ...     ${address_name}
    ...     ${region}
    ...     ${district}
    ...     ${sub_district}
    ...     ${postcode}
    ...     ${phone}
    ...     ${tax_id}
    ${get_cust_address}=    AppiumLibrary.Get Text      &{billing_address}[lbl_billinglist]
    ${get_cust_name}=   AppiumLibrary.Get Text      &{billing_address}[lbl_billingName]
    ${get_cust_phone}=  AppiumLibrary.Get Text      &{billing_address}[lbl_billingPhone]
    ${get_cust_vatId}=  AppiumLibrary.Get Text      &{billing_address}[lbl_billingtaxId]
    ${txt_shipping_address}=    BuiltIn.Set Variable    ${get_cust_name} ${get_cust_address} ${get_cust_phone}
    ${edited_shipping_address}=    BuiltIn.Set Variable    ${firstname} ${lastname} ${address_name}, ${sub_district}, ${district}, ${region}, ${postcode} ${mobile_common.phone_number}: ${phone}
    ${edited_tax_id}=     BuiltIn.Set Variable    ${mobile_common.tax_id}: ${tax_id}
    Should Match Regexp     ${txt_shipping_address}     ${edited_shipping_address}
    Should Match Regexp     ${get_cust_vatId}   ${edited_tax_id}


