*** Keywords ***
Get order summary details by sku and quantity dictionary
	[Documentation] 	Arguments sku_and_quantity must be the dictionary e.g.sku_number=quantity
	[Arguments] 	&{sku_and_quantity}
	Get total price from product details by dictionary 	&{sku_and_quantity}

Get order summary details by sku and quantity list
	[Documentation] 	Arguments sku_number and quantity must be the list
	[Arguments]	${sku_numbers} 	${product_quantities}
	Get total price from product details by list 	${sku_numbers} 	${product_quantities}

Convert SKU and quantity list to dictionary
	[Arguments] 	${sku_numbers} 	${product_quantities}
	&{sku_quantity}= 	Evaluate 	dict(zip($sku_numbers, $product_quantities))
	[Return] 	&{sku_quantity}

Calculate order summary
	[Arguments] 	${product_sku} 	${product_qty} 	${isNoSurchargeFee}=${False} 	${isNoDeliveryFee}=${False}
	&{sku_and_quantity}= 	calculation.Set product sku and qty as dictionary 	${product_sku} 	${product_qty}
	${price_excl_vat}= 	Set Variable 	${0}
	${surchage}= 	Set Variable 	${0}
	FOR 	${sku} 	IN 	@{sku_and_quantity.keys()}
		${total_price_excl_vat_with_qty}= 	Evaluate 	${${sku}}[price] * ${sku_and_quantity}[${sku}]	
		${price_excl_vat}= 	Evaluate 	${price_excl_vat} + ${total_price_excl_vat_with_qty}
		${surchage}= 	Evaluate 	${surchage} + ${${sku}}[special_delivery_fee]
	END
	Set Test Variable  ${total_price_excl_vat} 	${price_excl_vat}
	Set test variable total vat 	${price_excl_vat}
	Set test variable delivery fee 	${price_excl_vat} 	${vat_amount}
	Set Test Variable  ${total_surcharge}	${surchage}
	Set test variable grand total 	${isNoSurchargeFee} 	${isNoDeliveryFee}

Set test variable total vat
	[Arguments] 	${total_price_exclu_vat}
	calculation.Calculate vat from price exclude vat 	${total_price_exclu_vat}
	Set Test Variable 	${total_vat} 	${vat_amount} 

Set test variable delivery fee
	[Arguments] 	${total_price_exclu_vat} 	${vat_amount}
	${total_price_incl_vat}= 	Evaluate 	${total_price_exclu_vat}+${vat_amount}	
	${delivery_fee}= 	Set Variable If 	${total_price_incl_vat} > 499 	0.00	
	...	50.00
	Set Test Variable  ${delivery_fee}  ${delivery_fee}

Set test variable grand total
	[Arguments] 	${isNoSurchargeFee} 	${isNoDeliveryFee} 	${total_price_excl_vat}=${total_price_excl_vat} 	${total_vat}=${total_vat} 	${delivery_fee}=${delivery_fee} 	${total_surcharge}=${total_surcharge}
	${total_price_incl_vat}= 	Evaluate 	${total_price_excl_vat}+${total_vat}
	${surcharge}= 	Set Variable If  ${isNoSurchargeFee}  0.00
	...	${total_surcharge}
	${total_delivery_fee}= 	Set Variable If  ${isNoDeliveryFee}  0.00
	...	${delivery_fee}
	calculation.Summarize grand total 	${total_price_incl_vat} 	${total_delivery_fee} 	${surcharge} 	${0}

Display total price exclude VAT in order summary
	[Arguments] 	${total_price_excl_vat}
	${locator}= 	CommonKeywords.Format Text  ${order_summary}[lbl_total_price_excl_vat]  label=฿ ${total_price_excl_vat}
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Display total VAT in order summary
	[Arguments] 	${total_vat}
	${locator}= 	CommonKeywords.Format Text  ${order_summary}[lbl_total_vat]  label=฿ ${total_vat}
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Display delivery fee in order summary
	[Arguments] 	${delivery_fee} 	${isNoDeliveryFee}
	${delivery_fee}= 	Set Variable If 	${delivery_fee}==0.0 or ${isNoDeliveryFee}.lower()=='true' 	${mobile_order_summary.free}
	...	฿ ${delivery_fee}
	${locator}= 	CommonKeywords.Format Text  ${order_summary}[lbl_delivery_fee]  label=${delivery_fee}
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Display surcharge fee in order summary
	[Arguments] 	${surcharge_fee} 	${isNoSurchargeFee}
	${surcharge_fee}= 	calculation.Convert price to total amount format  ${surcharge_fee}
	${surcharge_fee}= 	Set Variable If 	${surcharge_fee}==0.0 or ${isNoSurchargeFee} 	${mobile_order_summary.free}
	...	฿ ${surcharge_fee}
	${locator}= 	CommonKeywords.Format Text  ${order_summary}[lbl_surcharge_fee]  label=${surcharge_fee}
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Display grand total in cart and checkout page
	${locator}= 	CommonKeywords.Format Text  ${order_summary}[lbl_grand_total] 	lable=฿ ${grand_total_with_format}
	CommonMobileKeywords.Scroll Up Until Element Is Found 	${order_summary}[screen_cart_table] 	${locator}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Display grand total in payment page
	${locator}= 	CommonKeywords.Format Text  ${order_summary}[lbl_grand_total] 	lable=฿ ${grand_total_with_format}
	CommonMobileKeywords.Scroll Up Until Element Is Found 	${order_summary}[screen_payment_table] 	${locator}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Display grand total in thank you page
	${grand_total}= 	Evaluate  "{:g}".format($grand_total)
	${locator}= 	CommonKeywords.Format Text  ${order_summary}[lbl_grand_total_thank_you] 	lable=฿ ${grand_total}
	CommonMobileKeywords.Scroll Up Until Element Is Found 	${order_summary}[screen_thankyou_table] 	${locator}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Display order summary by price details
	[Arguments] 	${isNoSurchargeFee}=${False} 	${isNoDeliveryFee}=${False}
	Display total price exclude VAT in order summary 	${total_price_excl_vat}
	Display total VAT in order summary 	${total_vat}
	Display delivery fee in order summary 	${delivery_fee} 	${isNoDeliveryFee}
	Display surcharge fee in order summary 	${total_surcharge} 	${isNoSurchargeFee}

Display order summary in cart and checkout page
	[Arguments] 	${isNoSurchargeFee}=${False} 	${isNoDeliveryFee}=${False}
	Display grand total in cart and checkout page
	Display order summary by price details 	${isNoSurchargeFee} 	${isNoDeliveryFee}

Display order summary in payment page
	[Arguments] 	${isNoSurchargeFee}=${False} 	${isNoDeliveryFee}=${False}
	Display grand total in payment page
	Display order summary by price details 	${isNoSurchargeFee} 	${isNoDeliveryFee}

Display order summary in thank you page
	[Arguments] 	${isNoSurchargeFee}=${False} 	${isNoDeliveryFee}=${False}
	Display grand total in thank you page
	Display order summary by price details 	${isNoSurchargeFee} 	${isNoDeliveryFee}
