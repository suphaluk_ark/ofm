*** Setting ***
Resource 	${CURDIR}/../mobile_imports.robot

*** Variables ***

*** Keywords ***
Select Log in button
	${elem}= 	CommonKeywords.Format Text 	&{loginPage}[btn_login] 	label=&{mobile_login}[btn_login]
	CommonMobileKeywords.Click Element 	${elem}

Input Email
	[Arguments] 	${value}
    CommonMobileKeywords.Verify Elements Are Visible  &{loginPage}[txt_email]
    AppiumLibrary.Input Text     &{loginPage}[txt_email]     ${value}

Input Password
	[Arguments] 	${value}
    CommonMobileKeywords.Verify Elements Are Visible    &{loginPage}[txt_pwd]
    AppiumLibrary.Input Text     &{loginPage}[txt_pwd]     ${value}

Display label in Login section
    Display label   &{mobile_login}[welcome_message]  &{mobile_login}[register]     &{mobile_login}[login]
    ...    &{mobile_login}[email]   &{mobile_login}[pwd]    &{mobile_login}[remark_login]   &{mobile_login}[forget_pwd]
    ...    &{mobile_login}[txt_or]  &{mobile_login}[facebook_login]     &{mobile_login}[btn_login]

Display label
    [Arguments]     @{label}
    : FOR   ${text}     IN  @{label}
    \   ${elem}=    CommonKeywords.Format Text  &{loginPage}[label]     label=${text}
    \   CommonMobileKeywords.Verify Elements Are Visible   ${elem}

Select on facebook button
    CommonMobileKeywords.Click Element  &{loginPage}[btn_facebook]

Input facebook username in facebook page
    [Arguments]     ${username}
    CommonMobileKeywords.Verify Elements Are Visible    &{loginPage}[txt_facebook_username]
    AppiumLibrary.Input Text     &{loginPage}[txt_facebook_username]     ${username}

Input facebook password in facebook page
    [Arguments]     ${password}
    CommonMobileKeywords.Verify Elements Are Visible    &{loginPage}[txt_facebook_password]
    AppiumLibrary.Input Password     &{loginPage}[txt_facebook_password]     ${password}

Select login button in facebook page
    CommonMobileKeywords.Click Element  &{loginPage}[btn_facebook_login_web]

Login facebook
    [Arguments]     ${username}     ${password}
    Input facebook username in facebook page    ${username}
    Input facebook password in facebook page    ${password}
    Select login button in facebook page

Display facebook login button
    Wait Until Keyword Succeeds     ${global_retry_time} x  ${global_retry_in_sec} sec  CommonMobileKeywords.Verify Elements Are Visible    &{loginPage}[btn_facebook_login]