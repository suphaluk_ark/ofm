*** Keywords ***
Verify product item amount in cart page
	[Arguments] 	@{sku_numbers}
	${elems}= 	Get product container elements in cart page
	${mobile_len}= 	Get Length 	${elems}
	${sku_len}= 	Get Length 	${sku_numbers}
	Should be True 	${mobile_len}==${sku_len}

Display product details by SKU number in cart page
	[Arguments] 	${sku_number} 	${product_qty}=1
	Display product sku in cart page 	${sku_number}
	${elems}= 	Get product container elements in cart page
	${mobile_len}= 	Get Length 	${elems}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  Verify Elements Are Visible 	&{cart_page}[lbl_product_sku]
	\  ${elem_product_sku}= 	AppiumLibrary.Get Webelements 	&{cart_page}[lbl_product_sku]
	\  ${get_sku_number}= 	AppiumLibrary.Get Text 	@{elem_product_sku}[${index}]
	\  Run Keyword If 	$sku_number == $get_sku_number.split(' ')[1] 	Run Keywords 	Verify product details in cart page
	... 	&{${sku_number}}[product_name]
	... 	&{${sku_number}}[product_sku]
	... 	&{${sku_number}}[price]
	... 	${product_qty}
	... 	${index}
	... 	AND 	Return From Keyword
	Fail 	msg=SKU NUMBER IS NOT VISIBLE IN CART PAGE

Display product sku in cart page
	[Arguments] 	${product_sku}
	${elem}= 	CommonKeywords.Format Text 	&{cart_page}[lal_label] 	label=${mobile_common.sku.title()}: ${product_sku}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Verify product details in cart page
	[Arguments] 	${product_name} 	${product_sku} 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product name in cart page 	${product_name} 	${product_list_index}
	Verify product sku in cart page 	${product_sku} 	${product_list_index}
	Verify product view price in cart page 	${product_price} 	${product_list_index}
	Verify product sub total in cart page 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product total price in cart page 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product quantity in cart page 	${product_quantity} 	${product_list_index}

Verify product name in cart page
	[Arguments] 	${product_name} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[lbl_product_name]
	${elems_product_name}= 	AppiumLibrary.Get Webelements 	&{cart_page}[lbl_product_name]
	AppiumLibrary.Element Text Should Be 	@{elems_product_name}[${product_list_index}] 	${product_name}

Verify product sku in cart page
	[Arguments] 	${product_sku} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[lbl_product_sku]
	${elems_product_sku}= 	AppiumLibrary.Get Webelements 	&{cart_page}[lbl_product_sku]
	AppiumLibrary.Element Text Should Be 	@{elems_product_sku}[${product_list_index}] 	${mobile_common.sku.title()}: ${product_sku}

Verify product view price in cart page
	[Arguments] 	${product_price} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[lbl_product_view_price]
	${elems_product_view_price}= 	AppiumLibrary.Get Webelements 	&{cart_page}[lbl_product_view_price]
	Subtract total price from vat 	${product_price}
	${product_view_price}= 	calculation.Convert price to total amount format 	${product_price}
	AppiumLibrary.Element Text Should Be 	@{elems_product_view_price}[${product_list_index}] 	฿${round_total_without_vat}

Verify product sub total in cart page
	[Arguments] 	${product_price} 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[lbl_sub_total]
	${elems_product_sub_total}= 	AppiumLibrary.Get Webelements 	&{cart_page}[lbl_sub_total]
	Subtract total price from vat 	${product_price}
	calculation.Summarize price with quantity 	${product_quantity} 	${round_total_without_vat}
	AppiumLibrary.Element Text Should Be 	@{elems_product_sub_total}[${product_list_index}] 	฿${price_with_quantity_format}

Verify product total price in cart page
	[Arguments] 	${product_price} 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[lbl_total_price]
	${elems_product_total_price}= 	AppiumLibrary.Get Webelements 	&{cart_page}[lbl_total_price]
	Subtract total price from vat 	${product_price}
	calculation.Summarize price with VAT 	${total_without_vat}
	calculation.Summarize price with quantity 	${product_quantity}
	AppiumLibrary.Element Text Should Be 	@{elems_product_total_price}[${product_list_index}] 	( ฿${price_with_quantity_format} incl. &{mobile_common}[vat] )

Verify product quantity in cart page
	[Arguments] 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[txt_product_quantity]
	${elems_product_quantity}= 	AppiumLibrary.Get Webelements 	&{cart_page}[txt_product_quantity]
	${product_quantity}= 	Evaluate 	str($product_quantity)
	AppiumLibrary.Element Text Should Be 	@{elems_product_quantity}[${product_list_index}] 	${product_quantity}

Select on delete button in cart page
	CommonMobileKeywords.Click Element 	&{cart_page}[btn_remove]

Delete all product in cart page
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[btn_remove]
	${elems}= 	AppiumLibrary.Get Webelements 	&{cart_page}[btn_remove]
	${len}= 	Get Length 	${elems}
	FOR 	${index} 	IN RANGE 	${len}
		Select on delete button in cart page
	END

Display remove item in modal cart page
	${title}= 	CommonKeywords.Format Text 	&{cart_page}[lal_label] 	label=&{mobile_remove_popup}[message1]
	${description}= 	CommonKeywords.Format Text 	&{cart_page}[lal_label] 	label=&{mobile_remove_popup}[message2]
	: FOR 	${text} 	IN 	${title} 	${description} 	&{cart_page}[btn_yes] 	&{cart_page}[btn_no]
	\  CommonMobileKeywords.Verify Elements Are Visible 	${text}

Select OK on remove item in modal cart page
	Display remove item in modal cart page
	CommonMobileKeywords.Click Element 	&{cart_page}[btn_yes]


Select cancel on remove item in modal cart page
	Display remove item in modal cart page
	${btn_no}= 	CommonKeywords.Format Text 	&{cart_page}[lal_label] 	label=${mobile_common.cancel.upper()}
	CommonMobileKeywords.Click Element 	${btn_no}

Get product container elements in cart page
	${locator}= 	CommonKeywords.Format Text 	&{cart_page}[container_product_item]
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}
	${elems}= 	AppiumLibrary.Get Webelements 	${locator}
	[Return] 	${elems}

Get SKU number in cart page by index
	[Arguments] 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[lbl_product_sku]
	${elems_product_sku}= 	AppiumLibrary.Get Webelements 	&{cart_page}[lbl_product_sku]
	${get_sku_number}= 	AppiumLibrary.Get Text 	@{elems_product_sku}[${product_list_index}]
	${get_sku_number}= 	Split String 	${get_sku_number} 	${SPACE}
	[Return] 	@{get_sku_number}[1]

Get product quantity in cart page by index
	[Arguments] 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[txt_product_quantity]
	${elems_product_quantity}= 	AppiumLibrary.Get Webelements 	&{cart_page}[txt_product_quantity]
	${qty}= 	AppiumLibrary.Get Text 	@{elems_product_quantity}[${product_list_index}]
	[Return] 	${qty}

Select on add product quantity in cart page by index
	[Arguments] 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[btn_add_qty]
	${elem_add}= 	AppiumLibrary.Get Webelements 	&{cart_page}[btn_add_qty]
	CommonMobileKeywords.Click Element 	@{elem_add}[${product_list_index}]

Select on remove product quantity in cart page by index
	[Arguments] 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[btn_remove_qty]
	${elem_remove}= 	 AppiumLibrary.Get Webelements 	&{cart_page}[btn_remove_qty]
	CommonMobileKeywords.Click Element 	@{elem_remove}[${product_list_index}]

Select on add product quantity in cart page by SKU number
	[Arguments] 	${sku_number}
	Display product sku in cart page 	${sku_number}
	${elems}= 	Get product container elements in cart page
	${mobile_len}= 	Get Length 	${elems}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  ${get_sku_number}= 	Get SKU number in cart page by index 	${index}
	\  Continue For Loop If 	'${sku_number}' != '${get_sku_number}'
	\  ${before_add}= 	Get product quantity in cart page by index 	${index}
	\  Select on add product quantity in cart page by index 	${index}
	\  ${after_add}= 	Get product quantity in cart page by index 	${index}
	\  Should be True 	${${before_add}+1}==${after_add}
	[Return] 	${after_add}

Select on remove product quantity in cart page by SKU number
	[Arguments] 	${sku_number}
	Display product sku in cart page 	${sku_number}
	${elems}= 	Get product container elements in cart page
	${mobile_len}= 	Get Length 	${elems}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  ${get_sku_number}= 	Get SKU number in cart page by index 	${index}
	\  Continue For Loop If 	'${sku_number}' != '${get_sku_number}'
	\  ${before_remove}= 	Get product quantity in cart page by index 	${index}
	\  Select on remove product quantity in cart page by index 	${index}
	\  ${after_remove}= 	Get product quantity in cart page by index 	${index}
	\  Should be True 	${${before_remove}-1}==${after_remove}
	[Return] 	${after_remove}

Input e coupon in cart page
	[Arguments] 	${e_coupon}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[txt_e_coupon]
	CommonMobileKeywords.Input Text And Verify Input For Mobile Element 	&{cart_page}[txt_e_coupon] 	${e_coupon}

Select apply e coupen in cart page
	CommonMobileKeywords.Click Element 	&{cart_page}[btn_apply_coupon]

Apply e coupon in cart page
	[Arguments] 	${e_coupon}
	Input e coupon in cart page 	${e_coupon}
	Select apply e coupen in cart page

Select on delete product item in cart page by index
	[Arguments] 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{cart_page}[btn_delete_item]
	${elem_delete}= 	 AppiumLibrary.Get Webelements 	&{cart_page}[btn_delete_item]
	CommonMobileKeywords.Click Element 	@{elem_delete}[${product_list_index}]

Select on delete product item in cart page by SKU number
	[Arguments] 	${sku_number}
	Display product sku in cart page 	${sku_number}
	${elems}= 	Get product container elements in cart page
	${mobile_len}= 	Get Length 	${elems}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  ${get_sku_number}= 	Get SKU number in cart page by index 	${index}
	\  Continue For Loop If 	'${sku_number}' != '${get_sku_number}'
	\  Select on delete product item in cart page by index 	${index}
	\  Select OK on remove item in modal cart page
	\  Does not display SKU number in cart page 	${sku_number}
	\  ${elems}= 	Get product container elements in cart page
	\  ${len_after_delete}= 	Get Length 	${elems}
	\  Should be True 	${${mobile_len}-1}==${len_after_delete}
	\  Exit For Loop If 	'${sku_number}' == '${get_sku_number}'

Does not display SKU number in cart page
	[Arguments] 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{cart_page}[label] 	label=${mobile_common.sku.title()}: ${sku_number}
	AppiumLibrary.Wait Until Page Does Not Contain Element 	${elem} 	${GLOBALTIMEOUT}

Select cart icon
	CommonMobileKeywords.Click Element 	&{headerMenu}[btn_cart]

Display product details in cart details page
	[Arguments] 	${product_sku}  ${product_qty}
	&{sku_and_quantity}= 	calculation.Set product sku and qty as dictionary 	${product_sku} 	${product_qty}
	FOR 	${product} 	IN 	@{sku_and_quantity.keys()}
		Display product name in product detail 	${${product}}[product_name]
		Display product sku in product detail 	${${product}}[product_sku]
		Display product price exclude vat in product detail 	${${product}}[price]
		calculation.Summarize price with quantity  ${${product}}[price]  ${sku_and_quantity}[${product}]
		Display product price with vat in product detail 	${price_with_quantity_format}
		Display include vat label in product detail
	END

Display product name in product detail
	[Arguments] 	${product_name} 
	${locator}= 	CommonKeywords.Format Text  ${cart_page}[lbl_product_name]  label=${product_name}
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Display product sku in product detail
	[Arguments] 	${product_sku}
	${locator}= 	CommonKeywords.Format Text 	${cart_page}[lbl_product_sku] 	label=${mobile_common}[sku] ${product_sku}
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Display product price exclude vat in product detail
	[Arguments] 	${product_price_excl_vat}
	${locator}= 	CommonKeywords.Format Text 	${cart_page}[lbl_product_price_excl] 	label=฿ ${product_price_excl_vat}
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Display product price with vat in product detail
	[Arguments] 	${product_price_incl_vat}
	${locator}= 	CommonKeywords.Format Text 	${cart_page}[lbl_product_price_incl] 	label=฿ ${product_price_incl_vat}
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Display include vat label in product detail
	${locator}= 	CommonKeywords.Format Text 	${cart_page}[lbl_inlu_vat] 	label=(${mobile_common}[include_vat])
	CommonMobileKeywords.Verify Elements Are Visible  ${locator}

Retry display shopping cart title in cart page
	Wait Until Keyword Succeeds  ${global_retry_time} x 	${global_retry_in_sec} sec 	CommonMobileKeywords.Verify Elements Are Visible 	${cart_page}[lbl_cart_title]