*** Setting ***
Resource 	${CURDIR}/../mobile_imports.robot

*** Keywords ***
Display order history list by increment id
	[Arguments] 	${increment_id}
	${elem_increment_id}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_increment_id]
	${mobile_len}= 	Get Length 	${elem_increment_id}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  ${get_increment_id}= 	AppiumLibrary.Get Text 	@{elem_increment_id}[${index}]
	\  Run Keyword If 	$increment_id in $get_increment_id 	Run Keywords 	Verify order history list in order history page
	... 	${increment_id}
	... 	${index}
	... 	AND 	Return From Keyword
	Fail 	msg=ORDER HISTORY LIST IS NOT VISIBLE

Verify order history list in order history page
	[Arguments] 	${increment_id} 	${product_list_index}
	Display order increment id in order history page	${increment_id} 	${product_list_index}
	Display order date in order history page	${increment_id}	${product_list_index}
	### TBC Bug :: order status display with invalid status (Frontend Developing)
	# Display order status in order history page	${increment_id}	${product_list_index}
	Display total price in order history page	${increment_id}	${product_list_index}
	Manage order button should be visible	${product_list_index}
	Buy all again should be visible 	${product_list_index}

Display order increment id in order history page
	[Arguments] 	${increment_id}	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_increment_id]
	${elems_increment_id}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_increment_id]
	AppiumLibrary.Element Text Should Be 	@{elems_increment_id}[${product_list_index}] 	&{mobile_common}[order]${SPACE}#${increment_id}

Display order date in order history page
	[Arguments] 	${increment_id}	${product_list_index}
	${date}= 	api_order_details.Get create_at with +7 utc format 	${increment_id}
	${date}= 	Convert response to +7 utc datetime for mobile application 	${date}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_order_datetime]
	${elems_datetime}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_order_datetime]
	AppiumLibrary.Element Text Should Be 	@{elems_datetime}[${product_list_index}] 	${date}

Display order status in order history page
	[Arguments] 	${increment_id}	${product_list_index}
	${status}= 	Get status from order detail response 	${increment_id}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_order_status]
	${elems_status}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_order_status]
	AppiumLibrary.Element Text Should Be 	@{elems_status}[${product_list_index}] 	&{mobile_common}[status]${SPACE}${status}

Display total price in order history page
	[Arguments] 	${increment_id}	${product_list_index}
	${total}= 	Get base_grand_total with total format 	${increment_id}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_total]
	${elems_total}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_total]
	AppiumLibrary.Element Text Should Be 	@{elems_total}[${product_list_index}] 	&{mobile_common}[total]${SPACE}฿${total}

Manage order button should be visible
	[Arguments] 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[btn_manage_order]
	${elems_manage_order}= 	AppiumLibrary.Get Webelements 	&{order_history}[btn_manage_order]
	AppiumLibrary.Wait Until Element Is Visible 	@{elems_manage_order}[${product_list_index}]

Buy all again should be visible
	[Arguments] 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[btn_buy_all_again]
	${elems_buy_all_again}= 	AppiumLibrary.Get Webelements 	&{order_history}[btn_buy_all_again]
	AppiumLibrary.Wait Until Element Is Visible 	@{elems_buy_all_again}[${product_list_index}]

Select on manage order button by increment id
	[Arguments] 	${increment_id}
	${elem_increment_id}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_increment_id]
	${elems_manage_order}= 	AppiumLibrary.Get Webelements 	&{order_history}[btn_manage_order]
	${mobile_len}= 	Get Length 	${elem_increment_id}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  ${get_increment_id}= 	AppiumLibrary.Get Text 	@{elem_increment_id}[${index}]
	\  Run Keyword If 	$increment_id in $get_increment_id 	Run Keywords	Manage order button should be visible	${index}
	... 	AND 	CommonMobileKeywords.Click Element	@{elems_manage_order}[${index}]
	... 	AND 	Return From Keyword
	Fail 	msg=MANGE ORDER BUTTON IS NOT VISIBLE

############ history details page ####################
Display shipping address in order history details page
	[Arguments] 	${firstname}
	... 	${lastname}
	... 	${address_name}
	... 	${region}
	... 	${district}
	... 	${sub_district}
	... 	${postcode}
	... 	${phone}
	${elem_txt_name}=    CommonKeywords.Format Text    &{order_history}[txt_shipping_name]     text=${firstname}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem_txt_name}
	${get_cust_name}=     AppiumLibrary.Get Text    ${elem_txt_name}
	${edited_shipping_name}=	BuiltIn.Set Variable    ${firstname} ${lastname}
	Should Be Equal As Strings     ${edited_shipping_name}    ${get_cust_name}
	${elem_txt_address}=    CommonKeywords.Format Text    &{order_history}[txt_shipping_address]     text=${address_name}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem_txt_address}
	${get_cust_address}=     AppiumLibrary.Get Text    ${elem_txt_address}
	${edited_shipping_address}= 	BuiltIn.Set Variable    ${address_name}, ${sub_district}, ${district}, ${region}, ${postcode}
	Should Be Equal As Strings    ${edited_shipping_address}    ${get_cust_address}
	${elem_txt_phone}=    CommonKeywords.Format Text    &{order_history}[txt_shipping_phone]     text=${phone}
	CommonMobileKeywords.Verify Elements Are Visible	${elem_txt_phone}
	${get_cust_phone}=     AppiumLibrary.Get Text    ${elem_txt_phone}
	${edited_shipping_phone}= 	BuiltIn.Set Variable    ${mobile_common.phone_number} ${phone}
	Should Be Equal As Strings     ${edited_shipping_phone}    ${get_cust_phone}

Display billing address in order history details page
    [Arguments]     ${firstname}
    ...     ${lastname}
    ...     ${address_name}
    ...     ${region}
    ...     ${district}
    ...     ${sub_district}
    ...     ${postcode}
    ...     ${phone}
    ...     ${tax_id}
	${elem_txt_name}=    CommonKeywords.Format Text    &{order_history}[txt_shipping_name]     text=${firstname}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_txt_address}
	${get_cust_name}=     AppiumLibrary.Get Text    ${elem_txt_address}
	${edited_shipping_name}=	BuiltIn.Set Variable    ${firstname} ${lastname}
	Should Be Equal As Strings     ${edited_shipping_name}    ${get_cust_name}
	${elem_txt_address}=    CommonKeywords.Format Text    &{order_history}[txt_shipping_address]     text=${address_name}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_txt_address}
	${get_cust_address}=     AppiumLibrary.Get Text    ${elem_txt_address}
	${edited_shipping_address}= 	BuiltIn.Set Variable    ${address_name}, ${sub_district}, ${district}, ${region}, ${postcode}
	Should Be Equal As Strings     ${edited_shipping_address}    ${get_cust_address}
	${elem_txt_phone}=    CommonKeywords.Format Text    &{order_history}[txt_shipping_phone]     text=${phone}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_txt_phone}
	${get_cust_phone}=     AppiumLibrary.Get Text    ${elem_txt_phone}
	${edited_shipping_phone}= 	BuiltIn.Set Variable    ${mobile_common.phone_number} ${phone}
	Should Be Equal As Strings     ${edited_shipping_phone}    ${get_cust_phone}
	${elem_txt_taxId}=    CommonKeywords.Format Text    &{order_history}[txt_shipping_name]     text=${tax_id}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_txt_taxId}
    ${get_cust_vatId}=  AppiumLibrary.Get Text      ${elem_txt_taxId}
    ${edited_tax_id}=     BuiltIn.Set Variable    ${mobile_common.tax_id} ${tax_id}
    Should Be Equal As Strings     ${get_cust_vatId}   ${edited_tax_id}

####### how to display multiple details
Display product details by SKU number in order history details page
	[Arguments] 	${sku_number} 	${product_qty}=1
	${elem_product_sku}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_product_sku]
	${mobile_len}= 	Get Length 	${elem_product_sku}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  Verify Elements Are Visible 	&{order_history}[lbl_product_sku]
	\  ${get_sku_number}= 	AppiumLibrary.Get Text 	@{elem_product_sku}[${index}]
	\  Run Keyword If 	$sku_number == $get_sku_number 	Run Keywords 	Verify product details in thankyou page
	... 	&{${sku_number}}[product_name]
	... 	&{${sku_number}}[product_sku]
	... 	&{${sku_number}}[price]
	... 	${product_qty}
	... 	${index}
	... 	AND 	Return From Keyword
	Fail 	msg=SKU NUMBER IS NOT VISIBLE IN ORDER HISTORY DETAILS PAGE

Verify product details in order history details page
	[Arguments] 	${product_name} 	${product_sku} 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product name in order history details page	${product_name}
	Verify product sku in order history details page	${product_sku} 	${product_list_index}
	Verify product view price in order history details page 	${product_price} 	${product_list_index}
	Verify product sub total in order history details page 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product total price in order history details page 	${product_price} 	${product_quantity} 	${product_list_index}
	Verify product quantity in order history details page	${product_quantity} 	${product_list_index}

Verify product name in order history details page
	[Arguments] 	${product_name}
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lbl_product_name] 	text=${product_name}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Verify product sku in order history details page
	[Arguments] 	${product_sku} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_product_sku]
	${elems_product_sku}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_product_sku]
	AppiumLibrary.Element Text Should Be 	@{elems_product_sku}[${product_list_index}] 	${product_sku}

Verify product view price in order history details page
	[Arguments] 	${product_price} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_product_view_price]
	${elems_product_view_price}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_product_view_price]
	${product_view_price}= 	calculation.Convert price to total amount format 	${product_price}
	AppiumLibrary.Element Text Should Be 	@{elems_product_view_price}[${product_list_index}] 	฿${product_view_price}

Verify product sub total in order history details page
	[Arguments] 	${product_price} 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_sub_total]
	${elems_product_sub_total}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_sub_total]
	calculation.Summarize price with quantity 	${product_quantity} 	${product_price}
	AppiumLibrary.Element Text Should Be 	@{elems_product_sub_total}[${product_list_index}] 	฿${price_with_quantity_format}

Verify product total price in order history details page
	[Arguments] 	${product_price} 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_price_incVat]
	${elems_product_total_price}= 	AppiumLibrary.Get Webelements 	&{order_history}[lbl_price_incVat]
	calculation.Summarize price with VAT 	${product_price}
	calculation.Summarize price with quantity 	${product_quantity}
	AppiumLibrary.Element Text Should Be 	@{elems_product_total_price}[${product_list_index}] 	( ฿${price_with_quantity_format} incl. &{mobile_common}[vat] )

Verify product quantity in order history details page
	[Arguments] 	${product_quantity} 	${product_list_index}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[txt_product_quantity]
	${elems_product_quantity}= 	AppiumLibrary.Get Webelements 	&{order_history}[txt_product_quantity]
	${product_quantity}= 	Evaluate 	str($product_quantity)
	AppiumLibrary.Element Text Should Be 	@{elems_product_quantity}[${product_list_index}] 	${mobile_thankyou.order_list.quantity} ${product_quantity}

################ payment details ##########
Display payment details in order history details page
	[Arguments] 	${sku_numbers} 	${product_quantities}
	Get total price from product details 	${sku_numbers} 	${product_quantities}
	Verify total price in thank you page
	Verify shipping & handling in thank you page
	# Verify discount in order history details page
	# Verify the 1 card redemption in order history details page
	# Verify grand total in order history details page

Verify total price in order history details page
	${total_price}= 	Convert price to total amount format 	${total_price}
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lbl_total_price] 	label=฿${total_price}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Verify shipping & handling in order history details page
	${shipping}= 	Set Variable If 	${total_price}<=${499} 	${50}
	...	ELSE 	${0}
	${shipping}= 	Convert price to total amount format 	${shipping}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history[lbl_shipping_handling]
	AppiumLibrary.Element Text Should Be 	&{order_history}[lbl_shipping_handling] 	฿${shipping}

Verify discount in order history details page
	[Documentation] 	TBC
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_discount]
	AppiumLibrary.Element Text Should Be 	&{order_history}[lbl_discount] 	฿${price_with_vat_format}

Verify the 1 card redemption in order history details page
	[Documentation] 	TBC
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_the_1_card]
	AppiumLibrary.Element Text Should Be 	&{order_history}[lbl_the_1_card] 	฿${price_with_vat_format}

Verify grand total in order history details page
	[Documentation] 	TBC
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_history}[lbl_grand_total]
	AppiumLibrary.Element Text Should Be 	&{order_history}[lbl_grand_total] 	฿${price_with_vat_format}


###########
Display order date in order history details page
	[Arguments] 	${increment_id}
	${date}= 	api_order_details.Get create_at with +7 utc format 	${increment_id}
	${date}= 	Convert response to +7 utc datetime for mobile application 	${date}
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lal_label] 	label=${date}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Display order increment id in history details page
	[Arguments] 	${increment_id}
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lal_label] 	label=${increment_id}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Display payment type in history details page
	[Arguments] 	${increment_id}
	${payment_type}= 	api_order_details.Get payment method from order detail response 	${increment_id}
	${payment_type}= 	Set Variable If 	'${payment_type}'=='${mdc_payment_method.credit}' 	${mobile_payment_type.credit_card}
	...  	'${payment_type}'=='${mdc_payment_method.cod}'	${mobile_payment_type.cod}
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lal_label] 	label=${payment_type}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Display order status in history details page
	[Arguments] 	${increment_id}
	${status}= 	api_order_details.Get status from order detail response 	${increment_id}
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lal_label] 	label=${status}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Verify order details in order history details page
	[Arguments] 	${increment_id} 	${sku_numbers} 	${product_quantities}
	: FOR 	${text} 	IN  	${mobile_thankyou.order_summary.title}
	... 	${mobile_thankyou.order_summary.order_code}
	... 	${mobile_thankyou.order_summary.order_date}
	... 	${mobile_thankyou.order_summary.payment_type}
	... 	${mobile_thankyou.order_summary.order_status}
	... 	${mobile_thankyou.order_summary.the1card}
	... 	${mobile_thankyou.shipping.title}
	... 	${mobile_thankyou.billing.title}
	... 	${mobile_thankyou.payment_detail.total_price}
	... 	${mobile_thankyou.payment_detail.shipping_handling}
	... 	${mobile_thankyou.payment_detail.discount}
	... 	${mobile_thankyou.payment_detail.the1card}
	... 	${mobile_thankyou.payment_detail.grand_total}
	\  ${elem}= 	CommonKeywords.Format Text 	&{order_history}[lal_label] 	label=${text}
	\  CommonMobileKeywords.Verify Elements Are Visible 	${elem}
	Display order date in order history details page	${increment_id}
	Display order increment id in history details page 	${increment_id}
	### TBC  Bug :: payment type display with invalid lang (EN) should be (TH)
	# Display payment type in history details page	${increment_id}
	Display order status in history details page 	${increment_id}
	Display product details by SKU number in order history details page	${sku_numbers} 	${product_quantities}
	Display payment details in order history details page 	${sku_numbers} 	${product_quantities}




