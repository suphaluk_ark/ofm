*** Setting ***
Resource 	${CURDIR}/../mobile_imports.robot

*** Keywords ***
Select main menu
    [Documentation]     This keyword for clicking on drawer to expand menu list
    CommonMobileKeywords.Click Element    &{headerMenu}[hamburger]

Select menu
	[Arguments] 	${value}
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[label] 	label=${value}
	CommonMobileKeywords.Click Element 	${elem}

select sub menu
	[Arguments] 	${value}
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[scroll_to_menu] 	label=${value}
	CommonMobileKeywords.Click Element 	${elem}

select menu under manage account page
	[Arguments] 	${value}
	${elem}= 	CommonKeywords.Format Text 	&{mange_account_page}[menu] 	menu=${value}
	CommonMobileKeywords.Click Element 	${elem}


Display Log in firstname
	[Arguments] 	${firstname}
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[label] 	label=${firstname}
	CommonMobileKeywords.Verify Elements Are Visible 	${elem}

Select on search button
	CommonMobileKeywords.Click Element 	&{headerMenu}[btn_search]

Select on shopping cart button
	CommonMobileKeywords.Click Element 	&{headerMenu}[btn_cart]

Select on back button in header menu
	CommonMobileKeywords.Click Element 	&{headerMenu}[btn_back]

Display product quantity at icon
	[Arguments] 	${cart_quantity}
	${locator}= 	CommonKeywords.Format Text  &{headerMenu}[lbl_product_qty]  qty=${cart_quantity}
	CommonMobileKeywords.Verify Elements Are Visible 	${locator}

Display OFM logo in header
	CommonMobileKeywords.Verify Elements Are Visible 	&{headerMenu}[img_ofm]
