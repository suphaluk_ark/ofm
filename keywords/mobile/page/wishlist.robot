*** Keywords ***
Add product to wishlist by SKU number
	[Arguments] 	${sku_number_add_wishlist} 	${product_qty}=${1}
	search_feature.Search and select product item by SKU number 	${sku_number_add_wishlist}

Click wishlist icon
	CommonMobileKeywords.Click Element 	&{wishlist}[btn_add_wishlist]

Accept popup login
    CommonMobileKeywords.Click Element  &{wishlist}[btn_login]

