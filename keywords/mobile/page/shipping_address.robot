*** Setting ***

*** Variables ***


*** Keywords ***
Select on edit button by address
	[Arguments] 	${address}
	${elems}= 	Get customer address elements in shipping page
	${mobile_len}= 	Get Length 	${elems}
	${elems_btn_edit}= 	AppiumLibrary.Get Webelements 	&{shipping_address}[btn_edit]
	${btn_edit_len}= 	Get Length 	${elems_btn_edit}
	: FOR 	${index} 	IN RANGE 	${mobile_len}
	\  ${get_cust_address}= 	AppiumLibrary.Get Text 	@{elems}[${index}]
	\  Run Keyword If 	$address in $get_cust_address 	Run Keywords    CommonMobileKeywords.Click Element    @{elems_btn_edit}[${index}]    AND    Return From Keyword

Get customer address elements in shipping page
	CommonMobileKeywords.Verify Elements Are Visible 	&{shipping_address}[lbl_address]
	${elems}= 	AppiumLibrary.Get Webelements 	&{shipping_address}[lbl_address]
	[Return] 	${elems}

Input shipping firstname
	[Arguments]     ${value}
	CommonMobileKeywords.Verify Elements Are Visible 	&{shipping_address}[txt_firstName]
	Input Text And Verify Input For Mobile Element   &{shipping_address}[txt_firstName]    ${value}

Input shipping lastname
	[Arguments]     ${value}
	CommonMobileKeywords.Verify Elements Are Visible 	&{shipping_address}[txt_lastName]
	Input Text And Verify Input For Mobile Element     &{shipping_address}[txt_lastName]    ${value}

Input shipping phone
	[Arguments]     ${value}
	CommonMobileKeywords.Verify Elements Are Visible	&{shipping_address}[txt_phone]
	Input Text And Verify Input For Mobile Element    &{shipping_address}[txt_phone]    ${value}

Input shipping address
	[Arguments]     ${value}
	CommonMobileKeywords.Verify Elements Are Visible	&{shipping_address}[txt_address]
	Input Text And Verify Input For Mobile Element    &{shipping_address}[txt_address]    ${value}

Select value in dropdown list by label
	[Arguments]     ${value}
	${elem}= 	CommonKeywords.Format Text 	&{shipping_address}[txt_in_ddl] 	text=${value}
	CommonMobileKeywords.Click Element  	${elem}

Select shipping region by label
	[Arguments]     ${value}
	CommonMobileKeywords.Click Element     &{shipping_address}[ddl_province]
	shipping_address.Select value in dropdown list by label     ${value}

Select shipping district by label
	[Arguments]     ${value}
	CommonMobileKeywords.Click Element    &{shipping_address}[ddl_district]
	shipping_address.Select value in dropdown list by label     ${value}

Select shipping sub district by label
	[Arguments]     ${value}
	CommonMobileKeywords.Click Element     &{shipping_address}[ddl_subdistrict]
	shipping_address.Select value in dropdown list by label     ${value}

Input shipping address section
	 [Arguments]    ${firstname}=&{shipping_address${language}}[firstname]
	...    ${lastname}=&{shipping_address${language}}[lastname]
	...    ${phone}=&{shipping_address${language}}[phone]
	...    ${address}=&{shipping_address${language}}[address]
	...    ${region}=&{shipping_address${language}}[region]
	...    ${district}=&{shipping_address${language}}[district]
	...    ${sub_district}=&{shipping_address${language}}[sub_district]
	shipping_address.Input shipping firstname    ${firstname}
	shipping_address.Input shipping lastname    ${lastname}
	shipping_address.Input shipping phone    ${phone}
	shipping_address.Input shipping address    ${address}
	shipping_address.Select shipping region by label     ${region}
	shipping_address.Select shipping district by label   ${district}
	shipping_address.Select shipping sub district by label    ${sub_district}


Click on save address button
	CommonMobileKeywords.Click Element     &{shipping_address}[btn_save]

Select back button on manage account menu
	CommonMobileKeywords.Click Element     &{mange_account_page}[btn_back]

Display shipping address list
	[Arguments] 	${firstname}
	... 	${lastname}
	... 	${address_name}
	... 	${region}
	... 	${district}
	... 	${sub_district}
	... 	${postcode}
	... 	${phone}
	${get_cust_address}= 	AppiumLibrary.Get Text 		&{shipping_address}[lbl_shippinglist]
	${get_cust_name}= 	AppiumLibrary.Get Text 		&{shipping_address}[lbl_shippingName]
	${get_cust_phone}= 	AppiumLibrary.Get Text 		&{shipping_address}[lbl_shippingPhone]
	${txt_shipping_address}= 	BuiltIn.Set Variable    ${get_cust_name} ${get_cust_address} ${get_cust_phone}
	${edited_shipping_address}=	BuiltIn.Set Variable    ${firstname} ${lastname} ${address_name}, ${sub_district}, ${district}, ${region}, ${postcode} ${mobile_common.phone_number}: ${phone}
	Should Match Regexp 	${txt_shipping_address}	${edited_shipping_address}



























