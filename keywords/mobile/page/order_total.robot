*** Keywords ***
Select on continue payment button
	CommonMobileKeywords.Click Element 	&{order_total}[btn_checkout]

Display continue payment button
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_total}[btn_checkout]
	AppiumLibrary.Element Text Should Be 	&{order_total}[btn_checkout] 	${mobile_common.continue_payment.upper()}

Display order total label in order total
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_total}[lbl_order_total]
	AppiumLinbrary.Element Should Contain Text 	&{order_total}[lbl_order_total] 	${mobile_common.order_detail}

Display grand total price in order total
	[Arguments] 	${grand_total_price}=${grand_total_with_format}
	CommonMobileKeywords.Verify Elements Are Visible 	&{order_total}[lbl_summary_price]
	AppiumLinbrary.Element Should Contain Text 	&{order_total}[lbl_summary_price] 	฿${grand_total_price}

