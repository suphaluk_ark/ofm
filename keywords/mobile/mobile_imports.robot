*** Setting ***
Resource 	${CURDIR}/page/product_page.robot
Resource 	${CURDIR}/page/order_total.robot
Resource 	${CURDIR}/page/payment.robot
Resource 	${CURDIR}/page/search_page.robot
Resource 	${CURDIR}/page/left_menu.robot
Resource 	${CURDIR}/page/header_menu.robot
Resource 	${CURDIR}/page/login.robot
Resource 	${CURDIR}/page/cart.robot
Resource 	${CURDIR}/page/order_summary.robot
Resource 	${CURDIR}/page/order_history.robot
Resource 	${CURDIR}/page/2c2p.robot
Resource 	${CURDIR}/page/checkout.robot
Resource 	${CURDIR}/page/mobile_thankyou.robot
Resource 	${CURDIR}/page/wishlist.robot
Resource 	${CURDIR}/../calculation.robot
Resource 	${CURDIR}/feature/login_feature.robot
Resource 	${CURDIR}/feature/checkout_feature.robot
Resource 	${CURDIR}/feature/search_feature.robot
Resource 	${CURDIR}/feature/place_order_feature.robot
Resource 	${CURDIR}/feature/my_account_feature.robot
Resource 	${CURDIR}/feature/wishlist_feature.robot
Resource 	${CURDIR}/page/shipping_address.robot
Resource 	${CURDIR}/page/billing_address.robot
Resource 	${CURDIR}/../common/keywords/CommonKeywords.robot
Resource 	${CURDIR}/../common/keywords/CommonMobileKeywords.robot
Resource 	${CURDIR}/../../resources/imports.robot

Variables 	${CURDIR}/../../resources/testdata/common${/}mobile_translation_${language}.yaml
Variables 	${CURDIR}/../../resources/common_configs.yaml

*** Variables ***
${testdata_filepath} 	${CURDIR}/../../resources/testdata/

*** Keywords ***
Mobile global init
	Import Variables 	${testdata_filepath}${ENV.lower()}${/}test_data.yaml
	Import Variables 	${testdata_filepath}${ENV.lower()}${/}product_data.yaml
	Import Variables 	${testdata_filepath}${ENV.lower()}${/}mdc_data.yaml
	Set Global Variable 	${language} 	${language.lower()}
	Import Resource 	${CURDIR}/element/element_&{${mobileDevice}}[platform].robot

