*** Settings ***
Resource 	${CURDIR}/api_imports.robot

*** Variables ***
${path_root} 	$.items[0].
${am_th} 	ก่อนเที่ยง
${pm_th} 	หลังเที่ยง
${language_th} 	th

*** Keywords ***
Get create_at from order detail response
	[Arguments] 	${increment_id}
 	${response}= 	order_details.Get order details by increment_id 	${increment_id}
 	${create_at}= 	REST.Output 	${path_root}created_at
 	[Return] 	${create_at}

Get status from order detail response
	[Arguments] 	${increment_id}
 	${response}= 	order_details.Get order details by increment_id 	${increment_id}
 	${status}= 	REST.Output 	${path_root}status
 	${status}= 	Covert MCOM order status to message 	${status}
 	[Return] 	${status}

Covert MCOM order status to message
	[Arguments] 	${MCOM_status}
	${convert_status}= 	Set Variable If 	'${MCOM_status}'=='&{mcom_order_status}[mcom_onhold]' 	&{web_order_history}[receive_order]
	...	'${MCOM_status}'=='&{mcom_order_status}[mcom_logistics]' 	&{web_order_history}[logistic]
	[Return] 	${convert_status}

Get base_grand_total from order detail response
	[Arguments] 	${increment_id}
 	${response}= 	order_details.Get order details by increment_id 	${increment_id}
 	${base_grand_total}= 	REST.Output 	${path_root}base_grand_total
 	[Return] 	${base_grand_total}

Get base_grand_total with total format
	[Arguments] 	${increment_id}
	${total}= 	Get base_grand_total from order detail response 	${increment_id}
	${total}= 	Convert response to total amount format 	${total}
	[Return] 	${total}

Get create_at with +7 utc format
	[Arguments] 	${increment_id}
	${datetime}= 	Get create_at from order detail response 	${increment_id}
	${datetime}= 	Run Keyword If 	${datetime[0]}==${0} 	Evaluate 	$datetime[1:]
	... 	ELSE 	Set Variable 	${datetime}
	[Return] 	${datetime}

Convert response to total amount format
	[Arguments] 	${total}
	${total}= 	Evaluate 	'{:,.2f}'.format($total)
	[Return] 	${total}

Convert datetime to D MMM YYYY hh.mm format
	[Arguments] 	${datetime}
	${fmt_datetime}= 	Evaluate 	arrow.get($datetime).format('D MMM YYYY hh.mmA') 	arrow, datetime
	[Return] 	${fmt_datetime}

Convert datetime to +7 utc and D MMM YYYY hh.mm format
	[Arguments] 	${datetime}
	${fmt_datetime}= 	Evaluate 	arrow.get($datetime).to('Asia/Bangkok').format('D MMM YYYY hh.mm') 	arrow, datetime
	${am_pm}= 	Evaluate 	arrow.get($datetime).to('Asia/Bangkok').format('A') 	arrow, datetime
	# ${am_pm}= 	Set Variable If 	'${language}'=='${language_th}' and '${am_pm}'=='PM' 	${pm_th}
	# ...	ELSE IF 	'${language}'=='${language_th}' and '${am_pm}'=='AM' 	${am_th}
	# ...	ELSE 	${am_pm}
	[Return] 	${fmt_datetime}${am_pm}

Get payment method from order detail response
	[Arguments] 	${increment_id}
	${payment_method}= 	Get create_at from order detail response 	${increment_id}
	${payment_method}= 	REST.Output 	${path_root}payment.method
	[Return] 	${payment_method}

Get status from order details response by entity_id
	[Arguments] 	${entity_id}
	${status}= 	order_details.Get order details by entity_id 	${entity_id}
	${status}= 	REST.Output 	$.status
	[Return] 	${status}

Get order id from order details response by entity_id
	[Arguments] 	${entity_id}
	${order_id}= 	order_details.Get order details by entity_id 	${entity_id}
	${order_id}= 	REST.Output 	$.increment_id
	[Return] 	${order_id}

Get payment method from order details response by entity_id
	[Arguments] 	${entity_id}
	${payment_method}= 	order_details.Get order details by entity_id 	${entity_id}
	${payment_method}= 	REST.Output 	$.payment.method
	[Return] 	${payment_method}

Get billing address from order details response by entity_id
	[Arguments] 	${entity_id}
	${billing_address}= 	order_details.Get order details by entity_id 	${entity_id}
	${firstname}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=='firstname')].value
	${lastname}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=='lastname')].value
	${address_line}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=='address_line')].value
	${district}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=='district')].value
	${sub_district}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=='subdistrict')].value
	${region}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=="city")].value
	${postcode}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=="postcode")].value
	${tel}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=="telephone")].value
	${tax_id}= 	REST.Output 	$.billing_address..custom_attributes[?(@.attribute_code=="vat_id")].value
	[Return] 	${firstname} ${lastname}, ${address_line} ${district} ${sub_district} ${region} ${postcode}, ${tel}, ${tax_id}

Get shipping address from order details response by entity_id
	[Arguments] 	${entity_id}
	${shipping_address}= 	order_details.Get order details by entity_id 	${entity_id}
	${firstname}= 	REST.Output 	$..shipping..custom_attributes[?(@.attribute_code=="firstname")].value
	${lastname}= 	REST.Output 	$..shipping..custom_attributes[?(@.attribute_code=="lastname")].value
	${address_line}= 	REST.Output 	$..shipping..custom_attributes[?(@.attribute_code=="address_line")].value
	${district}= 	REST.Output 	$..shipping..custom_attributes[?(@.attribute_code=="district")].value
	${sub_district}= 	REST.Output 	$..shipping..custom_attributes[?(@.attribute_code=="subdistrict")].value
	${region}= 	REST.Output 	$..shipping..custom_attributes[?(@.attribute_code=="city")].value
	${postcode}= 	REST.Output 	$..shipping..custom_attributes[?(@.attribute_code=="postcode")].value
	${tel}= 	REST.Output 	$..shipping..custom_attributes[?(@.attribute_code=="telephone")].value
	[Return] 	${firstname} ${lastname}, ${address_line} ${district} ${sub_district} ${region} ${postcode}, ${tel}


Convert response to +7 utc datetime for mobile application
	[Arguments] 	${datetime}
	${fmt1_datetime}= 	Evaluate 	arrow.get($datetime).to('Asia/Bangkok').format('D MMM',locale='${language}') 	arrow, datetime
	${fmt2_datetime}= 	Evaluate 	arrow.get($datetime).to('Asia/Bangkok').format('YYYY, HH:mm:ss') 	arrow, datetime
	[Return] 	${fmt1_datetime}${SPACE}${fmt2_datetime}



























