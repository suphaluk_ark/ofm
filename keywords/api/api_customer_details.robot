*** Variables ***



*** Keywords ***
Get items quantity from customer cart response
	[Arguments] 	${username} 	${password}
	${token}= 	authentication.Get login token 	${username} 	${password}
	customer_details.Get customer cart details 	${token}
	${item_qty}= 	REST.output 	$.items_qty
	[Return] 	${item_qty}

Get surcharge fee amount from customer cart response
	[Arguments] 	${username} 	${password}
	${token}= 	authentication.Get login token 	${username} 	${password}
	customer_details.Get my customer cart totals 	${token}
	${surcharge_fee}= 	REST.output 	$..surcharge_fee_amount
	[Return] 	${surcharge_fee}

Get product name from customer cart response
	[Arguments] 	${username} 	${password}
	${token}= 	authentication.Get login token 	${username} 	${password}
	customer_details.Get my customer cart totals 	${token}
	${product_name}= 	REST.output 	$..items..name
	[Return] 	${product_name}

Get product quantity by index from customer cart response
	[Arguments] 	${username} 	${password} 	${index}
	${token}= 	authentication.Get login token 	${username} 	${password}
	customer_details.Get my customer cart totals 	${token}
	${product_qty}= 	REST.output 	$..items[${index}]..qty
	[Return] 	${product_qty}

Update customer as default shipping address
	[Arguments] 	${username} 	${password}
	... 	${firsename}=&{shipping_address_${language}}[firstname]
	... 	${lastname}=&{shipping_address_${language}}[lastname]
	... 	${phone}=&{shipping_address_${language}}[phone]
	... 	${address_line}=&{shipping_address_${language}}[address]
	... 	${postcode}=&{shipping_address_${language}}[zip_code]
	... 	${city}=&{shipping_address_${language}}[region]
	... 	${district_id}=40
	... 	${subdistrict_id}=165
	... 	${region}=570
	${token}= 	authentication.Get login token 	${username} 	${password}
	${response}= 	customer_details.Get customer details 	${token}
	&{header}= 	Create customer request header 	${token}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].firstname 	${firsename}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].lastname 	${lastname}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].telephone 	${phone}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].postcode 	${postcode}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].custom_attributes[?(@.attribute_code == "address_line")].value 	${address_line}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].custom_attributes[?(@.attribute_code == "district_id")].value 	${district_id}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].custom_attributes[?(@.attribute_code == "subdistrict_id")].value 	${subdistrict_id}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].region_id 	${region}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_shipping == true)].city 	${city}
	${json}= 	Evaluate 	json.dumps({'customer':$response}) 	json
	REST.Put 	${api_root}${api_customer_details} 	${json} 	headers=&{header}
	REST.Integer 	response status 	200

Update customer as default billing address
	[Arguments] 	${username} 	${password}
	... 	${firsename}=&{billing_address_${language}}[firstname]
	... 	${lastname}=&{billing_address_${language}}[lastname]
	... 	${phone}=&{billing_address_${language}}[phone]
	... 	${address_line}=&{billing_address_${language}}[address]
	... 	${postcode}=&{billing_address_${language}}[zip_code]
	... 	${city}=&{billing_address_${language}}[region]
	... 	${district_id}=17
	... 	${subdistrict_id}=84
	... 	${region}=570
	... 	${vat}=&{billing_address_${language}}[guest_tax_id]
	${token}= 	authentication.Get login token 	${username} 	${password}
	${response}= 	customer_details.Get customer details 	${token}
	&{header}= 	Create customer request header 	${token}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].firstname 	${firsename}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].lastname 	${lastname}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].telephone 	${phone}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].postcode 	${postcode}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].vat_id 	${vat}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code == "address_line")].value 	${address_line}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code == "district_id")].value 	${district_id}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].custom_attributes[?(@.attribute_code == "subdistrict_id")].value 	${subdistrict_id}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].region_id 	${region}
	${response}= 	Update Value To Json 	${response} 	$..addresses[?(@.default_billing == true)].city 	${city}
	${json}= 	Evaluate 	json.dumps({'customer':$response}) 	json
	REST.Put 	${api_root}${api_customer_details} 	${json} 	headers=&{header}
	REST.Integer 	response status 	200

Get order number from API cart mine response
	[Arguments] 	${username} 	${password}
	${token}= 	authentication.Get login token 	${username} 	${password}
	customer_details.Get customer cart details 	${token}
	${product_name}= 	REST.output 	$..order_id
	[Return] 	${product_name}