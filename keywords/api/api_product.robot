*** Settings ***
Resource 	${CURDIR}/api_imports.robot

*** Variables ***
${path_root} 	$.items[0].
${am_th} 	ก่อนเที่ยง
${pm_th} 	หลังเที่ยง
${language_th} 	th

*** Keywords ***
Get price from product response
	[Arguments] 	${sku}
	product.Get product by sku 	${sku}
 	${price_mdc}= 	REST.Output 	$..custom_attributes[?(@.attribute_code == 'special_price')].value
 	[Return] 	${price_mdc}

Get product name from API response
	[Arguments] 	${sku}
	product.Get product by sku 	${sku}
	${product_name}= 	REST.Output 	$.name
	[Return] 	${name}

Get product surcharge fee from API response
	[Arguments] 	${sku}
	product.Get product by sku 	${sku}
	${product_surcharge}= 	REST.Output 	$..custom_attributes[?(@.attribute_code == 'delivery_surcharge_fee')].value
	[Return] 	${product_surcharge}

Get product data
	[Arguments] 	${sku}
	${product_price_exclude_vat}= 	api_product.Get price from product response 	${sku}
	${product_surcharge_fee}= 	api_product.Get product surcharge fee from API response 	${sku}
	[Return]	${${product_price_exclude_vat}} 	${${product_surcharge_fee}}

Set product data from API response
	[Arguments] 	@{sku_list}
	FOR 	${sku} 	IN 	@{sku_list}
		${price_excl_vat} 	${surcharge_fee} 	Get product data 	${sku}
		Set To Dictionary 	${${sku}} 	price=${price_excl_vat} 	special_delivery_fee=${surcharge_fee}
		Set Global Variable  ${${sku}}  ${${sku}}
	END