*** Settings ***
Resource 	${CURDIR}/../../resources/imports.robot
Resource 	${CURDIR}/../common/keywords/CommonKeywords.robot
Resource 	api_order_details.robot
Resource 	verify_mdc.robot
Resource 	api_customer_details.robot
Resource 	api_product.robot
Resource 	${CURDIR}/../common/mdc/api/mdc/authentication.robot
Resource 	${CURDIR}/../common/mdc/api/mdc/customer_details.robot
Resource 	${CURDIR}/../common/mdc/api/mdc/order_details.robot
Resource 	${CURDIR}/../common/mdc/api/mdc/order_details.robot
Resource 	${CURDIR}/../common/mdc/api/mdc/product.robot


*** Variables ***
${config_filepath} 	/../../resources/configs/

*** Keywords ***
API global init
	Import Variables 	${CURDIR}${config_filepath}${ENV.lower()}${/}env_config.yaml
	Import Resource 	${CURDIR}/../common/mdc/api/mcom/search_sales_order.robot
	Import Resource 	${CURDIR}/../common/mdc/api/mcom/McomAuthentication.robot
	Import Resource 	${CURDIR}/../common/mdc/api/mcom/McomStock.robot
	Set product data from API response 	@{product_sku_list}