*** Setting ***

*** Variables ***

*** Keywords ***
Verify mdc status filter by order_id
	[Arguments] 	${order_id} 	${expect_status}
	${status}= 	api_order_details.Get status from order details response by entity_id 	${order_id}
	Should be True 	'${status}'=='${expect_status}'

Verify mdc payment method filter by order_id
	[Arguments] 	${order_id} 	${expect_status}
	${payment_method}= 	api_order_details.Get payment method from order details response by entity_id 	${order_id}
	Should be True 	'${payment_method}'=='${expect_status}'

Verify mdc status filter by increment_id
	[Arguments] 	${increment_id} 	${expect_status}
	${status}= 	api_order_details.Get status from order detail response 	${increment_id}
	Should be True 	'${status}'=='${expect_status}'

Verify mdc payment method filter by increment_id
	[Arguments] 	${increment_id} 	${expect_status}
	${method}= 	Get payment method from order detail response 	${increment_id}
	Should be True 	'${method}'=='${expect_status}'

Verify mdc billing address filter by order_id
	[Arguments] 	${order_id}
	... 	${firstname}
	... 	${lastname}
	... 	${address}
	... 	${district}
	... 	${sub_district}
	... 	${region}
	... 	${postcode}
	... 	${phone}
	... 	${tax_id}
	${billing_address}= 	api_order_details.Get billing address from order details response by entity_id 	${order_id}
	Should be True 	'${billing_address}'=='${firstname} ${lastname}, ${address} ${district} ${sub_district} ${region} ${postcode}, ${phone}, ${tax_id}'

Verify mdc shipping address filter by order_id
	[Arguments] 	${order_id}
	... 	${firstname}
	... 	${lastname}
	... 	${address}
	... 	${district}
	... 	${sub_district}
	... 	${region}
	... 	${postcode}
	... 	${phone}
	${shipping_address}= 	api_order_details.Get shipping address from order details response by entity_id 	${order_id}
	Should be True 	'${shipping_address}'=='${firstname} ${lastname}, ${address} ${district} ${sub_district} ${region} ${postcode}, ${phone}'

Verify empty item from mdc cart filter by username
	[Arguments] 	${username} 	${password}
	${item_qty}= 	api_customer_details.Get items quantity from customer cart response 	${username} 	${password}
	Should be True 	${item_qty}==${0}

Verify mdc payment by credit card filter by increment_id
	[Arguments] 	${increment_id}
	Verify mdc payment method filter by increment_id 	${increment_id} 	${mdc_payment_method.credit}

Verify mdc status pending_payment filter by increment_id
	[Arguments] 	${increment_id}
	Verify mdc status filter by increment_id 	${increment_id} 	${mdc_order_status.pending_payment}

Verify mcom order status
	[Arguments] 	${order_id} 	${expected_status}
	${mcom_status}= 	search_sales_order.Get order status from MCOM 	${order_id}
	Should Be True 	'${mcom_status}'=='${expected_status}'

Verify mcom order status with retry
	[Arguments] 	${order_id} 	${expected_status}	${retry_time}=3	${retry_period_in_second}=170
	BuiltIn.Wait Until Keyword Succeeds	${retry_time} x	${retry_period_in_second} sec	Verify mcom order status 	${order_id}	${expected_status}




