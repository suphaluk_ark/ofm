*** Setting ***
Resource 	${CURDIR}/page/header_menu.robot
Resource 	${CURDIR}/page/login.robot
Resource 	${CURDIR}/page/productCategory.robot
Resource 	${CURDIR}/page/productPage.robot
Resource 	${CURDIR}/page/cart.robot
Resource 	${CURDIR}/page/checkout.robot
Resource 	${CURDIR}/page/order_summary.robot
Resource 	${CURDIR}/page/order_history.robot
Resource 	${CURDIR}/page/order_details_page.robot
Resource 	${CURDIR}/page/payment.robot
Resource 	${CURDIR}/page/success.robot
Resource 	${CURDIR}/page/wishlist.robot
Resource 	${CURDIR}/page/2c2p.robot
Resource 	${CURDIR}/page/web_thankyou.robot
Resource 	${CURDIR}/page/my_account/shipping_address.robot
Resource 	${CURDIR}/page/my_account/my_account_menu.robot
Resource 	${CURDIR}/page/my_account/billing_address.robot
Resource 	${CURDIR}/feature/login_feature.robot
Resource 	${CURDIR}/feature/checkoutProduct.robot
Resource 	${CURDIR}/feature/my_account_feature.robot
Resource 	${CURDIR}/../common/keywords/CommonKeywords.robot
Resource 	${CURDIR}/../common/keywords/CommonWebKeywords.robot
Resource 	${CURDIR}/../calculation.robot
Variables   ${CURDIR}/../../resources/testdata/common${/}web_translation_${language}.yaml

*** Variables ***
${testdata_filepath} 	${CURDIR}/../../resources/testdata/

*** Keywords ***
Global init
	Import Variables 	${testdata_filepath}${ENV.lower()}${/}test_data.yaml
	Import Variables 	${testdata_filepath}${ENV.lower()}${/}product_data.yaml
	Import Variables 	${testdata_filepath}${ENV.lower()}${/}mdc_data.yaml
	Set Global Variable 	${language} 	${language.lower()}