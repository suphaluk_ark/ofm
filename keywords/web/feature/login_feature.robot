*** Setting ***

*** Keywords ***
Login with Personal account
	header_menu.Select Log in button
	login.Display label in Login section
 	login.Input Email 	${personal_username}
 	login.Input Password 	${personal_password}
 	login.Select Log in button
 	header_menu.Display Log in firstname and lastname 	&{personal_information}[firstname] 	&{personal_information}[lastname]

Login with Corporate account
	header_menu.Select Log in button
	login.Display label in Login section
 	login.Input Email 	${corporate_username}
 	login.Input Password 	${corporate_password}
 	login.Select Log in button
 	# login.Display selecting account modal
 	# login.Select Corporate botton
 	header_menu.Display Log in corporate name 	&{corporate_information}[corporatename]

Login with Facebook account
	header_menu.Select Log in button
	login.Display label in Login section
	login.Select on facebook button
	login.Input facebook email 	${facebook_username}
	login.Input facebook password 	${facebook_password}
	login.Select on facebook login button
