*** Setting ***
Resource 	${CURDIR}/../web_common_keywords.robot

*** Keywords ***
Add product item to shopping cart
	[Documentation]    Search by Product name or SKU number
	[Arguments] 	${product}
	${product_quantity}= 	Set Variable 	${1}
	${product_sku}= 	Set Variable 	${product}
	header_menu.Search Product name or SKU number 	${product}
	productCategory.Select Add to cart button by SKU number 	${product}
	[Return] 	${product_quantity} 	${product_sku}

Add product item to cart from PLP and go to cart details page
	${product_quantity}= 	Set Variable 	${2}
	${product_sku}= 	Set Variable 	@{product_sku_list}[0]
	checkoutProduct.Add product item and adjust quantity to shopping cart 	${product_sku} 	${product_quantity}
	header_menu.Display product amount in shopping cart modal 	${product_quantity} 	${product_sku}
	header_menu.Go to Shopping cart
	[Return] 	${product_quantity} 	${product_sku}


Add product item from PDP to shopping cart
	[Arguments] 	${product_sku}
	header_menu.Search Product name or SKU number 	${product_sku}
	productCategory.Select on product item by SKU number 	${product_sku}
	Wait Until Keyword Succeeds 	3 x 	10 sec 	productPage.Select add to cart button 	${product_sku}

Add product item and adjust quantity to shopping cart
	[Arguments] 	${product} 	${quantity}
	header_menu.Search Product name or SKU number 	${product}
	productCategory.Input SKU quantity in category page 	${product} 	${quantity}
	productCategory.Select Add to cart button by SKU number 	${product}

Add product item and adjust quantity from product page
	[Arguments] 	${product}
	header_menu.Search Product name or SKU number 	${product}
	productCategory.Select on product item by SKU number 	${product}
	${quantity}= 	productPage.Add product quantity
	productPage.Select add to cart button 	${product}
	[return] 	${quantity}

Add multiple product items to shopping cart
	[Documentation]    Search by Product name or SKU number
	[Arguments] 	@{product}
	: FOR 	${product} 	IN 	@{product}
	\  Add product item to shopping cart 	${product}

Add multiple product and go to Shopping cart
	[Documentation]    Search by Product name or SKU number
	[Arguments] 	@{product}
	checkoutProduct.Add multiple product items to shopping cart 	@{product}
	${productAmount}= 	Get Length 	${product}
	header_menu.Display product item amount 	${productAmount}
	header_menu.Go to Shopping cart
	cart.Display Cart page 	@{product}
	order_summary.Display Order Summary section

Apply code and re-calculate Grand total
	[Arguments] 	${code}
	order_summary.Apply e-Coupon or Promotion 	${code}
	order_summary.Display Order Summary section

Apply shipping details
	[Arguments] 	@{product}
	Input Shipping details
	order_summary.Display Order Summary section
	checkout.Display product picture item in checkout page 	@{product}

Add wishlist item to shopping cart
	[Arguments] 	${sku_number_list} 	${quantity}=2 	${wishlist_group}=${wishlist_group_name}
	header_menu.Select on wishlist button
	wishlist.Select on wishlist group 	${wishlist_group}
	wishlist.Adjust product quantity in wishlist page 	@{sku_number_list}[0] 	${quantity}
	wishlist.Multiple add to cart in wishlist page by SKU number 	@{sku_number_list}
	${quantity}= 	wishlist.Summarize product quantity in wishlist page 	@{sku_number_list}
	header_menu.Display product item amount 	${quantity}

Order successful with COD/ CCOD
	[Documentation] 	Verify order status after checkout order immediately
	... 	MDC - status, payment method variables comming from global yaml file <mdc_data.yaml>
	... 	After calling this keyword ‘test variable’ order_id would be assigned
	success.Display success page
	${order_id}= 	success.Get order id from url
	verify_mdc.Verify mdc status filter by order_id 	${order_id} 	&{mdc_order_status}[pending]
	verify_mdc.Verify mdc payment method filter by order_id 	${order_id} 	&{mdc_payment_method}[cod]
	Set Test Variable 	${order_id} 	${order_id}

Order successful with billing payment
	#TBC

Add wishlist item from product category page
	[Arguments] 	${sku_number}
	header_menu.Search Product name or SKU number 	${sku_number}
	productCategory.Select add wishlist item in category page by SKU number 	${sku_number}

Add wishlist item from product page
	[Arguments] 	${sku_number}
	header_menu.Search Product name or SKU number 	${sku_number}
	productCategory.Select on product item by SKU number 	${sku_number}
	productPage.Select add wishlist item in product page by SKU number 	${sku_number}

Successfully place order by credit card
	[Arguments] 	${amount}=${summary_grand_total}
	payment.Pay order with credit card
	2c2p.Display product details in 2c2p page
	2c2p.Display product amount in 2c2p page 	${amount}
	${order_number}= 	2c2p.Get Order number in 2c2p page
	2c2p.Submit credit card
	2c2p.Proceed OTP
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec 	web_thankyou.Display thank you title
	[Return] 	${order_number}

Place order by online banking
	[Arguments] 	${amount}=${summary_grand_total} 	${bank_code}=BBL 	${phone_number}=0255555555
	payment.Pay order with online banking 	back_code=${bank_code} 	tel=${phone_number}
	2c2p.Display phone number in 123 page 	phone_number=${phone_number}
	2c2p.Select next button in 123 page
	2c2p.Display payment amount in 123 page 	payment_amount=${amount}
	2c2p.Select on back button in 123 page
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec 	web_thankyou.Display thank you title
	${url}= 	SeleniumLibrary.Get Location
	${order_number}= 	Evaluate 	$url.split('/')[-1]
	[Return] 	${order_number}

Place order by counter service
	[Arguments] 	${amount}=${summary_grand_total} 	${counter_type}=counter-service 	${phone_number}=0255555555
	payment.Pay order with counter services 	counter_type=${counter_type} 	tel=${phone_number}
	2c2p.Display phone number in 123 page 	phone_number=${phone_number}
	2c2p.Select next button in 123 page
	2c2p.Display payment amount in 123 page 	payment_amount=${amount}
	2c2p.Select on back button in 123 page
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec 	web_thankyou.Display thank you title
	${url}= 	SeleniumLibrary.Get Location
	${order_number}= 	Evaluate 	$url.split('/')[-1]
	[Return] 	${order_number}

Place order by cod
	[Arguments] 	${amount}=${summary_grand_total}
	header_menu.Wait until does not display full page load
	Wait Until Keyword Succeeds 	3x 	10 sec 	header_menu.Mouse over on checkout OFM logo
	payment.Pay order with COD
	header_menu.Wait until does not display full page load
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec 	web_thankyou.Display thank you title
	${url}= 	SeleniumLibrary.Get Location
	${order_number}= 	Evaluate 	$url.split('/')[-1]
	${order_number}=	Get order id from order details response by entity_id 	${order_number}
	[Return] 	${order_number}

Place order by ccod
	[Arguments] 	${amount}=${summary_grand_total}
	header_menu.Wait until does not display full page load
	Wait Until Keyword Succeeds 	3x 	10 sec 	header_menu.Mouse over on checkout OFM logo
	payment.Pay order with CCOD
	header_menu.Wait until does not display full page load
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec 	web_thankyou.Display thank you title
	${url}= 	SeleniumLibrary.Get Location
	${order_number}= 	Evaluate 	$url.split('/')[-1]
	${order_number}=	Get order id from order details response by entity_id 	${order_number}
	[Return] 	${order_number}

Successfully place order by T1 credit card
	[Arguments] 	${amount}=${summary_grand_total}
	payment.Pay order with T1 credit card
	2c2p.Display product amount in 2c2p page 	${amount}
	${order_number}= 	2c2p.Get Order number in 2c2p page
	2c2p.Submit credit card 	card_number=5555555555554444
	2c2p.Proceed OTP
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec 	web_thankyou.Display thank you title
	[Return] 	${order_number}

Member select on Proceed to checkout button and go to checkout detials page
	order_summary.Select on Proceed to checkout button
	header_menu.Wait until does not display full page load
	checkout.Retry display shipping address title

Guest select on Proceed to checkout button and go to checkout detials page
	order_summary.Select on Proceed to checkout button
	header_menu.Wait until does not display full page load
	checkout.Retry display specify address title

Select on Proceed to checkout button and go to payment details page
	order_summary.Select on Proceed to checkout button
	header_menu.Wait until does not display full page load
	payment.Retry display selecting payment title











