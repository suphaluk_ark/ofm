*** Keywords ***
Update shipping address as edit value
	my_account_menu.Select on delivery information menu
	shipping_address.Select on edit button by address    &{shipping_address_${language}}[address]
	shipping_address.Input shipping address section     firstname=&{edit_shipping_address_${language}}[firstname]
    ...    lastname=&{edit_shipping_address_${language}}[lastname]
    ...    phone=&{edit_shipping_address_${language}}[phone]
    ...    address=&{edit_shipping_address_${language}}[address]
    ...    zip_code=&{edit_shipping_address_${language}}[zip_code]
    ...    region=&{edit_shipping_address_${language}}[region]
    ...    district=&{edit_shipping_address_${language}}[district]
    ...    sub_district=&{edit_shipping_address_${language}}[sub_district]
	shipping_address.Select on add new address button
	shipping_address.Display shipping address list 	firstname=&{edit_shipping_address_${language}}[firstname]
	... 	lastname=&{edit_shipping_address_${language}}[lastname]
	... 	address_name=&{edit_shipping_address_${language}}[address]
	... 	district=&{edit_shipping_address_${language}}[district]
	... 	sub_district=&{edit_shipping_address_${language}}[sub_district]
	... 	postcode=&{edit_shipping_address_${language}}[zip_code]
	... 	phone=&{edit_shipping_address_${language}}[phone]

Update billing address as edit value
	my_account_menu.Select on billing information menu
	billing_address.Select on edit button by address 	&{billing_address_${language}}[address]
	billing_address.Input billing address section     firstname=&{edit_billing_address${language}}[firstname]
    ...    lastname=&{edit_billing_address${language}}[lastname]
    ...    phone=&{edit_billing_address${language}}[phone]
    ...    address=&{edit_billing_address${language}}[address]
    ...    zip_code=&{edit_billing_address${language}}[zip_code]
    ...    region=&{edit_billing_address${language}}[region]
    ...    district=&{edit_billing_address${language}}[district]
    ...    sub_district=&{edit_billing_address${language}}[sub_district]
    ...    tax_id=&{edit_billing_address${language}}[guest_tax_id]
	billing_address.Select on add new address button
	billing_address.Display billing address list 	firstname=&{edit_billing_address_${language}}[firstname]
	... 	lastname=&{edit_billing_address_${language}}[lastname]
	... 	address_name=&{edit_billing_address_${language}}[address]
	... 	district=&{edit_billing_address_${language}}[district]
	... 	sub_district=&{edit_billing_address_${language}}[sub_district]
	... 	tax_id=&{edit_billing_address_${language}}[guest_tax_id]
	... 	postcode=&{edit_billing_address_${language}}[zip_code]
	... 	phone=&{edit_billing_address_${language}}[phone]

Update shipping address as default value
	my_account_menu.Select on delivery information menu
	shipping_address.Select on edit button by address    &{edit_shipping_address_${language}}[address]
	shipping_address.Input shipping address section     firstname=&{shipping_address_${language}}[firstname]
    ...    lastname=&{shipping_address_${language}}[lastname]
    ...    phone=&{shipping_address_${language}}[phone]
    ...    address=&{shipping_address_${language}}[address]
    ...    zip_code=&{shipping_address_${language}}[zip_code]
    ...    region=&{shipping_address_${language}}[region]
    ...    district=&{shipping_address_${language}}[district]
    ...    sub_district=&{shipping_address_${language}}[sub_district]
    shipping_address.Select on add new address button
	shipping_address.Display shipping address list 	firstname=&{shipping_address_${language}}[firstname]
	... 	lastname=&{shipping_address_${language}}[lastname]
	... 	address_name=&{shipping_address_${language}}[address]
	... 	district=&{shipping_address_${language}}[district]
	... 	sub_district=&{shipping_address_${language}}[sub_district]
	... 	postcode=&{shipping_address_${language}}[zip_code]
	... 	phone=&{shipping_address_${language}}[phone]

Update billing address as default value
	my_account_menu.Select on billing information menu
	billing_address.Select on edit button by address 	&{edit_billing_address_${language}}[address]
	billing_address.Input billing address section     firstname=&{billing_address${language}}[firstname]
    ...    lastname=&{billing_address${language}}[lastname]
    ...    phone=&{billing_address${language}}[phone]
    ...    address=&{billing_address${language}}[address]
    ...    zip_code=&{billing_address${language}}[zip_code]
    ...    region=&{billing_address${language}}[region]
    ...    district=&{billing_address${language}}[district]
    ...    sub_district=&{billing_address${language}}[sub_district]
    ...    tax_id=&{billing_address${language}}[guest_tax_id]
    billing_address.Select on add new address button
	billing_address.Display billing address list 	firstname=&{billing_address_${language}}[firstname]
	... 	lastname=&{billing_address_${language}}[lastname]
	... 	address_name=&{billing_address_${language}}[address]
	... 	district=&{billing_address_${language}}[district]
	... 	sub_district=&{billing_address_${language}}[sub_district]
	... 	tax_id=&{billing_address_${language}}[guest_tax_id]
	... 	postcode=&{billing_address_${language}}[zip_code]
	... 	phone=&{billing_address_${language}}[phone]
