*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{headerMenu} 	label=xpath=//*[.='{label}']
... 	searchTXT=css=[placeholder*='{label}']
... 	searchBTN=css=img[src*='search']
... 	cartBTN=css=img[src*='cart-outline']
... 	lbl_itemAmount=css=img[src*='cart-outline']+span
... 	img_user=css=img[src*='ic_user']
... 	btn_modal=xpath=//img[contains(@src,'ic_user')]/following::a[text()='{label}']
... 	btn_modal_logout=xpath=//img[contains(@src,'ic_user')]/following::li[text()='{label}']
... 	img_ofm_logo=css=img[src*='ofm-white-color']
... 	btn_wishlist=xpath=//img[contains(@src,'logo')]/../following-sibling::div//img[contains(@src,'heart-outline')]
... 	btn_cart=xpath=//object[contains(@data,'logo')]/../following-sibling::div//img[contains(@src,'cart')]
... 	btn_modal_delete_by_sku=id=mini-cart--delete--{sku_number}
... 	btn_modal_product=css=#mini-cart--items #mini-cart--{sku_number}
... 	img_modal_product=css=#mini-cart--items img[src*='product']
... 	btn_modal_delete=css=#mini-cart--items img[src*='delete']
... 	img_logo=xpath=//object[contains(@data,'logo')]
... 	btn_login=xpath=//object[contains(@data,'logo')]/preceding::*[text()='{label}']
... 	img_loading=css=#mini-cart img[src*='Rolling']
... 	img_empty_cart=css=#mini-cart--empty [src*='ic_emptycart']
... 	checkout_img_logo=css=img[src*=logo]
... 	icon_fullpageloader=id=full-page-loader

*** Keywords ***
Select Log in button
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[btn_login] 	label=&{web_common}[login]
	CommonWebKeywords.Click Element 	${elem}

Display Log in firstname and lastname
	[Arguments] 	${firstname} 	${lastname}
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[label] 	label=${firstname} ${lastname}
	Wait Until Keyword Succeeds 	3 x 	5 sec 	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Display Log in corporate name
	[Arguments] 	${corporateName}
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[label] 	label=${corporateName}
	Wait Until Keyword Succeeds 	3 x 	5 sec 	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Input Product name or SKU number
	[Arguments] 	${product}
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[searchTXT] 	label=${search_placeholder}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	SeleniumLibrary.Clear Element Text 	${elem}
	CommonWebKeywords.Input Text And Verify Input For Web Element 	${elem} 	${product}

Select Search product button
	CommonWebKeywords.Click Element 	&{headerMenu}[searchBTN]

Search Product name or SKU number
	[Arguments] 	${product}
	Input Product name or SKU number 	${product}
	Select Search product button

Select on Shopping cart icon
	BuiltIn.Wait Until Keyword Succeeds  ${global_retry_time} x  ${global_retry_in_sec} sec  CommonWebKeywords.Click Element 	&{headerMenu}[cartBTN]

Display product item amount
	[Arguments] 	${amount}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{headerMenu}[lbl_itemAmount]
	${actualAmount}= 	SeleniumLibrary.Get Text 	&{headerMenu}[lbl_itemAmount]
	Should be True 	${amount}==${actualAmount}

Display product item amount from api filter by username
	[Arguments] 	${username} 	${password}
	${item_qty} 	api_customer_details.Get items quantity from customer cart response 	${username} 	${password}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{headerMenu}[lbl_itemAmount]
	${actualAmount}= 	SeleniumLibrary.Get Text 	&{headerMenu}[lbl_itemAmount]
	Should be True 	${item_qty}==${actualAmount}

Select on View cart button
	CommonWebKeywords.Verify Web Elements Are Not Visible	&{headerMenu}[img_loading]
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[label] 	label=&{web_common}[view_cart]
	Click Element 	${elem}
	header_menu.Wait until does not display full page load

Go to Shopping cart
	Select on Shopping cart icon
	Select on View cart button

Mouse over on user account
	CommonWebKeywords.Mouse over 	&{headerMenu}[img_user]

Select order history in modal
 	Mouse over on user account
 	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[btn_modal] 	label=&{web_common}[order_history]
 	CommonWebKeywords.Click Element 	${elem}

Select manage account in modal
	Mouse over on user account
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[btn_modal] 	label=&{web_common}[manage_account]
	CommonWebKeywords.Click Element 	${elem}

Select OFM logo
	CommonWebKeywords.Click Element 	&{headerMenu}[img_ofm_logo]

Select on wishlist button
	CommonWebKeywords.Click Element 	&{headerMenu}[btn_wishlist]

Select log out in modal
	Mouse over on user account
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[btn_modal_logout] 	label=&{web_common}[log_out]
	CommonWebKeywords.Click Element 	${elem}
	header_menu.Display log in button

Display log in button
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[label] 	label=&{web_common}[login]
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Select on cart button in header menu
	Wait Until Keyword Succeeds 	3x 	10 sec
	... 	CommonWebKeywords.Click Element 	&{headerMenu}[btn_cart]

Select on delete buttin in header menu
	[Arguments] 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[btn_modal_product] 	sku_number=${sku_number}
	CommonWebKeywords.Mouse over 	${elem}
	${elem}= 	CommonKeywords.Format Text 	&{headerMenu}[btn_modal_delete_by_sku] 	sku_number=${sku_number}
	CommonWebKeywords.Click Element 	${elem}

Remove product item from shopping cart modal
	[Arguments] 	${sku_number}
	header_menu.Select on cart button in header menu
	header_menu.Select on delete buttin in header menu 	${sku_number}

Display product amount in shopping cart modal
	[Arguments] 	${cart_quantity} 	@{product_list}
	header_menu.Select on cart button in header menu
	${len}= 	Get Length 	${product_list}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{headerMenu}[img_modal_product]
	${elems}= 	SeleniumLibrary.Get WebElements 	&{headerMenu}[img_modal_product]
	${actual_amount}= 	Get Length 	${elems}
	Should be True 	${len} == ${actual_amount}
	Mouse over on OFM logo
	BuiltIn.Wait Until Keyword Succeeds  3 x  10 sec  Display product item amount 	${cart_quantity}

Remove and display product amount in shipping cart modal
	[Arguments] 	${sku_number} 	@{product_list}
	Remove product item from shopping cart modal 	${sku_number}
	Display product amount in shopping cart modal 	@{product_list}

Remove all product items in shopping cart
	header_menu.Select on cart button in header menu
	CommonWebKeywords.Verify Web Elements Are Visible 	&{headerMenu}[img_modal_product]
	${elems}= 	SeleniumLibrary.Get WebElements 	&{headerMenu}[img_modal_product]
	${len}= 	Get Length 	${elems}
	: FOR 	${index} 	IN RANGE 	${len}
	\  CommonWebKeywords.Mouse over 	&{headerMenu}[img_modal_product]
	\  CommonWebKeywords.Click Element 	&{headerMenu}[btn_modal_delete]
	\  Run Keyword If 	${${index+1}} != ${len} 	Wait Until Keyword Succeeds 	3 x 	10 sec 	CommonWebKeywords.Verify Web Elements Are Visible 	&{headerMenu}[img_modal_product]
	\  ${elems}= 	Run Keyword If 	${${index+1}} != ${len} 	SeleniumLibrary.Get WebElements 	&{headerMenu}[img_modal_product]
	\  ${after_remove_len}= 	Run Keyword If 	${${index+1}} != ${len} 	Get Length 	${elems}
	\  ... 	ELSE 	Set Variable 	${0}
	\  Should be True 	${${len}-${${index}+1}} == ${after_remove_len}
	Mouse over on OFM logo

Display empty item in shopping cart
	Select on Shopping cart icon
	CommonWebKeywords.Verify Web Elements Are Visible	&{headerMenu}[img_empty_cart]

Mouse over on OFM logo
	CommonWebKeywords.Mouse over 	&{headerMenu}[img_logo]

Mouse over on checkout OFM logo
	CommonWebKeywords.Mouse over 	&{headerMenu}[checkout_img_logo]

Wait until does not display full page load
	Wait Until Keyword Succeeds 	3 x 	10 sec 	CommonWebKeywords.Verify Web Elements Are Not Visible 	&{headerMenu}[icon_fullpageloader]

Go to order history
	Mouse over on user account
	Select order history in modal
