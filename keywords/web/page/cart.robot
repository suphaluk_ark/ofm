*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{cart} 	img_productItem=css=img[src*='{skuNumber}']
... 	lal_eCoupon=css=input[placeholder*='{label}']
... 	label=xpath=//div[@id='cart-items']//*[.='{label}']
... 	lal_product_description=xpath=//*[contains(@src,'{skuNumber}')]/parent::div/..
... 	lal_quantity=xpath=//div[@data-productid='{skuNumber}']/following-sibling::div/input
... 	btn_delete=xpath=//*[contains(@src,'{skuNumber}')]/../../following-sibling::div//img[contains(@src,'delete')]
... 	modal_delete=css=.sweet-alert
... 	lal_delete_modal=xpath=//*[contains(@class,'sweet-alert')]//*[text()='{label}']
... 	img_products=xpath=//*[contains(@src,'product') and @height='auto']
... 	btn_remove_qty=xpath=//span[contains(.,'{label}')]/../../../following-sibling::div//div[text()='-']
... 	btn_quick_add=xpath=//*[contains(@id,'random') and contains(text(),'{label}')]

*** Keywords ***
Display cart page
	[Arguments] 	@{productList}
	Display Shopping cart section 	@{productList}

Display shopping cart section
	[Arguments] 	@{productList}
	${elem}= 	CommonKeywords.Format Text 	&{cart}[label] 	label=&{web_common}[shopping_cart]
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	Display product item in shopping cart page 	@{productList}

Display product item in shopping cart page
	[Documentation] 	This keyword receive SKU number as a list that looping verify each product picture,product description and product amount
	... 	After calling this keyword ‘test variable’ sum_per_product_price would be assigned
	[Arguments] 	@{productList}
	${price_list} 	Create List
	: FOR 	${product} 	IN 	@{productList}
	\  ${elem}= 	CommonKeywords.Format Text 	&{cart}[img_productItem] 	skuNumber=${product}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	\  ${elem}= 	CommonKeywords.Format Text 	&{cart}[lal_product_description] 	skuNumber=${product}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	\  ${elem}= 	SeleniumLibrary.Get WebElement 	${elem}
	\  ${elem_text}= 	SeleniumLibrary.Get Text 	${elem}
	\  ${elem}= 	CommonKeywords.Format Text 	&{cart}[lal_quantity] 	skuNumber=${product}
	\  ${quantity}= 	SeleniumLibrary.Get Value 	${elem}
	\  Display product description in shopping cart page 	${elem_text} 	${product} 	${quantity}
	\  Append To List 	${price_list} 	${price_with_quantity}
	Set Test Variable 	${sum_per_product_price} 	${price_list}

Display product description in shopping cart page
	[Documentation] 	This keyword receive text from web element and use Test Variable from calculation.robot
	[Arguments] 	${elem_text} 	${skuNumber} 	${quantity}
	${product}= 	Set Variable 	${${skuNumber}}
	calculation.Summarize price with quantity 	${quantity} 	&{product}[price]
	: FOR 	${text} 	IN 	&{product}[product_name]
	\  ... 	&{web_common}[product_code]: ${skuNumber}
	\  ... 	฿ &{product}[price]
	\  ... 	(\n&{web_common}[without_total] VAT\n)
	\  ... 	฿ ${price_with_quantity_format}
	\  ... 	(&{web_common}[total] VAT)
	\  Should be True 	$text in $elem_text

Select on delete button in cart page
	[Arguments] 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{cart}[btn_delete] 	skuNumber=${sku_number}
	CommonWebKeywords.Click Element 	${elem}

Display product amount in cart page
	[Arguments] 	@{product_list}
	${len}= 	Get Length 	${product_list}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{cart}[img_products]
	${elems}= 	SeleniumLibrary.Get WebElements 	&{cart}[img_products]
	${actual_amount}= 	Get Length 	${elems}
	Should be True 	${len} == ${actual_amount}

Remove and display product amount in cart page
	[Arguments] 	${sku_number} 	@{product_list}
	Select on delete button in cart page 	${sku_number}
	Select on remove in modal cart
	Display product amount in cart page 	@{product_list}

Display delete in modal cart
	CommonWebKeywords.Verify Web Elements Are Visible 	&{cart}[modal_delete]
	${text}= 	SeleniumLibrary.Get Text 	&{cart}[modal_delete]
	Should be True 	$delete_item.message1 in $text
	Should be True 	$delete_item.message2 in $text

Select on remove in modal cart
	Display delete in modal cart
	${elem}= 	CommonKeywords.Format Text 	&{cart}[lal_delete_modal] 	label=${web_common.remove.upper()}
	CommonWebKeywords.Click Element	${elem}

Remove product quantity in cart page
	[Arguments] 	${sku_number}
	${elem_qty}= 	CommonKeywords.Format Text 	&{cart}[lal_quantity] 	skuNumber=${sku_number}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem_qty}
	${befor_remove}= 	SeleniumLibrary.Get Value 	${elem_qty}
	${elem}= 	CommonKeywords.Format Text 	&{cart}[btn_remove_qty] 	label=${sku_number}
	CommonWebKeywords.Click Element	${elem}
	${after_remove}= 	SeleniumLibrary.Get Value 	${elem_qty}
	Should Be True 	${${befor_remove}-1}==${after_remove}

Get product quantity by SKU in cart page
	[Arguments] 	@{sku_number}
	${quantity_list} 	Create List
	: FOR 	${sku} 	IN 	@{sku_number}
	\  ${locator}= 	CommonKeywords.Format Text 	&{cart}[lal_quantity] 	skuNumber=${sku}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${locator}
	\  ${qty}= 	Get Value 	${locator}
	\  Append To List 	${quantity_list} 	${qty}
	[Return] 	${quantity_list}



