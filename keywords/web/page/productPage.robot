*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{productPage} 	addProductBTN=xpath=//div[contains(.,'{product}')]/following::span[contains(.,'{label}')]
... 	reviewStarIcon=xpath=//div[contains(.,'{product}')]/following::img[contains(@src,'star')]
... 	lbl_loading=xpath=//div[contains(.,'{product}')]/following::span[contains(.,'{label}')]
... 	lbl_label=xpath=//*[.='{label}']
... 	btn_wishlist=xpath=//div[contains(.,'{product}')]/following::img[contains(@src,'wishlist')]
... 	btn_wishlist_group=xpath=//div[contains(.,'{product}')]//div[contains(text(),'{wishlisy_group}')]
... 	btn_wishlist_enabled=xpath=//div[contains(.,'{product}')]/following::img[contains(@src,'wishlist_solid')]
... 	btn_product_image=css=img[src*='{product}']
... 	btn_add_qty=xpath=//*[text()='+']
... 	btn_remove_qty=xpath=//*[text()='-']
... 	lbl_quantity=xpath=//*[text()='+']//preceding-sibling::div//input
... 	btn_add_to_cart=id=btn-addCart-{product}
... 	lbl_loading=xpath=//div[contains(.,'{product}')]/following::span[contains(.,'{label}')]

*** Keywords ***
Mouse over on SKU number in product page
	[Arguments] 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{productPage}[lbl_label] 	label=&{web_common}[sku]${SPACE}:${SPACE}${sku_number}
	CommonWebKeywords.Mouse over 	${elem}

Select add wishlist item in product page by SKU number
	[Arguments] 	${sku_number} 	${wishlist_group}=${wishlist_group_name}
	${elem}= 	CommonKeywords.Format Text 	&{productPage}[btn_wishlist] 	product=${sku_number}
	Mouse over on SKU number in product page 	${sku_number}
	CommonWebKeywords.Click Element 	${elem}
	Select wishlist group in modal in product page 	${sku_number}
	Display enabled wishlist icon in product page 	${sku_number}

Select wishlist group in modal in product page
	[Arguments] 	${sku_number} 	${wishlist_group}=${wishlist_group_name}
	${elem}= 	CommonKeywords.Format Text 	&{productPage}[btn_wishlist_group] 	product=${sku_number} 	wishlisy_group=${wishlist_group}
	CommonWebKeywords.Click Element 	${elem}

Display enabled wishlist icon in product page
	[Arguments] 	${sku_number} 	${wishlist_group}=${wishlist_group_name}
	${elem}= 	CommonKeywords.Format Text 	&{productPage}[btn_wishlist_enabled] 	product=${sku_number}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Add product quantity
	CommonWebKeywords.Verify Web Elements Are Visible 	&{productPage}[lbl_quantity]
	${qty}= 	SeleniumLibrary.Get Value 	&{productPage}[lbl_quantity]
	CommonWebKeywords.Click Element 	&{productPage}[btn_add_qty]
	CommonWebKeywords.Verify Web Elements Are Visible 	&{productPage}[lbl_quantity]
	${actual_qty}= 	SeleniumLibrary.Get Value 	&{productPage}[lbl_quantity]
	Should be True 	${${${qty}}+1} == ${actual_qty}
	[return] 	${${${qty}}+1}

Remove product quantity
	CommonWebKeywords.Verify Web Elements Are Visible 	&{productPage}[lbl_quantity]
	${qty}= 	SeleniumLibrary.Get Value 	&{productPage}[lbl_quantity]
	CommonWebKeywords.Click Element 	&{productPage}[btn_remove_qty]
	CommonWebKeywords.Verify Web Elements Are Visible 	&{productPage}[lbl_quantity]
	${actual_qty}= 	SeleniumLibrary.Get Value 	&{productPage}[lbl_quantity]
	Should be True 	${${${qty}}-1}} == ${actual_qty}

Select add to cart button
	[Arguments] 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{productPage}[btn_add_to_cart] 	product=${sku_number}
	CommonWebKeywords.Click Element 	${elem}

	${elem}= 	CommonKeywords.Format Text 	&{productPage}[lbl_loading] 	product=${sku_number} 	label=&{web_common}[loading]...
	CommonWebKeywords.Verify Web Elements Are Not Visible 	${elem}






