*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{checkout} 	txt_firstName=name=firstName
... 	txt_lastName=name=lastName
... 	txt_phone=name=phone
... 	txt_email=name=email
... 	txt_t1c=name=t1c
... 	txt_address=name=address
... 	txt_zipCode=name=zipCode
... 	sel_subDistrict=name=subDistrict
... 	sel_district=name=district
... 	sel_region=name=region
... 	txt_GuestTaxId=name=Guest Tax Id
... 	lbl_label=xpath=//*[text()='{label}']
... 	img_productItem=css=img[src*='{skuNumber}']
... 	img_flat_rate=css=#chk-checkout-seletShippingMethod-ofm img[src*='checked']
... 	lbl_billing_address=xpath=//*[text()='{label}']/following::div[@id='checkout-address']
... 	lbl_shipping_address=css=#CheckoutShippingSelector ~ #checkout-address
... 	lbl_tax_id=css=[id*=CheckoutBillingDetailsForm-tax_id]
... 	btn_edit_billing=xpath=//*[text()='{label}']/following::*[@id='edit-shipping']
... 	modal_billing_address=xpath=//*[contains(@class,'ReactModal__Content')]//*[text()='{title}']
... 	modal_alert=css=.sweet-alert
... 	lbl_product_description=xpath=//img[contains(@src,'{sku_number}')]/../..
... 	lbl_product_quantity=xpath=//img[contains(@src,'{sku_number}')]/following::span[contains(text(),'{label}')]/..
... 	btn_edit_billing_modal=xpath=//*[contains(@class,'ReactModal__Content')]//*[contains(text(),'{address}')]/following::div[contains(@id,'btn-billing-editLabel')]
... 	txt_billing_firstname=id=txt-billingAddressForm-firstName
... 	txt_billing_lastname=id=txt-billingAddressForm-lastName
... 	txt_billing_address=id=txt-billingAddressForm-address
... 	txt_billing_zipcode=id=txt-billingAddressForm-zipCode
... 	txt_billing_tax=id=txt-billingAddressForm-vat_id
... 	sel_billing_subDistrict=id=sel-billingAddressForm-subDistrict
... 	sel_billing_district=id=sel-billingAddressForm-district
... 	sel_billing_region=id=sel-billingAddressForm-region
... 	btn_billing_save=id=btn-billingAddressForm-submitButton
... 	btn_choose_btn=id=btn-checkoutAddressModal-saveSelectedbilling
... 	chk_shipMethod=css=#chk-checkout-selectShippingMethod-ofm img[src*=checked]
... 	chk_pickupStore=css=#chk-checkout-selectShippingMethod-pickupatstore>div>div
... 	txt_pickup_postcode=id=txt-checkout-pickupAtStoreModal-search
... 	btn_pickup_search=id=btn-checkout-pickupAtStoreModal-search
... 	btn_pickup_location=xpath=//div[contains(@id,'btn-pickupAtStoreModal-StoreListItem-selectStore')]//h3[contains(text(),'{location}')]
... 	btn_pickup=css=[id*=btn-checkout-pickupAtStoreModal-infoBox-setLocationToShipping]
... 	txt_pickup_firstname=id=txt-pickupAtStorePickerForm-firstName
... 	txt_pickup_lastname=id=txt-pickupAtStorePickerForm-lastName
... 	txt_pickup_phone=id=txt-pickupAtStorePickerForm-phone
... 	txt_pickup_email=id=txt-pickupAtStorePickerForm-email
... 	txt_billing_guest_company=id=txt-guestBillingAddressForm-address
... 	txt_billing_guest_address=id=txt-guestBillingAddressForm-address2
... 	txt_billing_guest_postcode=id=txt-guestBillingAddressForm-zipCode
... 	sel_billing_guest_region=id=sel-guestBillingAddressForm-region
... 	sel_billing_guest_district=id=sel-guestBillingAddressForm-district
... 	sel_billing_guest_sub_district=id=sel-guestBillingAddressForm-subDistrict
... 	txt_billing_guest_taxid=id=txt-guestBillingAddressForm-vat_id
...		lbl_member_title=xpath=//div[@id='checkout-page']//h2[contains(text(),'{title}')]
...		lbl_guest_title=xpath=//div[@id='checkout-page']//h3[contains(text(),'{title}')]

*** Keywords ***
Input Shipping details
	[Arguments] 	${firstName}=&{shipping_address_${language}}[firstname]
	... 	${lastName}=&{shipping_address_${language}}[lastname]
	... 	${phone}=&{shipping_address_${language}}[phone]
	... 	${email}=&{shipping_address_${language}}[email]
	... 	${the1Card}=&{shipping_address_${language}}[the_1_card]
	... 	${address}=&{shipping_address_${language}}[address]
	... 	${zipCode}=&{shipping_address_${language}}[zip_code]
	... 	${region}=&{shipping_address_${language}}[region]
	... 	${district}=&{shipping_address_${language}}[district]
	... 	${subDistrict}=&{shipping_address_${language}}[sub_district]
	# ... 	${guestTaxId}=&{shipping_address_${language}}[guest_tax_id]
	Display Shipping details section
	checkout.Input Shipping Firstname 	${firstName}
	checkout.Input Shipping Lastname 	${lastName}
	checkout.Input Shipping Phone 	${phone}
	checkout.Input Shipping Email 	${email}
	checkout.Input Shipping The 1 Card 	${the1Card}
	checkout.Input Shipping Address 	${address}
	checkout.Input Shipping Zip Code 	${zipCode}
	checkout.Select Shipping Region 	${region}
	checkout.Select Shipping District 	${district}
	checkout.Select Shipping Sub District 	${subDistrict}
	# checkout.Input Shipping Guest Tax ID 	${guestTaxId}

Display Shipping details section
	Display Checkout label 	&{web_common}[first_name]
	... 	&{web_common}[last_name]
	... 	&{web_common}[phone]
	... 	&{web_common}[email]
	... 	&{web_common}[the1Card]
	... 	&{web_common}[address]
	... 	&{web_common}[zip_code]
	... 	&{web_common}[sub_district]
	... 	&{web_common}[district]
	... 	&{web_common}[region]
	... 	&{web_common}[shipping_details]

Display Checkout label
	[Arguments] 	@{textList}
	: FOR 	${text} 	IN 	@{textList}
	\  ${elem}= 	CommonKeywords.Format Text 	&{checkout}[lbl_label] 	label=${text}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Input Shipping Firstname
	[Arguments] 	${firstname}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_firstName]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_firstName] 	${firstname}

Input Shipping Lastname
	[Arguments] 	${lastname}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_lastName]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_lastName] 	${lastname}

Input Shipping Phone
	[Arguments] 	${phone}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_phone]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_phone] 	${phone}

Input Shipping Email
	[Arguments] 	${email}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_email]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_email] 	${email}

Input Shipping The 1 Card
	[Arguments] 	${the1Card}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_t1c]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_t1c] 	${the1Card}

Input Shipping Address
	[Arguments] 	${address}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_address]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_address] 	${address}

Input Shipping Zip Code
	[Arguments] 	${zipCode}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_zipCode]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_zipCode] 	${zipCode}

Select Shipping Sub District
	[Arguments] 	${subDistrict}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[sel_subDistrict]
	CommonWebKeywords.Select dropdownlist by label 	&{checkout}[sel_subDistrict] 	${subDistrict}

Select Shipping District
	[Arguments] 	${district}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[sel_district]
	CommonWebKeywords.Select dropdownlist by label 	&{checkout}[sel_district] 	${district}

Select Shipping Region
	[Arguments] 	${region}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[sel_region]
	CommonWebKeywords.Select dropdownlist by label 	&{checkout}[sel_region] 	${region}

Input Shipping Guest Tax ID
	[Arguments] 	${guestTaxID}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_GuestTaxId]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_GuestTaxId] 	${guestTaxID}

Display product item in checkout page
	[Arguments] 	@{productList}
	: FOR 	${product} 	IN 	@{productList}
	\  ${elem}= 	CommonKeywords.Format Text 	&{checkout}[img_productItem] 	skuNumber=${product}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	\  ${elem}= 	CommonKeywords.Format Text 	&{checkout}[lbl_product_description] 	sku_number=${product}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	\  ${elem}= 	SeleniumLibrary.Get WebElement 	${elem}
	\  ${elem_text}= 	SeleniumLibrary.Get Text 	${elem}
	\  ${elem}= 	CommonKeywords.Format Text 	&{checkout}[lbl_product_quantity] 	sku_number=${product} 	label=&{web_common}[quantity]
	\  ${quantity}= 	SeleniumLibrary.Get Element Attribute 	${elem} 	innerText
	\  checkout.Display product description in checkout page 	${elem_text} 	${product} 	${quantity}

Display product description in checkout page
	[Arguments] 	${elem_text} 	${sku_number} 	${quantity}
	${product}= 	Set Variable 	${${skuNumber}}
	calculation.Summarize price with quantity 	${quantity} 	&{product}[price]
	${price_exclude_vat}= 	Evaluate 	&{product}[price] * ${quantity}
	: FOR 	${text} 	IN 	&{product}[product_name]
	\  ... 	&{web_common}[product_code] ${skuNumber}
	\  ... 	${quantity}
	\  ... 	฿ ${price_exclude_vat}
	\  ... 	( ฿ ${price_with_quantity_format}${SPACE}&{web_common}[total] VAT )
	\  Should be True 	$text in $elem_text

Display checked flat rate checkbox
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[img_flat_rate]

Display billing address in checkout page
	[Arguments] 	${company_name}
	... 	${address}
	... 	${postcode}
	... 	${district}
	... 	${sub_district}
	... 	${region}
	... 	${phone}
	${elem}= 	CommonKeywords.Format Text 	&{checkout}[lbl_billing_address] 	label=&{web_common}[tax_invoice]
	CommonWebKeywords.Verify Web Elements Are Visible   ${elem}
    ${text}=    SeleniumLibrary.Get Element Attribute   ${elem}     innerText
    Should Match Regexp     ${text} 	${company_name}\n${address}\n${sub_district}, ${district}, ${region}, ${postcode}

Display shipping address in checkout page
	[Arguments] 	${firstname}
	... 	${lastname}
	... 	${address}
	... 	${postcode}
	... 	${district}
	... 	${sub_district}
	... 	${region}
	... 	${phone}
	CommonWebKeywords.Verify Web Elements Are Visible   &{checkout}[lbl_shipping_address]
    ${text}=    SeleniumLibrary.Get Element Attribute   &{checkout}[lbl_shipping_address]     innerText
    Should Match Regexp 	${text} 	${firstname} ${lastname}\n${address}\n${sub_district}, ${district}, ${region}, ${postcode}\n${phone}

Display tax id in checkout page
	[Arguments] 	${tax_id}
	Verify Web Elements Are Visible 	&{checkout}[lbl_tax_id]
	${text}= 	SeleniumLibrary.Get Text 	&{checkout}[lbl_tax_id]
	Should be True 	'${tax_id}'=='${text}'

Display shipping address for member account
	[Arguments] 	${firstname}
	... 	${lastname}
	... 	${address}
	... 	${postcode}
	... 	${district}
	... 	${sub_district}
	... 	${region}
	... 	${phone}
	checkout.Display home delivery checkbox
	checkout.Display shipping address in checkout page 	firstname=${firstname}
	... 	lastname=${lastname}
	... 	address=${address}
	... 	district=${district}
	... 	sub_district=${sub_district}
	... 	region=${region}
	... 	postcode=${postcode}
	... 	phone=${phone}

Display billing address for member account
	[Arguments] 	${company_name}
	... 	${address}
	... 	${postcode}
	... 	${district}
	... 	${sub_district}
	... 	${region}
	... 	${phone}
	... 	${tax_id}
	checkout.Display billing address in checkout page 	company_name=${company_name}
	... 	address=${address}
	... 	district=${district}
	... 	sub_district=${sub_district}
	... 	region=${region}
	... 	postcode=${postcode}
	... 	phone=${phone}
	checkout.Display tax id in checkout page 	${tax_id}

Select on edit billing button
	${elem}= 	CommonKeywords.Format Text 	&{checkout}[btn_edit_billing] 	label=&{web_common}[tax_invoice]
	CommonWebKeywords.Click Element 	${elem}

Display billing address modal in checkout page
	${elem}= 	CommonKeywords.Format Text 	&{checkout}[modal_billing_address] 	title=&{web_common}[select_billing_address]
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Display default billing address in checkout page
	checkout.Display billing address for member account 	company_name=&{billing_address_${language}}[company_name]
	... 	address=&{billing_address_${language}}[address]
	... 	district=&{billing_address_${language}}[district]
	... 	sub_district=&{billing_address_${language}}[sub_district]
	... 	region=&{billing_address_${language}}[region]
	... 	postcode=&{billing_address__${language}}[zip_code]
	... 	phone=&{billing_address_${language}}[phone]
	... 	tax_id=&{billing_address_${language}}[guest_tax_id]

Display default shipping address in checkout page
	checkout.Display shipping address for member account 	firstname=&{shipping_address_${language}}[firstname]
	... 	lastname=&{shipping_address_${language}}[lastname]
	... 	address=&{shipping_address_${language}}[address]
	... 	district=&{shipping_address_${language}}[district]
	... 	sub_district=&{shipping_address_${language}}[sub_district]
	... 	region=&{shipping_address_${language}}[region]
	... 	postcode=&{shipping_address__${language}}[zip_code]
	... 	phone=&{shipping_address_${language}}[phone]

Display checkout page and default address when access via member
	[Arguments] 	@{product}
	checkout.Retry display shipping address title
	checkout.Display default shipping address in checkout page
	checkout.Display default billing address in checkout page
	checkout.Display product item in checkout page 	@{product}

Select on edit button in billing modal by address name
	[Arguments] 	${address}
	${elem}= 	CommonKeywords.Format Text 	&{checkout}[btn_edit_billing_modal] 	address=${address}
	CommonWebKeywords.Click Element 	${elem}

Input billing firstname field in checkout modal
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_firstname]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_firstname] 	${value}

Input billing lastname field in checkout modal
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_lastname]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_lastname] 	${value}

Input billing address field in checkout modal
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_address]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_address] 	${value}

Input billing zip code field in checkout modal
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_zipcode]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_zipcode] 	${value}

Input billing tax id field in checkout modal
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_tax]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_tax] 	${value}

Select billing district field in checkout modal
	[Arguments] 	${value}
	Select dropdownlist by label 	&{checkout}[sel_billing_district] 	${value}

Select billing sub district field in checkout modal
	[Arguments] 	${value}
	Select dropdownlist by label 	&{checkout}[sel_billing_subDistrict] 	${value}

Select billing region field in checkout modal
	[Arguments] 	${value}
	Select dropdownlist by label 	&{checkout}[sel_billing_region] 	${value}

Select on billing save button in checkout modal
 	CommonWebKeywords.Click Element 	&{checkout}[btn_billing_save]

Select on select billing address button in checkout modal
	CommonWebKeywords.Click Element 	&{checkout}[btn_choose_btn]

Input billing address in checkout modal
	[Arguments] 	${firstname}
	... 	${lastname}
	... 	${address}
	... 	${zip_code}
	... 	${tax_id}
	... 	${district}
	... 	${sub_district}
	... 	${region}
	Input billing firstname field in checkout modal 	${firstname}
	Input billing lastname field in checkout modal 	${lastname}
	Input billing address field in checkout modal 	${address}
	Input billing zip code field in checkout modal 	${zip_code}
	Input billing tax id field in checkout modal 	${tax_id}
	Select billing region field in checkout modal 	${region}
	Select billing district field in checkout modal 	${district}
	Select billing sub district field in checkout modal 	${sub_district}

Edit billing address in checkout page
	[Arguments] 	${select_address}=&{billing_address_${language}}[address]
	... 	${firstname}=&{edit_billing_address_${language}}[firstname]
	... 	${lastname}=&{edit_billing_address_${language}}[lastname]
	... 	${address}=&{edit_billing_address_${language}}[address]
	... 	${zip_code}=&{edit_billing_address_${language}}[zip_code]
	... 	${tax_id}=&{edit_billing_address_${language}}[guest_tax_id]
	... 	${district}=&{edit_billing_address_${language}}[district]
	... 	${sub_district}=&{edit_billing_address_${language}}[sub_district]
	... 	${region}=&{edit_billing_address_${language}}[region]
	Select on edit billing button
	Display billing address modal in checkout page
	Select on edit button in billing modal by address name 	${select_address}
	Input billing address in checkout modal 	${firstname}
	... 	${lastname}
	... 	${address}
	... 	${zip_code}
	... 	${tax_id}
	... 	${district}
	... 	${sub_district}
	... 	${region}
	Select on billing save button in checkout modal
	Select on select billing address button in checkout modal

Display edited billing address in checkout page
	checkout.Display billing address for member account 	firstname=&{edit_billing_address_${language}}[firstname]
	... 	lastname=&{edit_billing_address_${language}}[lastname]
	... 	address=&{edit_billing_address_${language}}[address]
	... 	district=&{edit_billing_address_${language}}[district]
	... 	sub_district=&{edit_billing_address_${language}}[sub_district]
	... 	region=&{edit_billing_address_${language}}[region]
	... 	postcode=&{edit_billing_address__${language}}[zip_code]
	... 	phone=&{edit_billing_address_${language}}[phone]
	... 	tax_id=&{edit_billing_address_${language}}[guest_tax_id]

Display edited shipping address in checkout page
	checkout.Display shipping address for member account 	firstname=&{edit_shipping_address_${language}}[firstname]
	... 	lastname=&{edit_shipping_address_${language}}[lastname]
	... 	address=&{edit_shipping_address_${language}}[address]
	... 	district=&{edit_shipping_address_${language}}[district]
	... 	sub_district=&{edit_shipping_address_${language}}[sub_district]
	... 	region=&{edit_shipping_address_${language}}[region]
	... 	postcode=&{edit_shipping_address__${language}}[zip_code]
	... 	phone=&{edit_shipping_address_${language}}[phone]

Display change tax id in checkout modal
	[Arguments] 	${tax_id}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[modal_alert]
	${text}= 	SeleniumLibrary.Get Element Attribute 	&{checkout}[modal_alert] 	innerText
	${message2}= 	CommonKeywords.Format Text 	&{change_tax_id}[message2] 	tax_id=${tax_id}
	: FOR 	${expect} 	IN 	&{change_tax_id}[message1] 	${message2}
	\  Should be True 	$expect in $text

Select on yes button in checkout modal
	${elem}= 	CommonKeywords.Format Text 	&{checkout}[lbl_label] 	label=${web_common.common_yes.upper()}
	CommonWebKeywords.Click Element 	${elem}

Accept change tax id modal
	[Arguments] 	${tax_id}
	Display change tax id in checkout modal 	${tax_id}
	Select on yes button in checkout modal

Select on delivery to home checkbox
	CommonWebKeywords.Click Element 	&{checkout}[chk_shipMethod]

Select on pick up at officemate checkbox
	CommonWebKeywords.Click Element 	&{checkout}[chk_pickupStore]

Display home delivery checkbox
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[chk_shipMethod]

Input pick up at store
	[Arguments] 	${search_wording}=10110
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_pickup_postcode]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_pickup_postcode] 	${search_wording}

Select pick up search button
	CommonWebKeywords.Click Element 	&{checkout}[btn_pickup_search]

Select pick up store branch
	[Arguments] 	${branch}=${web_common.store_location.big_c_rama4}
	${elem}= 	CommonKeywords.Format Text 	&{checkout}[btn_pickup_location] 	location=${branch}
	CommonWebKeywords.Click Element 	${elem}

Select pick up store button
	CommonWebKeywords.Click Element 	&{checkout}[btn_pickup]

Input pick up first name
	[Arguments] 	${firstname}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_pickup_firstname]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_pickup_firstname] 	${firstname}

Input pick up last name
	[Arguments] 	${lastname}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_pickup_lastname]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_pickup_lastname] 	${lastname}

Input pick up telephone
	[Arguments] 	${telephone}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_pickup_phone]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_pickup_phone] 	${telephone}

Input pick up email
	[Arguments] 	${email}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_pickup_email]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_pickup_email] 	${email}

Select pick up store
	[Arguments] 	${search_wording}=10110 	${branch}=${web_common.store_location.big_c_rama4}
	checkout.Select on pick up at officemate checkbox
	checkout.Input pick up at store 	${search_wording}
	checkout.Select pick up search button
	checkout.Select pick up store branch 	${branch}
	checkout.Select pick up store button

Input pick up details
	[Arguments] 	${firstname}=&{personal_information}[firstname]
	... 	${lastname}=&{personal_information}[lastname]
	... 	${telephone}=&{personal_information}[tel]
	... 	${email}=&{personal_information}[email]
	Input pick up first name 	${firstname}
	Input pick up last name 	${lastname}
	Input pick up telephone 	${telephone}
	Input pick up email 	${email}

Input billing company name in checkout page
	[Arguments] 	${company_name}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_guest_company]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_guest_company] 	${company_name}

Input billing address in checkout page
	[Arguments] 	${address}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_guest_address]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_guest_address] 	${address}

Input billing postcode in checkout page
	[Arguments] 	${postcode}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_guest_postcode]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_guest_postcode] 	${postcode}

Select billing district field in checkout page
	[Arguments] 	${district}
	Select dropdownlist by label 	&{checkout}[sel_billing_guest_district] 	${district}

Select billing sub district field in checkout page
	[Arguments] 	${sub_district}
	Select dropdownlist by label 	&{checkout}[sel_billing_guest_sub_district] 	${sub_district}

Select billing region field in checkout page
	[Arguments] 	${region}
	Select dropdownlist by label 	&{checkout}[sel_billing_guest_region] 	${region}

Input billing taxid in checkout page
	[Arguments] 	${taxid}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{checkout}[txt_billing_guest_taxid]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{checkout}[txt_billing_guest_taxid] 	${taxid}

Input guest billing address
	[Arguments] 	${company_name}=&{billing_address_${language}}[company_name]
	... 	${address}=&{billing_address_${language}}[address]
	... 	${postcode}=&{billing_address_${language}}[zip_code]
	... 	${region}=&{billing_address_${language}}[region]
	... 	${district}=&{billing_address_${language}}[district]
	... 	${sub_district}=&{billing_address_${language}}[sub_district]
	... 	${taxid}=&{billing_address_${language}}[guest_tax_id]
	Input billing company name in checkout page 	${company_name}
	Input billing address in checkout page 	${address}
	Input billing postcode in checkout page 	${postcode}
	Select billing region field in checkout page 	${region}
	Select billing district field in checkout page 	${district}
	Select billing sub district field in checkout page 	${sub_district}
	Input billing taxid in checkout page 	${taxid}

Display shipping address title in checkout detail page
	${locator}= 	CommonKeywords.Format Text 	&{checkout}[lbl_member_title]	title=&{web_common}[delivery_address]
	CommonWebKeywords.Verify Web Elements Are Visible	${locator}

Retry display shipping address title
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec	Display shipping address title in checkout detail page

Display specify delivery title in checkout detail page
	${locator}= 	CommonKeywords.Format Text 	&{checkout}[lbl_guest_title]	title=&{web_common}[specific_address]
	CommonWebKeywords.Verify Web Elements Are Visible	${locator}

Retry display specify address title
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec	Display specify delivery title in checkout detail page


