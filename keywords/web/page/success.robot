*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{success_page} 	lbl_label=xpath=//*[text()='{label}']

*** Keywords ***
Display success label
	[Arguments] 	@{textList}
	: FOR 	${text} 	IN 	@{textList}
	\  ${elem}= 	CommonKeywords.Format Text 	&{success_page}[lbl_label] 	label=${text}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Display success page
	Display success label 	&{success}[message1] 	&{success}[message2]

Get order id from url
	${location}= 	SeleniumLibrary.Get Location
	${order_id}= 	Evaluate 	$location.split('/')[6]
	[Return] 	${order_id}






