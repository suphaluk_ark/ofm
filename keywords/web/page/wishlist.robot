*** Setting ***

*** Variables ***
&{wishlist_page} 	btn_group_name=xpath=//label[contains(.,'{group_name}')]
... 	txt_wishlist_group=css=input[placeholder='{label}']
... 	btn_create_wishlist=xpath=//button[contains(.,'&{web_common}[create_new_group]')]
... 	btn_delete_wishlist=xpath=//label[contains(.,'{group_name}')]/following::img[contains(@src,'trash')]
... 	btn_add_to_cart=id=btn-addCart-{product}
... 	txt_product_quantity=xpath=//*[@id='btn-addCart-{product}']/preceding-sibling::div//input

*** Keywords ***
Select on wishlist group
	[Arguments] 	${wishlist_group}
	${elem}= 	CommonKeywords.Format Text 	&{wishlist_page}[btn_group_name] 	group_name=${wishlist_group}
	CommonWebKeywords.Click Element 	${elem}

Input wishlist group
	[Arguments] 	${wishlist_group}
	${elem}= 	CommonKeywords.Format Text 	&{wishlist_page}[txt_wishlist_group] 	label=&{web_common}[add_name_group]
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	CommonWebKeywords.Input Text And Verify Input For Web Element 	${elem} 	${wishlist_group}

Select on add new wishlist group
	CommonWebKeywords.Click Element 	&{wishlist_page}[btn_create_wishlist]

Select on delete wishlist group
	[Arguments] 	${wishlist_group}
	${elem}= 	CommonKeywords.Format Text 	&{wishlist_page}[btn_delete_wishlist] 	group_name=${wishlist_group}
	CommonWebKeywords.Click Element 	${elem}
	Does not display wishlist group 	${wishlist_group}

Create new wishlist group
	[Arguments] 	${wishlist_group}
	Input wishlist group 	${wishlist_group}
	Select on add new wishlist group
	Display wishlist group 	${wishlist_group}

Does not display wishlist group
	[Arguments] 	${wishlist_group}
	${elem}= 	CommonKeywords.Format Text 	&{wishlist_page}[btn_group_name] 	group_name=${wishlist_group}
	CommonWebKeywords.Verify Web Elements Are Not Visible 	${elem}

Display wishlist group
	[Arguments] 	${wishlist_group}
	${elem}= 	CommonKeywords.Format Text 	&{wishlist_page}[btn_group_name] 	group_name=${wishlist_group}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Select on add to cart in wishlist page by SKU number
	[Arguments] 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{wishlist_page}[btn_add_to_cart] 	product=${sku_number}
	CommonWebKeywords.Click Element 	${elem}

Multiple add to cart in wishlist page by SKU number
	[Arguments] 	@{sku_number}
	: FOR 	${sku} 	IN 	@{sku_number}
	\  Select on add to cart in wishlist page by SKU number 	${sku}

Adjust product quantity in wishlist page
	[Arguments] 	${sku_number} 	${quantity}=2
	${elem}= 	CommonKeywords.Format Text 	&{wishlist_page}[txt_product_quantity] 	product=${sku_number}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	CommonWebKeywords.Input Text And Verify Input For Web Element 	${elem} 	${quantity}

Summarize product quantity in wishlist page
	[Arguments] 	@{sku_number}
	${quantity_list}= 	Create List
	: FOR 	${sku} 	IN 	@{sku_number}
	\  ${elem}= 	CommonKeywords.Format Text 	&{wishlist_page}[txt_product_quantity] 	product=${sku}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	\  ${product_quantity}= 	SeleniumLibrary.Get Value 	${elem}
	\  Append to list 	${quantity_list} 	${${product_quantity}}
	${total_quantity}= 	Evaluate 	sum($quantity_list)
	[Return] 	${total_quantity}



