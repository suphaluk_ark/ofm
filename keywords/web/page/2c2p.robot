*** Variables ***
&{2c2p} 	txt_credit_card=id=credit_card_number
... 	txt_cardholder=id=credit_card_holder_name
... 	txt_cvv=credit_card_cvv
... 	sel_expiry_month=id=credit_card_expiry_month
... 	sel_expiry_year=id=credit_card_expiry_year
... 	btn_submit=id=btnCCSubmit
... 	lal_otp=id=lblTestMode
... 	btn_proceed=id=btnProceed
... 	txt_otp=name=txnOTP
... 	lbl_product_details=xpath=//label[text()='{label}']/following-sibling::span[text()[normalize-space() = 'Officemate']]
... 	lbl_product_amount=xpath=//label[text()='{label}']/following-sibling::span[text()[normalize-space() = '{amount} THB']]
... 	lbl_order_number=css=[id=payment-order] span
... 	txt_123_phone=css=input[name='MobileNumber'][value='{phone_number}']
... 	btn_123_next=id=nextButton
... 	lbl_123_amount=css=.amount
... 	btn_123_back=id=btnGoBack

*** Keywords ***
Input credit card in 2c2p page
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{2c2p}[txt_credit_card]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{2c2p}[txt_credit_card] 	${value}

Input card holder in 2c2p page
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{2c2p}[txt_cardholder]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{2c2p}[txt_cardholder] 	${value}

Input CVV in 2c2p page
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{2c2p}[txt_cvv]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{2c2p}[txt_cvv] 	${value}

Select expiry month in 2c2p page
	[Arguments] 	${value}
	CommonWebKeywords.Select dropdownlist by label 	&{2c2p}[sel_expiry_month] 	${value}

Select expiry year in 2c2p page
	[Arguments] 	${value}
	CommonWebKeywords.Select dropdownlist by label 	&{2c2p}[sel_expiry_year] 	${value}

Select on continue payment button
	CommonWebKeywords.Click Element 	&{2c2p}[btn_submit]

Submit credit card
	[Arguments] 	${card_number}=&{credit_card}[number]
	... 	${card_holder}=&{credit_card}[holder]
	... 	${card_cvv}=&{credit_card}[cvv]
	... 	${expiry_month}=&{credit_card}[month]
	... 	${expiry_year}=&{credit_card}[year]
	Input credit card in 2c2p page 	${card_number}
	Input card holder in 2c2p page 	${card_holder}
	Input CVV in 2c2p page 	${card_cvv}
	Select expiry month in 2c2p page 	${expiry_month}
	Select expiry year in 2c2p page 	${expiry_year}
	Select on continue payment button

Get OTP from 2c2p page
	CommonWebKeywords.Verify Web Elements Are Visible 	&{2c2p}[lal_otp]
	${otp}= 	SeleniumLibrary.Get Text 	&{2c2p}[lal_otp]
	${otp}= 	Get Regexp Matches 	${otp} 	=\\s([0-9]+) 	1
	[Return] 	@{otp}[0]

Input OTP in 2c2p page
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{2c2p}[txt_otp]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{2c2p}[txt_otp] 	${value}

Select on proceed button in 2c2p page
	CommonWebKeywords.Click Element 	&{2c2p}[btn_proceed]

Proceed OTP
	${status}= 	Run Keyword And Return Status 	Get OTP from 2c2p page
	Return From Keyword If 	${status}==${False}
	${otp}= 	Get OTP from 2c2p page
	Input OTP in 2c2p page 	${otp}
	Select on proceed button in 2c2p page

Display product details in 2c2p page
	${elem}= 	CommonKeywords.Format Text 	&{2c2p}[lbl_product_details] 	label=${web_common.product_details.title()}:
	CommonWebKeywords.Verify Web Elements Are Visible 	 ${elem}

Display product amount in 2c2p page
	[Arguments] 	${amount}
	${elem}= 	CommonKeywords.Format Text 	&{2c2p}[lbl_product_amount] 	label=${web_common.amount}: 	amount=${amount}
	CommonWebKeywords.Verify Web Elements Are Visible 	 ${elem}

Get Order number in 2c2p page
	CommonWebKeywords.Verify Web Elements Are Visible 	&{2c2p}[lbl_order_number]
	${order_number}= 	SeleniumLibrary.Get Text 	&{2c2p}[lbl_order_number]
	[Return] 	${order_number}

Display phone number in 123 page
	[Arguments] 	${phone_number}
	${elem}= 	CommonKeywords.Format Text 	&{2c2p}[txt_123_phone] 	phone_number=${phone_number}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Select next button in 123 page
	CommonWebKeywords.Click Element 	&{2c2p}[btn_123_next]

Display payment amount in 123 page
	[Arguments] 	${payment_amount}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{2c2p}[lbl_123_amount]
	SeleniumLibrary.Element Text Should Be 	&{2c2p}[lbl_123_amount] 	${payment_amount}

Select on back button in 123 page
	CommonWebKeywords.Click Element 	&{2c2p}[btn_123_back]

