*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{order_history} 	label=xpath=//*[text()='{label}']
... 	lbl_deepPath=xpath=//*[.='{label}']
... 	lbl_pageTitle=xpath=//h1[text()='{title}']
... 	lbl_orderList=xpath=//a[.='{mdc_increment_id}']/../../..//*[text()='{label}']
... 	img_productItem=xpath=//a[.='{mdc_increment_id}']/../../../..//a/img
... 	lbl_skuSummary=xpath=//a[.='{mdc_increment_id}']/../../../..//a/div[.='{label}']

*** Keywords ***
Display order history page
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lbl_pageTitle] 	title=&{web_common}[order_list]
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Display order list by increment id
	[Arguments] 	${mdc_increment_id} 	${productImagesAmount}=${3}	${remaining_number_sku}=${0}
	${create_at}= 	Get create_at with +7 utc format 	${mdc_increment_id}
	${create_at}= 	Convert datetime to +7 utc and D MMM YYYY hh.mm format 	${create_at}
	${status}= 	Get status from order detail response 	${mdc_increment_id}
	${total}= 	Get base_grand_total with total format 	${mdc_increment_id}

	Display order history section 	&{web_common}[order]${SPACE}${SPACE}#${mdc_increment_id}
	... 	${create_at}
	... 	&{web_common}[total]
	... 	${SPACE}${total}
	... 	${status}
	... 	&{web_common}[buy_all_agian]
	... 	&{web_common}[order_management]${SPACE}${SPACE}
	Display product images in history section 	${mdc_increment_id} 	${productImagesAmount}
	Run Keyword If 	${remaining_number_sku} > ${0} 	Display product sku summary in history section 	${mdc_increment_id} 	${remaining_number_sku}

Display order history label
	[Arguments] 	@{textList}
	: FOR 	${text} 	IN 	@{textList}
	\ 	${elem}= 	CommonKeywords.Format Text 	&{order_history}[label] 	label=${text}
	\ 	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Display order history section
	[Arguments] 	${mdc_increment_id} 	@{textList}
	FOR 	${text} 	IN 	@{textList}
		${elem}= 	CommonKeywords.Format Text 	&{order_history}[lbl_orderList] 	mdc_increment_id=${mdc_increment_id} 	label=${text}
		CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	END

Display product images in history section
	[Arguments] 	${mdc_increment_id} 	${productImagesAmount}=3
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[img_productItem] 	mdc_increment_id=&{web_common}[order]${SPACE}${SPACE}#${mdc_increment_id}
	${elems}= 	SeleniumLibrary.Get WebElements 	${elem}
	${actual_len}= 	Get Length 	${elems}
	Should be True 	${productImagesAmount}==${actual_len}

Display product sku summary in history section
	[Arguments] 	${mdc_increment_id} 	${skuSummaryAmount}=1
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lbl_skuSummary] 	mdc_increment_id=&{web_common}[order]${SPACE}${SPACE}#${mdc_increment_id} 	label=${skuSummaryAmount}+ &{web_common}[more]
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Select on order id button in order history page
	[Arguments] 	${mdc_increment_id}
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lbl_deepPath] 	label=&{web_common}[order]${SPACE}${SPACE}#${mdc_increment_id}
	CommonWebKeywords.Click Element 	${elem}

Select on product picture button in order history page
	[Arguments] 	${mdc_increment_id}
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[img_productItem] 	mdc_increment_id=&{web_common}[order]${SPACE}${SPACE}#${mdc_increment_id}
	CommonWebKeywords.Click Element 	${elem}

Select on sku summary button in order history page
	[Arguments] 	${mdc_increment_id} 	${skuSummaryAmount}=1
	${elem}= 	CommonKeywords.Format Text 	&{order_history}[lbl_skuSummary] 	mdc_increment_id=&{web_common}[order]${SPACE}${SPACE}#${mdc_increment_id} 	label=${skuSummaryAmount}+ &{web_common}[more]
	CommonWebKeywords.Click Element 	${elem}

Randomly select button to order details page
	[Arguments] 	${mdc_increment_id} 	${skuSummaryAmount}=1
	${state}= 	Evaluate 	random.randint(1,3) 	random
	Run Keyword If 	${state}==1 	Select on order id button in order history page 	${mdc_increment_id}
	...	ELSE IF 	${state}==2 	Select on product picture button in order history page 	${mdc_increment_id}
	...	ELSE 	Select on sku summary button in order history page 	${mdc_increment_id} 	${skuSummaryAmount}

