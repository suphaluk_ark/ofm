*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{order_summary} 	lal_eCoupon=css=input[placeholder*='{label}']
... 	label=xpath=//*[.='{label}']
... 	price_by_lbl=xpath=//div/span[.='{label}']/../div
... 	lbl_special_fee=xpath=(//div[.='{label}']/../div/div)[2]
... 	lbl_delivery_fee=xpath=//div[.='{label}']/following-sibling::div

*** Keywords ***
Display Order Summary section
	: FOR 	${text} 	IN 	&{web_common}[order_summary]
	... 	&{web_common}[total_price_vat_excluded]
	... 	&{web_common}[vat]
	... 	&{web_common}[delivery_fee]
	... 	&{web_common}[special_delivery_fee]
	... 	&{web_common}[total_delivery_fee]
	... 	&{web_common}[grand_total]
	... 	&{web_common}[or_pay_with_the_1_card]
	\  ${elem}= 	CommonKeywords.Format Text 	&{order_summary}[label] 	label=${text}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	Caculate order summary 	${sum_per_product_price}

Input e-Coupon or Promotion code
	[Arguments] 	${code}
	${elem}= 	CommonKeywords.Format Text 	&{order_summary}[lal_eCoupon] 	label=&{web_common}[e_coupon]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	${elem} 	${code}

Select on Apply button
	${elem}= 	CommonKeywords.Format Text 	&{order_summary}[label] 	label=&{web_common}[apply]
	CommonWebKeywords.Click Element 	${elem}

Apply e-Coupon or Promotion
	[Arguments] 	${code}
	order_summary.Input e-Coupon or Promotion code 	${code}
	order_summary.Select on Apply button

Select on Proceed to checkout button
	${elem}= 	CommonKeywords.Format Text 	&{order_summary}[label] 	label=&{web_common}[proceed_to_checkout]
	CommonWebKeywords.Click Element 	${elem}

Caculate order summary
	[Arguments] 	${sum_per_product_price}
	${sum_price}= 	Evaluate 	sum($sum_per_product_price)
	calculation.Calculate total vat from original total 	${sum_price}
	${sum_price_without_vat}= 	Evaluate 	${sum_price} - ${vat_amount}
	# TBC

Get order summary details by sku and quantity dictionary
	[Documentation] 	Arguments sku_and_quantity must be the dictionary e.g.sku_number=quantity
	[Arguments] 	&{sku_and_quantity}
	Get total price from product details by dictionary 	&{sku_and_quantity}

Get order summary details by sku and quantity list
	[Documentation] 	Arguments sku_number and quantity must be the list
	[Arguments]	${sku_numbers} 	${product_quantities}
	Get total price from product details by list 	${sku_numbers} 	${product_quantities}


Convert SKU and quantity list to dictionary
	[Arguments] 	${sku_numbers} 	${product_quantities}
	&{sku_quantity}= 	Evaluate 	dict(zip($sku_numbers, $product_quantities))
	[Return] 	&{sku_quantity}

Get total price from product details by dictionary
	[Documentation] 	Arguments sku_number=quantity must be the dictionary
	[Arguments] 	&{sku_and_quantity}
	${summary_total_price} 	Create List
	${summary_total_vat}=	Create List
	${summary_total_fee}= 	Create List
	${summary_delivery_fee}= 	Create List
	${summary_special_fee}= 	Create List
	: FOR 	${key} 	IN 	@{sku_and_quantity.keys()}
	\  ${product_price}= 	Set Variable 	&{${key}}[price]
	\  ${surcharge_fee}= 	Set Variable 	&{${key}}[special_delivery_fee]
	\  Subtract total price from vat 	${product_price}
	\  ${total_price}= 	Evaluate 	${product_price} * &{sku_and_quantity}[${key}]
	\  ${total_fee}= 	Evaluate 	${surcharge_fee} * &{sku_and_quantity}[${key}]
	\  Append to List 	${summary_total_price} 	${total_price}
	\  Append to List 	${summary_total_fee} 	${total_fee}
	\  Append to List 	${summary_special_fee} 	&{${key}}[special_delivery_fee]
	${summary_total_price} 	Evaluate 	sum($summary_total_price)
	${total_vat}= 	Evaluate 	sum($summary_total_vat)
	${total_fee}= 	Evaluate 	sum($summary_total_fee)
	${total_special_fee}= 	Evaluate 	sum($summary_special_fee)
	calculation.Calculate vat from price exclude vat 	${summary_total_price}
	${summary_total_price}= 	Convert price to total amount format 	${summary_total_price}
	${summary_total_fee}= 	Convert price to total amount format 	${total_fee}
	${summary_special_fee}= 	Convert price to total amount format 	${total_special_fee}
	${price_with_vat}= 	Evaluate 	${summary_total_price} + ${vat_amount}
	${total_delivery_fee}= 	Set Variable If 	${price_with_vat} >= 499.00 	0.0
	... 	50.00
	Set Test Variable 	${summary_total_vat} 	${vat_amount_with_format}
	Set Test Variable 	${summary_total_price} 	${summary_total_price}
	Set Test Variable 	${summary_total_fee} 	${summary_total_fee}
	Set Test Variable 	${summary_special_fee} 	${summary_special_fee}
	Set Test Variable 	${summary_delivery_fee} 	${total_delivery_fee}


Get total price from product details by list
	[Documentation] 	Arguments sku_number and quantity must be the list
	[Arguments] 	${sku_numbers} 	${product_quantities}
	&{sku_quantity}= 	Convert SKU and quantity list to dictionary 	${sku_numbers} 	${product_quantities}
	Get total price from product details by dictionary 	&{sku_quantity}

Set special delivery fee from product details
	[Arguments] 	${special_fee}
	${special_fee}= 	Set Variable If 	${special_fee}==0.0 	&{web_common}[free]
	...	฿${special_fee}
	[Return] 	${special_fee}

Verify total price vat excluded in order summary
	${locator}= 	CommonKeywords.Format Text 	&{order_summary}[price_by_lbl] 	label=&{web_common}[total_price_vat_excluded]
	CommonWebKeywords.Verify Web Elements Are Visible	${locator}
	SeleniumLibrary.Element Text Should Be 	${locator} 	฿${summary_total_price}

Verify vat in order summary in order summary
	${locator}= 	CommonKeywords.Format Text 	&{order_summary}[price_by_lbl] 	label=&{web_common}[vat]
	CommonWebKeywords.Verify Web Elements Are Visible	${locator}
	SeleniumLibrary.Element Text Should Be 	${locator} 	฿${summary_total_vat}

Verify delivery fee in order summary
	[Arguments] 	${summary_delivery_fee}
	${locator}= 	CommonKeywords.Format Text 	&{order_summary}[lbl_delivery_fee] 	label=&{web_common}[delivery_fee]
	CommonWebKeywords.Verify Web Elements Are Visible 	${locator}
	${summary_delivery_fee}= 	Set variable if 	${summary_delivery_fee}==0.0 	&{web_common}[free]
	... 	฿${summary_delivery_fee}
	SeleniumLibrary.Element Text Should Be 	${locator} 	${summary_delivery_fee}

Verify special delivery fee in order summary
	[Arguments] 	${summary_special_fee} 	${display_special_delivery}=${False}
	${locator}= 	CommonKeywords.Format Text 	&{order_summary}[lbl_special_fee] 	label=&{web_common}[special_delivery_fee]
	Run Keyword If 	${summary_special_fee} != 0.0 and ${display_special_delivery}==${True}	 CommonWebKeywords.Verify Web Elements Are Visible	${locator}
	...	ELSE 	Run Keywords 	CommonWebKeywords.Verify Web Elements Are Not Visible 	${locator}
	...	AND 	Return From Keyword 	0.0
	${special_fee}= 	Set special delivery fee from product details 	${summary_special_fee}
	SeleniumLibrary.Element Text Should Be 	${locator} 	${special_fee}
	[Return] 	${summary_special_fee}

Verify total save in order summary
	[Arguments] 	${total_save}=${0}
	${locator}= 	CommonKeywords.Format Text 	&{order_summary}[price_by_lbl] 	label=&{web_common}[total_save]
	CommonWebKeywords.Verify Web Elements Are Visible	${locator}
	${total_save}= 	Convert price to total amount format 	${total_save}
	Run Keyword If 	${total_save} > 0 	SeleniumLibrary.Element Text Should Be 	${locator}	-฿${total_save}
	... 	ELSE 	SeleniumLibrary.Element Text Should Be 	${locator}	฿${total_save}

Verify grand total in order summary
	[Arguments] 	${price_include_vat} 	${special_fee_delivery} 	${summary_delivery_fee} 	${e_coupon}=0
	${locator}= 	CommonKeywords.Format Text 	&{order_summary}[price_by_lbl] 	label=&{web_common}[grand_total]
	calculation.Summarize grand total 	${price_include_vat} 	${special_fee_delivery} 	${summary_delivery_fee} 	${e_coupon}
	CommonWebKeywords.Verify Web Elements Are Visible	${locator}
	SeleniumLibrary.Element Text Should Be 	${locator} 	฿${grand_total_with_format}
	Set Test Variable 	${summary_grand_total} 	${grand_total_with_format}

Display order summary
	[Arguments] 	${display_special_delivery}=${False}
	Verify total price vat excluded in order summary
	Verify vat in order summary in order summary
	Verify delivery fee in order summary 	${summary_delivery_fee}
	# Verify e coupon discount in order summary
	${summary_special_fee}=	Verify special delivery fee in order summary 	${summary_special_fee} 	${display_special_delivery}
	${price_include_vat}= 	Evaluate 	${summary_total_vat} + ${summary_total_price}
	Verify grand total in order summary 	${price_include_vat} 	${summary_special_fee} 	${summary_delivery_fee} 	${0.00}





