*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{payment} 	btn_credit=css=img[src*='payment/ic-credit']
... 	btn_atm=css=img[src*='atm']
... 	btn_onlineBanking=css=img[src*='onlinebanking']
... 	lbl_label=xpath=//*[text()='{label}']
... 	btn_addIcon=css=img[src*='add-circle-outline']
... 	txt_email=name=email
... 	txt_password=name=password
... 	btn_choose_cod=css=img[src*='ic_cod']
... 	btn_cod=id=btn-select-cod-cash
... 	btn_ccod=id=btn-select-cod-creditCard
... 	rdo_counter_service=xpath=//*[@value='counter-service']/following-sibling::div
... 	btn_internet_mobile=css=[value={bank_code}][name = banking-service-group] + div
... 	txt_telephone=xpath=//p[contains(text(),'{payment_type}')]/following::input[@name='counter_tel']
... 	btn_counter_service=css=[name=counter-service-group][value='{counter_type}'] + div
... 	btn_t1_credit_card=css=img[src*='payment/centralcard.png']
... 	btn_payment=css=div[id*=submit]
...		lbl_title=xpath=//label[contains(text(),'{title}')]

*** Keywords ***
Display choose payment method section
	Display Payment label 	&{web_common}[choose_payment_method]
	... 	&{web_common}[credit_card] ${/} &{web_common}[debit_card]
	... 	&{web_common}[counter_service] ${/} &{web_common}[atm]
	... 	&{web_common}[online_banking]
	: FOR 	${elem} 	IN 	&{payment}[btn_credit] 	&{payment}[btn_atm] 	&{payment}[btn_onlineBanking] 	&{payment}[btn_cod]
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Select on Create / Debit button
	CommonWebKeywords.Click Element 	&{payment}[btn_credit]

Select on Counter Service / ATM button
	CommonWebKeywords.Click Element 	&{payment}[btn_atm]

Select on Online Banking button
	CommonWebKeywords.Click Element 	&{payment}[btn_onlineBanking]

Select on COD button
	CommonWebKeywords.Click Element 	&{payment}[btn_choose_cod]

Select on CCOD button
	CommonWebKeywords.Click Element 	&{payment}[btn_ccod]

Select on T1 Credit card button
	CommonWebKeywords.Click Element 	&{payment}[btn_t1_credit_card]

Display Payment label
	[Arguments] 	@{textList}
	: FOR 	${text} 	IN 	@{textList}
	\  ${elem}= 	CommonKeywords.Format Text 	&{payment}[lbl_label] 	label=${text}
	\  CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Expand the 1 card section
	CommonWebKeywords.Click Element 	&{payment}[btn_addIcon]

Input the 1 card email
	[Arguments] 	${email}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{payment}[txt_email]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{payment}[txt_email] 	${email}

Input the 1 card password
	[Arguments] 	${password}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{payment}[txt_password]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{payment}[txt_password] 	${password}

Select on the 1 card login button
	${text}= 	Remove String	&{web_common}[login] 	${SPACE}
	${elem}= 	CommonKeywords.Format Text 	&{payment}[lbl_label] 	label=${text}
	CommonWebKeywords.Click Element 	${elem}

Login to the 1 card reward points
	[Arguments] 	${email} 	${password}
	Expand the 1 card section
	Input the 1 card email 	${email}
	Input the 1 card password 	${password}
	Select on the 1 card login button

Select on payment button
	CommonWebKeywords.Click Element 	&{payment}[btn_payment]

Pay order with COD/ CCOD
	payment.Select on COD / CCOD button
	payment.Select on payment button

Pay order with billing payment at counter service
	payment.Select on Counter Service / ATM button
	CommonWebKeywords.Click Element 	&{payment}[rdo_counter_service]
	payment.Select on payment button

Pay order with credit card
	payment.Select on Create / Debit button
	payment.Select on payment button

Pay order with T1 credit card
	payment.Select on T1 Credit card button
	payment.Select on payment button

Select internet mobile button
	[Arguments] 	${back_code}=BBL
	${elem}= 	CommonKeywords.Format Text 	&{payment}[btn_internet_mobile] 	bank_code=${back_code}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	CommonWebKeywords.Click Element 	${elem}

Pay order with online banking
	[Arguments] 	${back_code}=BBL 	${tel}=0255555555 	${payment_type}=&{web_common}[online_banking]
	payment.Select on Online Banking button
	payment.Select internet mobile button
	payment.Input bill payment tel. 	${tel} 	${payment_type}
	payment.Select on payment button
	[Return] 	${tel}

Input bill payment tel.
	[Arguments] 	${tel}=0255555555 	${payment_type}=&{web_common}[online_banking]
	${elem}= 	CommonKeywords.Format Text 	&{payment}[txt_telephone] 	payment_type=${payment_type}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	CommonWebKeywords.Input Text And Verify Input For Web Element 	${elem} 	${tel}

Select counter service button
	[Arguments] 	${counter_type}=counter-service
	${elem}= 	CommonKeywords.Format Text 	&{payment}[btn_counter_service] 	counter_type=${counter_type}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	CommonWebKeywords.Click Element 	${elem}

Pay order with counter services
	[Arguments] 	${counter_type}=counter-service 	${tel}=0255555555 	${payment_type}=&{web_common}[counter_service_msg]
	payment.Select on Counter Service / ATM button
	payment.Select counter service button 	${counter_type}
	Input bill payment tel. 	${tel} 	${payment_type}
	payment.Select on payment button

Pay order with COD
	payment.Select on COD button
	payment.Select on payment button

Pay order with CCOD
	payment.Select on COD button
	payment.Select on CCOD button
	payment.Select on payment button

Display selecting payment title in payment details page
	${locator}= 	CommonKeywords.Format Text 	&{payment}[lbl_title]	title=&{web_common}[selecting_payment]
	CommonWebKeywords.Verify Web Elements Are Visible	${locator}

Retry display selecting payment title
	Wait Until Keyword Succeeds 	${global_retry_time} x 	${global_retry_in_sec} sec	Display selecting payment title in payment details page







