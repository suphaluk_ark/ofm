*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{web_thank_you_page} 	lbl_thankyou_detail=xpath=//div[contains(text(),'{label}')]/following-sibling::div[text()='{value}']
... 	icon_checked_green=css=img[src*=checked-green]
... 	lbl_thankyou=xpath=//*[text()='{label}']

*** Keywords ***
Display payment type in thank you page
	[Arguments] 	${payment_type}
	${locator}= 	CommonKeywords.Format Text 	&{web_thank_you_page}[lbl_thankyou_detail] 	label=&{web_thank_you}[payment_type] 	value=${payment_type}
	CommonWebKeywords.Verify Web Elements Are Visible 	${locator}

Display order status in thank you page
	[Arguments] 	${order_status}
	${locator}= 	CommonKeywords.Format Text 	&{web_thank_you_page}[lbl_thankyou_detail] 	label=&{web_thank_you}[order_status] 	value=${order_status}
	CommonWebKeywords.Verify Web Elements Are Visible 	${locator}

Displace thank you label in thank you page
	[Arguments] 	${order_id}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{web_thank_you_page}[icon_checked_green]
	FOR 	${text} 	IN 	&{web_thank_you}[thankyou]
	... 	&{web_thank_you}[thankyou_msg]
	... 	&{web_thank_you}[purchase_code]
	... 	${order_id}
		${locator}= 	CommonKeywords.Format Text 	&{web_thank_you_page}[lbl_thankyou] 	label=${text}
		CommonWebKeywords.Verify Web Elements Are Visible 	${locator}
	END

Display thank you title
	CommonWebKeywords.Verify Web Elements Are Visible 	&{web_thank_you_page}[icon_checked_green]
	${locator}= 	CommonKeywords.Format Text 	&{web_thank_you_page}[lbl_thankyou] 	label=&{web_thank_you}[thankyou]
	CommonWebKeywords.Verify Web Elements Are Visible	${locator}

