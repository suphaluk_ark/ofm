*** Setting ***
Resource 	${CURDIR}/../../web_imports.robot

*** Variables ***
&{billing_address} 	label=xpath=//*[text()='{title}']/../following-sibling::*//*[text()='{label}']
... 	lbl_table=xpath=//td[.='{column}']/following::*[.='{value}']
...    btn_edit=xpath=//*[contains(text(),'{address}')]/following-sibling::*//*[contains(text(),'{btn_edit}')]
...    txt_firstName=name=firstName
...    txt_lastName=name=lastName
...    txt_phone=name=phone
...    txt_email=name=email
...    txt_address=name=address
...    txt_zipCode=name=zipCode
...    sel_subDistrict=name=subDistrict
...    sel_district=name=district
...    sel_region=name=region
...    txt_vatId=name=vat_id
...    btn_add_address=id=btn-billingAddressForm-submitButton
...    tr_billinglist=xpath=//*[contains(text(),'{label}')]/parent::tr
...    chk_default=css=#lab-billingAddressForm-useAsDefault div img[src*='assets']

*** Keywords ***
Select on edit button by address
	[Arguments] 	${address}
	${elem}=     CommonKeywords.Format Text    &{billing_address}[btn_edit]    address=${address}  btn_edit=&{web_common}[edit]
	CommonWebKeywords.Click Element    ${elem}

Input billing firstname
    [Arguments]     ${value}
    CommonWebKeywords.Verify Web Elements Are Visible   &{billing_address}[txt_firstName]
    Input Text And Verify Input For Web Element    &{billing_address}[txt_firstName]    ${value}

Input billing lastname
    [Arguments]     ${value}
    CommonWebKeywords.Verify Web Elements Are Visible   &{billing_address}[txt_lastName]
    Input Text And Verify Input For Web Element     &{billing_address}[txt_lastName]    ${value}

Input billing phone
    [Arguments]     ${value}
    CommonWebKeywords.Verify Web Elements Are Visible   &{billing_address}[txt_phone]
    Input Text And Verify Input For Web Element    &{billing_address}[txt_phone]    ${value}

Input billing address
    [Arguments]     ${value}
    CommonWebKeywords.Verify Web Elements Are Visible   &{billing_address}[txt_address]
    Input Text And Verify Input For Web Element    &{billing_address}[txt_address]    ${value}

Input billing zip code
    [Arguments]     ${value}
    CommonWebKeywords.Verify Web Elements Are Visible   &{billing_address}[txt_zipCode]
    Input Text And Verify Input For Web Element    &{billing_address}[txt_zipCode]    ${value}

Select billing region by label
    [Arguments]     ${value}
    Select dropdownlist by label    &{billing_address}[sel_region]    ${value}

Select billing district by label
    [Arguments]     ${value}
    Select dropdownlist by label    &{billing_address}[sel_district]    ${value}

Select billing sub district by label
    [Arguments]     ${value}
    Select dropdownlist by label    &{billing_address}[sel_subDistrict]    ${value}

Input billing tax identification number
    [Arguments]    ${value}
    CommonWebKeywords.Verify Web Elements Are Visible   &{billing_address}[txt_vatId]
    Input Text And Verify Input For Web Element     &{billing_address}[txt_vatId]    ${value}

Select on add new address button
    CommonWebKeywords.Click Element     &{billing_address}[btn_add_address]

Input billing address section
    [Arguments]    ${firstname}=&{billing_address${language}}[firstname]
    ...    ${lastname}=&{billing_address${language}}[lastname]
    ...    ${phone}=&{billing_address${language}}[phone]
    ...    ${address}=&{billing_address${language}}[address]
    ...    ${zip_code}=&{billing_address${language}}[zip_code]
    ...    ${region}=&{billing_address${language}}[region]
    ...    ${district}=&{billing_address${language}}[district]
    ...    ${sub_district}=&{billing_address${language}}[sub_district]
    ...    ${tax_id}=&{billing_address${language}}[tax_id]
    billing_address.Input billing firstname    ${firstname}
    billing_address.Input billing lastname    ${lastname}
    billing_address.Input billing phone     ${phone}
    billing_address.Input billing address    ${address}
    billing_address.Input billing zip code   ${zip_code}
    billing_address.Select billing region by label    ${region}
    billing_address.Select billing district by label    ${district}
    billing_address.Select billing sub district by label    ${sub_district}
    billing_address.Input billing tax identification number    ${tax_id}

Display billing address list
    [Arguments]     ${firstname}
    ...     ${lastname}
    ...     ${address_name}
    ...     ${district}
    ...     ${sub_district}
    ...     ${tax_id}
    ...     ${postcode}
    ...     ${phone}
    ${elem}=    CommonKeywords.Format Text  &{billing_address}[tr_billinglist]    label=${address_name}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}
    ${text}=    SeleniumLibrary.Get Element Attribute   ${elem}     innerText
    Should Match Regexp     ${text}     ${firstname} ${lastname}\t${address_name}\n${sub_district}, ${district}\n${SPACE}${postcode}\t${tax_id}\t${phone}\t\n\t&{web_common}[edit]

Display enabled use as default
    CommonWebKeywords.Verify Web Elements Are Visible    &{billing_address}[chk_default]
