*** Setting ***
Resource 	${CURDIR}/../../web_imports.robot

*** Variables ***
&{my_account_menu}    label=xpath=//*[text()='{title}']/../following-sibling::*//*[text()='{label}']
...    lbl_table=xpath=//td[.='{column}']/following::*[.='{value}']
...    btn_delivery=xpath=//*[text()='{label}']/following::a[contains(@href,'delivery')]
...    btn_billing=xpath=//*[text()='{label}']/following::a[contains(@href,'billing')]

*** Keywords ***
Select on delivery information menu
	${elem}= 	CommonKeywords.Format Text 	&{my_account_menu}[btn_delivery] 	label=&{web_common}[manage_account]
	CommonWebKeywords.Mouse over 	${elem}
    CommonWebKeywords.Click Element    ${elem}

Select on billing information menu
	${elem}= 	CommonKeywords.Format Text 	&{my_account_menu}[btn_billing] 	label=&{web_common}[manage_account]
	CommonWebKeywords.Mouse over 	${elem}
    CommonWebKeywords.Click Element    ${elem}
