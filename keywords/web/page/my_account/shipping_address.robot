*** Setting ***
Resource 	${CURDIR}/../../web_imports.robot

*** Variables ***
&{shipping_address} 	label=xpath=//*[text()='{title}']/../following-sibling::*//*[text()='{label}']
... 	lbl_table=xpath=//td[.='{column}']/following::*[.='{value}']
... 	btn_edit=xpath=//*[contains(text(),'{address}')]/following-sibling::*//*[contains(text(),'{btn_edit}')]
... 	txt_firstName=name=firstName
... 	txt_lastName=name=lastName
... 	txt_phone=name=phone
... 	txt_email=name=email
... 	txt_address=name=address
... 	txt_zipCode=name=zipCode
... 	sel_subDistrict=name=subDistrict
... 	sel_district=name=district
... 	sel_region=name=region
... 	btn_add_address=id=btn-deliveryAddressForm-submitButton
... 	tr_shippinglist=xpath=//*[contains(text(),'{label}')]/parent::tr
... 	chk_default=css=#lab-deliveryAddressForm-useAsDefault div img[src*='assets']

*** Keywords ***
Display shipping address page
	#TBC
Select on edit button by address
	[Arguments] 	${address}
	${elem}=     CommonKeywords.Format Text    &{shipping_address}[btn_edit]    address=${address} 	btn_edit=&{web_common}[edit]
	CommonWebKeywords.Click Element    ${elem}

Input shipping firstname
	[Arguments]     ${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{shipping_address}[txt_firstName]
	Input Text And Verify Input For Web Element    &{shipping_address}[txt_firstName]    ${value}

Input shipping lastname
	[Arguments]     ${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{shipping_address}[txt_lastName]
	Input Text And Verify Input For Web Element     &{shipping_address}[txt_lastName]    ${value}

Input shipping phone
	[Arguments]     ${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{shipping_address}[txt_phone]
	Input Text And Verify Input For Web Element    &{shipping_address}[txt_phone]    ${value}

Input shipping address
	[Arguments]     ${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{shipping_address}[txt_address]
	Input Text And Verify Input For Web Element    &{shipping_address}[txt_address]    ${value}

Input shipping zip code
	[Arguments]     ${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{shipping_address}[txt_zipCode]
	Input Text And Verify Input For Web Element    &{shipping_address}[txt_zipCode]    ${value}

Select shipping region by label
	[Arguments]     ${value}
	Select dropdownlist by label    &{shipping_address}[sel_region]    ${value}

Select shipping district by label
	[Arguments]     ${value}
	Select dropdownlist by label    &{shipping_address}[sel_district]    ${value}

Select shipping sub district by label
	[Arguments]     ${value}
	Select dropdownlist by label    &{shipping_address}[sel_subDistrict]    ${value}

Input shipping address section
	 [Arguments]    ${firstname}=&{shipping_address${language}}[firstname]
	...    ${lastname}=&{shipping_address${language}}[lastname]
	...    ${phone}=&{shipping_address${language}}[phone]
	...    ${address}=&{shipping_address${language}}[address]
	...    ${zip_code}=&{shipping_address${language}}[zip_code]
	...    ${region}=&{shipping_address${language}}[region]
	...    ${district}=&{shipping_address${language}}[district]
	...    ${sub_district}=&{shipping_address${language}}[sub_district]
	shipping_address.Input shipping firstname    ${firstname}
	shipping_address.Input shipping lastname    ${lastname}
	shipping_address.Input shipping phone    ${phone}
	shipping_address.Input shipping address    ${address}
	shipping_address.Input shipping zip code    ${zip_code}
	shipping_address.Select shipping region by label     ${region}
	shipping_address.Select shipping district by label   ${district}
	shipping_address.Select shipping sub district by label    ${sub_district}

Select on add new address button
	CommonWebKeywords.Click Element     &{shipping_address}[btn_add_address]

Display shipping address list
	[Arguments] 	${firstname}
	... 	${lastname}
	... 	${address_name}
	... 	${district}
	... 	${sub_district}
	... 	${postcode}
	... 	${phone}
	${elem}= 	CommonKeywords.Format Text 	&{shipping_address}[tr_shippinglist] 	label=${address_name}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	${text}= 	SeleniumLibrary.Get Element Attribute 	${elem} 	innerText
	Should Match Regexp 	${text} 	${firstname} ${lastname}\t${address_name}\n${sub_district}, ${district}\n${SPACE}${postcode}\t\t${phone}\t\n\t&{web_common}[edit]

Display enabled use as default
	CommonWebKeywords.Verify Web Elements Are Visible 	&{shipping_address}[chk_default]

























