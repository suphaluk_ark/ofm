*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{order_details} 	label=xpath=//*[text()='{title}']/..//*[.='{label}']
... 	lbl_table=xpath=//td[.='{column}']/following::*[.='{value}']

*** Keywords ***
Display order details label
	[Arguments] 	@{textList}
	: FOR 	${text} 	IN 	@{textList}
	\ 	${elem}= 	CommonKeywords.Format Text 	&{order_details}[label] 	title=&{web_common}[your_order] 	label=${text}
	\ 	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Display order details table column and value
	[Arguments] 	${column} 	${value}
	${elem}= 	CommonKeywords.Format Text 	&{order_details}[lbl_table] 	column=${column} 	value=${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Display order details section
	[Arguments] 	${mdc_increment_id}
	${create_at}= 	Get create_at with +7 utc format 	${mdc_increment_id}
	${create_at}= 	Convert datetime to +7 utc and D MMM YYYY hh.mm format 	${create_at}
	${status}= 	Get status from order detail response 	${mdc_increment_id}
	${payment_method}= 	Get payment method from order detail response 	${mdc_increment_id}
	#TBC

	Display order details label 	&{web_common}[order]${SPACE}#${mdc_increment_id}
	... 	&{web_common}[order_date]${SPACE}${create_at}
	... 	&{web_common}[buy_all_agian]
	Display order details table column and value 	&{web_common}[status]: 	${status}
	Display order details table column and value 	&{web_common}[total]
	Display order details table column and value 	&{web_common}[payment_method] 	${payment_method}

Display product details in order details page
	#TBC