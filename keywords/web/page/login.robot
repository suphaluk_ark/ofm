*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{loginPage} 	label=//*[text()='{label}']
... 	loginEmail=name=email
... 	loginPassword=name=password
... 	loginBTN=name=submitButton
... 	selectingModal=xpath=//*[.='{label}']/parent::*
... 	btn_facebook=css=a[href*='facebook']
... 	txt_facebook_email=id=email
... 	txt_facebook_password=id=pass
... 	btn_facebook_signin=name=login

*** Keywords ***
Select Log in button
	${elem}= 	CommonKeywords.Format Text 	&{loginPage}[loginBTN] 	label=&{web_common}[login]
	CommonWebKeywords.Click Element 	${elem}



Input Email
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{loginPage}[loginEmail]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{loginPage}[loginEmail] 	${value}

Input Password
	[Arguments] 	${value}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{loginPage}[loginPassword]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{loginPage}[loginPassword] 	${value}

Display label in Login section
	Display label 	${welcome_message} 	&{web_common}[email] 	&{web_common}[password] 	${remember} 	&{web_common}[forgot_password]

Display selecting account modal
	Display label 	&{select_account}[title] 	&{select_account}[message] 	&{web_common}[personal_account] 	&{web_common}[corporate_account]

Display label
	[Arguments] 	@{label}
	: FOR 	${text} 	IN 	@{label}
	\ 	${elem}= 	CommonKeywords.Format Text 	&{loginPage}[label] 	label=${text}
	\ 	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Select Corporate botton
	${elem}= 	CommonKeywords.Format Text 	&{loginPage}[label] 	label=&{web_common}[corporate_account]
	CommonWebKeywords.Click Element 	${elem}

Select on facebook button
	CommonWebKeywords.Click Element 	&{loginPage}[btn_facebook]

Input facebook email
	[Arguments] 	${email}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{loginPage}[txt_facebook_email]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{loginPage}[txt_facebook_email] 	${email}

Input facebook password
	[Arguments] 	${password}
	CommonWebKeywords.Verify Web Elements Are Visible 	&{loginPage}[txt_facebook_password]
	CommonWebKeywords.Input Text And Verify Input For Web Element 	&{loginPage}[txt_facebook_password] 	${password}

Select on facebook login button
	CommonWebKeywords.Click Element 	&{loginPage}[btn_facebook_signin]



















