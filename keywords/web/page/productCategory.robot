*** Setting ***
Resource 	${CURDIR}/../web_imports.robot

*** Variables ***
&{productCategory} 	addProductBTN=css=#btn-addCart-{product}
... 	reviewStarIcon=xpath=//div[contains(.,'{product}')]/following::img[contains(@src,'star')]
... 	lbl_loading=xpath=//div[@id='btn-addCart-{product}']/span[text()='{label}']
... 	lbl_label=xpath=//*[.='{label}']
... 	btn_wishlist=xpath=//div[contains(.,'{product}')]/following::img[contains(@src,'wishlist')][2]
... 	btn_wishlist_group=xpath=//div[contains(.,'{product}')]//div[contains(text(),'{wishlisy_group}')]
... 	btn_wishlist_enabled=xpath=//div[contains(.,'{product}')]/following::img[contains(@src,'ios-heart-outline')]
... 	btn_product_image=css=[data-productid='{sku_number}']
... 	txt_quantity=xpath=//*[@id='btn-addCart-{product}']/preceding-sibling::div//input

*** Keywords ***
Select Add to cart button by SKU number
	[Arguments] 	${sku_number}
	# ${elem}= 	CommonKeywords.Format Text 	&{productCategory}[reviewStarIcon] 	product=${product}
	# CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	Mouse over on SKU number 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{productCategory}[addProductBTN] 	product=${sku_number}
	CommonWebKeywords.Click Element 	${elem}

	${elem}= 	CommonKeywords.Format Text 	&{productCategory}[lbl_loading] 	product=${sku_number} 	label=&{web_common}[loading]...
	CommonWebKeywords.Verify Web Elements Are Not Visible 	${elem}

Mouse over on SKU number
	[Arguments] 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{productCategory}[lbl_label] 	label=&{web_common}[code]:${SPACE}${sku_number}
	CommonWebKeywords.Mouse over 	${elem}

Select add wishlist item in category page by SKU number
	[Arguments] 	${sku_number} 	${wishlist_group}=${wishlist_group_name}
	Mouse over on SKU number 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{productCategory}[btn_wishlist] 	product=${sku_number}
	CommonWebKeywords.Click Element 	${elem}
	Select wishlist group modal in category page 	${sku_number}
	Display enabled wishlist icon in category page 	${sku_number}

Select wishlist group modal in category page
	[Arguments] 	${sku_number} 	${wishlist_group}=${wishlist_group_name}
	${elem}= 	CommonKeywords.Format Text 	&{productCategory}[btn_wishlist_group] 	product=${sku_number} 	wishlisy_group=${wishlist_group}
	CommonWebKeywords.Click Element 	${elem}

Display enabled wishlist icon in category page
	[Arguments] 	${sku_number} 	${wishlist_group}=${wishlist_group_name}
	Mouse over on SKU number 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{productCategory}[btn_wishlist_enabled] 	product=${sku_number}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}

Select on product item by SKU number
	[Arguments] 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{productCategory}[btn_product_image] 	label=${web_common.product_results.lower()} 	sku_number=${sku_number}
	CommonWebKeywords.Click Element 	${elem}

Input SKU quantity in category page
	[Arguments] 	${sku_number} 	${quantity}
	Mouse over on SKU number 	${sku_number}
	${elem}= 	CommonKeywords.Format Text 	&{productCategory}[txt_quantity] 	product=${sku_number}
	CommonWebKeywords.Verify Web Elements Are Visible 	${elem}
	SeleniumLibrary.Press Key 	${elem} 	\ue003
	CommonWebKeywords.Input Text And Verify Input For Web Element 	${elem} 	${quantity}








