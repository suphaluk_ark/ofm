*** Setting ***

*** Variables ***

*** Keywords ***
Convert price to total amount format
	[Arguments] 	${price}
	${price}= 	Evaluate 	'{:,.2f}'.format($price)
	[Return] 	${price}

Summarize price with VAT
	[Documentation] 	This keyword summarize price with vat
	... 	After calling this keyword ‘test variable’ price_with_vat and price_with_vat_format would be assigned
	[Arguments] 	${product_price_without_vat} 	${product_vat}=${product_vat}
	${price_with_vat}= 	Evaluate 	${product_price_without_vat} + (${product_price_without_vat} * ${product_vat}/100)
	${price_with_vat_format}= 	Convert price to total amount format 	${price_with_vat}
	Set Test Variable 	${price_with_vat} 	${price_with_vat}
	Set Test Variable 	${price_with_vat_format} 	${price_with_vat_format}

Summarize price with quantity
	[Documentation] 	This keyword summarize price with vat and quantity
	... 	After calling this keyword ‘test variable’ price_with_quantity and price_with_quantity_format would be assigned
	[Arguments] 	${product_quantity} 	${price_exclude_vat}
	${price_with_quantity}= 	Evaluate 	(${price_exclude_vat} * ${product_quantity}) + round((${price_exclude_vat} * ${product_quantity}) * (${product_vat}/100.00),2)
	${price_with_quantity_format}= 	Convert price to total amount format 	${price_with_quantity}
	Set Test Variable 	${price_with_quantity} 	${price_with_quantity}
	Set Test Variable 	${price_with_quantity_format} 	${price_with_quantity_format}

Calculate vat from price exclude vat
	[Documentation] 	This keyword subtract from original total with grand total
	... 	After calling this keyword ‘test variable’ vat_amount and vat_amount_with_format would be assigned
	[Arguments] 	${price_exclude_vat} 	${product_vat}=${product_vat}
	${vat_amount}= 	Evaluate 	round(${price_exclude_vat} * ${product_vat}/100, 2)
	${vat_amount_with_format}= 	Convert price to total amount format 	${vat_amount}
	Set Test Variable 	${vat_amount} 	${vat_amount}
	Set Test Variable 	${vat_amount_with_format} 	${vat_amount_with_format}

Get vat amount from total price with vat
    [Documentation]     This keyword subtract from original total with grand total
    ...     After calling this keyword 'test variable' vat_amount and vat_amount_with_format would be assigned
    [Arguments]     ${total_price_with_vat}     ${product_vat}=${product_vat}
    ${vat_amount}=     Evaluate     ${total_price_with_vat} - (${total_price_with_vat} * 100) / (100 + ${product_vat})
    ${round_vat_amount}=     Evaluate     round(${total_price_with_vat} - round((${total_price_with_vat} * 100) / (100 + ${product_vat}),2),2)
    ${vat_amount_with_format}=     Convert price to total amount format     ${vat_amount}
    Set Test Variable     ${vat_amount}     ${vat_amount}
    Set Test Variable     ${round_vat_amount}     ${round_vat_amount}
    Set Test Variable     ${vat_amount_with_format}     ${vat_amount_with_format}

Summarize surcharge fee with product quantity
	[Arguments]     ${surcharge_fee}     ${product_quantity}
	${total_surcharge_fee}= 	Evaluate 	${surcharge_fee} * ${product_quantity}
	${total_surcharge_fee}= 	Convert price to total amount format 	${total_surcharge_fee}
	Set Test Variable 	${total_surcharge_fee} 	${total_surcharge_fee}
	Set Test Variable 	${total_surcharge_fee_format} 	${total_surcharge_fee_format}

Summarize grand total
	[Arguments] 	${total_price_vat} 	${special_fee_delivery} 	${delivery_fee} 	${coupon_price}
	${grand_total}= 	Evaluate 	${total_price_vat}+${special_fee_delivery}+${coupon_price}+${delivery_fee}
	${grand_total_with_format}= 	Convert price to total amount format 	${grand_total}
	Set Test Variable 	${grand_total} 	${grand_total}
	Set Test Variable 	${grand_total_with_format} 	${grand_total_with_format}

Subtract total price from vat
    [Arguments]     ${total_price}
    Get vat amount from total price with vat     ${total_price}
    ${total_without_vat}=     Evaluate     ${total_price} - ${vat_amount}
    ${total_without_vat_format}= 	Evaluate 	'{:,.2f}'.format(math.floor($total_without_vat*100)/100) 	math
    ${round_total_without_vat}=     Evaluate     ${total_price} - ${round_vat_amount}
    ${round_total_without_vat_format}=     Convert price to total amount format     ${total_without_vat}
    Set Test Variable     ${total_without_vat}     ${total_without_vat}
    Set Test Variable     ${round_total_without_vat}     ${round_total_without_vat}
    Set Test Variable     ${total_without_vat_format}     ${total_without_vat_format}
    Set Test Variable     ${round_total_without_vat_format}     ${round_total_without_vat_format}

Set product sku and qty as dictionary
	[Arguments] 	${product_sku} 	${product_qty}
	${status}= 	Evaluate 	isinstance($product_sku, list)
	${product_sku_and_qty}= 	Create Dictionary
	${product_sku_and_qty}= 	Run Keyword If 	${status} 	Evaluate 	dict(zip($product_sku,$product_qty))
	...	ELSE 	Evaluate 	dict(${product_sku}=${product_qty})
	[Return] 	&{product_sku_and_qty}


