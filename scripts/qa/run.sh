#!/bin/bash

# Go out to root of git folder
cd ../../

# Set test ENV
ENV="qa"
RESULT_FOLDER="results/"$ENV
SCRIPTS_FOLDER="../../scripts/"$ENV"/"

# Install all of required python library or other library
pip install -r requirements.txt

# Prepare folder result
if [ ! -d $RESULT_FOLDER ]; then
    mkdir -p $RESULT_FOLDER
fi
cd $RESULT_FOLDER

# Run test robot command
rm -rf *.html *.xml *.jpg
date
python -m robot.run -v ENV:$ENV -A $SCRIPTS_FOLDER/argfile.txt ../../testcases/otp_service
date
python -m robot.run -v ENV:$ENV --rerunfailed output.xml --output rerun.xml ../../testcases/otp_service
date
python -m robot.rebot --merge --output output.xml output.xml rerun.xml
date

# For do not let Jenkins mark failed from shell script.
exit 0