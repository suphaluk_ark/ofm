def branches = [:]
def tags_list = ["individual","guest","company"]
def is_headless_browser = 'True'
def file_path = 'testcases/web/smoke/'
node {
   stage('Preparation') {
      checkout([$class: 'GitSCM', branches: [[name: '*/master']], 
      doGenerateSubmoduleConfigurations: false, 
      extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false], 
      [$class: 'CloneOption', noTags: false, reference: '', shallow: true, timeout: 30]], 
      submoduleCfg: [], userRemoteConfigs: 
      [[credentialsId: '896a280a-3010-4232-a88d-2fcbbf4e7b36', 
      url: 'git@bitbucket.org:centralonline/ofm.git']]])
   }
   stage('Smoke Testing') {
       for (int i = 0; i < tags_list.size(); i++){
           def index = i
           def tags = tags_list[index]
           branches["${tags}"]={
               sh """/usr/local/bin/robot \
               --variable headless_flag:${is_headless_browser} \
               --variable ENV:staging \
               --include smokeAND${tags} \
               --outputdir Report --output output${index}.xml \
               --nostatusrc \
               -L TRACE \
               ${file_path}smoke.robot"""
           }
       }
   }
   parallel branches
   def merge_file = ''
    for(i = 0; i < tags_list.size(); i++){
        merge_file += ' Report/output'+i+'.xml'
    }
   sh "/usr/local/bin/rebot --nostatusrc --outputdir Report --merge ${merge_file}"
   stage('Results') {
       step([$class: 'RobotPublisher', disableArchiveOutput: false, enableCache: true, 
       logFileName: 'log.html', 
       onlyCritical: true, 
       otherFiles: '*.png', 
       outputFileName: 'output.xml', 
       outputPath: 'Report/', 
       passThreshold: 80.0, 
       reportFileName: 'report.html', 
       unstableThreshold: 20.0])
   }
   stage('Slack'){
       slackSend baseUrl: 'https://central-technology.slack.com/services/hooks/jenkins-ci/', 
       botUser: true, 
       channel: 'ofm-automation', 
       message: 'Test Result', 
       token: '0Dyy5MAjdBBnB8llTlTce6AU'
   }
}